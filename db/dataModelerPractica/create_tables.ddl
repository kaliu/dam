-- Generado por Oracle SQL Developer Data Modeler 19.4.0.350.1424
--   en:        2020-03-11 16:37:59 CET
--   sitio:      Oracle Database 12c
--   tipo:      Oracle Database 12c



CREATE TABLE client (
    codi         NUMBER(5) NOT NULL,
    nif          VARCHAR2(8) NOT NULL,
    nom          VARCHAR2(20) NOT NULL,
    adreça       VARCHAR2(50) NOT NULL,
    email        VARCHAR2(30),
    telefon      NUMBER(9) NOT NULL,
    oferta_codi  NUMBER(5) NOT NULL
);

ALTER TABLE client ADD CONSTRAINT client_pk PRIMARY KEY ( codi );

ALTER TABLE client ADD CONSTRAINT client_nif_un UNIQUE ( nif );

ALTER TABLE client ADD CONSTRAINT client_nom_un UNIQUE ( nom );

CREATE TABLE demandant (
    dni             VARCHAR2(8) NOT NULL,
    nom             VARCHAR2(20) NOT NULL,
    cognom          VARCHAR2(20) NOT NULL,
    cognom2         VARCHAR2(20),
    adreça          VARCHAR2(50) NOT NULL,
    carnet_conduir  CHAR(1) DEFAULT 'N' NOT NULL,
    foto            BLOB
);

ALTER TABLE demandant
    ADD CONSTRAINT ck_carnet_conduir CHECK ( carnet_conduir = 'N'
                                             OR carnet_conduir = 'S' );

ALTER TABLE demandant ADD CONSTRAINT demandant_pk PRIMARY KEY ( dni );

CREATE TABLE es_associat (
    demandant_dni  VARCHAR2(8) NOT NULL,
    oferta_codi    NUMBER(5) NOT NULL
);

ALTER TABLE es_associat ADD CONSTRAINT es_associat_pk PRIMARY KEY ( demandant_dni,
                                                                    oferta_codi );

CREATE TABLE ha_estudiat (
    demandant_dni   VARCHAR2(8) NOT NULL,
    titulació_codi  NUMBER NOT NULL,
    data_expedicio  DATE NOT NULL,
    centre_estudis  VARCHAR2(20) NOT NULL
);

ALTER TABLE ha_estudiat ADD CONSTRAINT ha_estudiat_pk PRIMARY KEY ( demandant_dni,
                                                                    titulació_codi );

CREATE TABLE oferta (
    codi        NUMBER(5) NOT NULL,
    descripcio  VARCHAR2(50) NOT NULL,
    sou         NUMBER(5, 2) NOT NULL
);

ALTER TABLE oferta ADD CONSTRAINT ck_sou CHECK ( sou > 0 );

ALTER TABLE oferta ADD CONSTRAINT oferta_pk PRIMARY KEY ( codi );

CREATE TABLE titulació (
    codi  NUMBER NOT NULL,
    nom   VARCHAR2(20) NOT NULL
);

ALTER TABLE titulació ADD CONSTRAINT titulació_pk PRIMARY KEY ( codi );

ALTER TABLE client
    ADD CONSTRAINT client_oferta_fk FOREIGN KEY ( oferta_codi )
        REFERENCES oferta ( codi );

ALTER TABLE es_associat
    ADD CONSTRAINT es_associat_demandant_fk FOREIGN KEY ( demandant_dni )
        REFERENCES demandant ( dni );

ALTER TABLE es_associat
    ADD CONSTRAINT es_associat_oferta_fk FOREIGN KEY ( oferta_codi )
        REFERENCES oferta ( codi );

ALTER TABLE ha_estudiat
    ADD CONSTRAINT ha_estudiat_demandant_fk FOREIGN KEY ( demandant_dni )
        REFERENCES demandant ( dni );

ALTER TABLE ha_estudiat
    ADD CONSTRAINT ha_estudiat_titulació_fk FOREIGN KEY ( titulació_codi )
        REFERENCES titulació ( codi );

CREATE OR REPLACE VIEW oferta_demanda ( codi, descripcio, DNI_Demandant ) AS
SELECT o.codi, o.descripcio, COUNT(ea.DNI_Demandant)
FROM Oferta o
JOIN Es_associat ea ON (ea.Oferta_codi = o.codi)
GROUP BY codi, descripcio 
;



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             6
-- CREATE INDEX                             0
-- ALTER TABLE                             15
-- CREATE VIEW                              1
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- TSDP POLICY                              0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
