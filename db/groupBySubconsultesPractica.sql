-- CONSULTES RESUM
-------------------------------

-- 1.(0.5 PUNTS) Mostrar per cada videoclub (indica el seu nom) la quantitat 
-- de lloguers on s'ha trigat més del que estava previst en retornar una copia. 
-- El resultat s'ha de mostrar ordenat alfabèticament pel nom del videoclub.
SELECT v.nom, COUNT(ll.lloguer_id) AS lloguers
FROM videoclub v
JOIN copia c ON (c.videoclub_id = v.videoclub_id)
JOIN lloguer ll ON (ll.copia_id = c.copia_id)
WHERE data_retorn_prevista < data_retorn_real
GROUP BY v.nom
ORDER BY v.nom ASC;

-- 2.(1 PUNT) Per cada client (mostrar el seu nom i cognom) que hagi fet lloguers, 
-- mostrar la quantitat de lloguers que ha realitzat cada any. 
-- Els resultats s'han de mostrar ordenats per cognom, nom i any.
SELECT c.nom, c.cognom, TO_CHAR(ll.data_lloguer, 'yyyy'), COUNT(ll.lloguer_id) AS lloguers
FROM client c
JOIN lloguer ll ON (c.client_id = ll.client_id)
GROUP by c.nom, c.cognom, TO_CHAR(ll.data_lloguer, 'yyyy')
ORDER BY c.cognom, c.nom, TO_CHAR(ll.data_lloguer, 'yyyy');

-- 3.(0.5 PUNTS) Mostrar el nom dels generes que s'utilitzin a menys de 55 pel·lícules,
-- ordenats alfabèticament.
SELECT g.nom
FROM genere g
JOIN pelicula_genere pg ON (g.genere_id = pg.genere_id) 
GROUP BY g.nom
HAVING COUNT(pg.genere_id) > 55;
-- 4.(1 PUNT)  Mostrar el nom dels dies de la setmana on s'han fet lloguers i 
-- la quantitat pagada cada dia. Mostrar ordenat pel dia de la setmana. 
SELECT TO_CHAR(ll.data_lloguer, 'day') AS dia_setmana, SUM(ll.quantitat_pagada)
FROM lloguer ll
GROUP BY TO_CHAR(ll.data_lloguer, 'day'), TO_CHAR(ll.data_lloguer, 'd')
ORDER BY TO_CHAR(ll.data_lloguer, 'd') ASC;

-- 5.(1 PUNT) Mostrar per cada actor (nom i cognom) a quantes pel·lícules ha 
-- treballat que s'hagin estrenat l'any 2006. També s'ha de mostrar la duració 
-- mitja d'aquestes pel·lícules (arrodonida a dos decimals). Només ens interessen 
-- els actors que han fet més de 10. El resultat s'ha de mostrar ordenat per la 
-- quantitat de pel·lícules (de més a menys)
SELECT a.nom, a.cognom, COUNT(p.pelicula_id) AS NUM_PELICULES , ROUND(AVG(p.duracio_pelicula), 2) AS DURACIO_MITJANA
FROM actor a 
JOIN pelicula_actor pa ON (pa.actor_id = a.actor_id)
JOIN pelicula p ON (p.pelicula_id = pa.pelicula_id)
WHERE p.any_estrena = 2006
GROUP BY a.nom, a.cognom
HAVING COUNT(p.pelicula_id) > 10
ORDER BY COUNT(p.pelicula_id) DESC;

-- 6.(0.5 PUNTS) Mostrar els videoclubs que tenen treballadors inactius. S'ha de 
-- mostrar el nom del videoclub i quants treballadors inactius té, ordenats 
-- alfabèticament pel nom del videoclub.
SELECT v.nom, COUNT(t.treballador_id) AS treballadors_inactius
FROM videoclub v
JOIN treballador t ON (t.videoclub_id = v.videoclub_id)
WHERE t.actiu = 0
GROUP BY v.nom
ORDER BY v.nom ASC;

-- 7.(1 PUNT) Mostrar el nom i el cognom dels treballadors i la quantitat de 
-- lloguers que han gestionat i que s'han tornat. Només han de sortir els 
-- treballadors que han fet més de 2000 lloguers d'aquest tipus.
-- El resultat s'ha de mostrar ordenat de més a menys quantitat.
SELECT t.nom, t.cognom, COUNT(ll.lloguer_id)
FROM treballador t
JOIN lloguer ll ON (ll.treballador_lloguer_id = t.treballador_id)
WHERE ll.data_retorn_real IS NOT NULL
GROUP BY t.nom, t.cognom
HAVING COUNT(ll.lloguer_id) > 2000
ORDER BY COUNT(ll.lloguer_id) DESC;

-----------------------------------------
-- SUBCONSULTES
-----------------------------------------

-- 1.(0.5 PUNTS) Mostrar quantes pel·lícules tenen una duració menor que 
-- qualsevol duració de les pel·lícules amb qualificació R
SELECT COUNT(p.pelicula_id)
FROM pelicula p
WHERE duracio_pelicula < ANY (SELECT duracio_pelicula
                              FROM pelicula p
                              WHERE qualificacio = 'R');

-- 2.(1 PUNT) Mostrar el percentatge, arrodonit a dos decimals, 
-- que representa la quantitat de lloguers de pel·lícules amb qualificació 
-- PG respecte a la quantitat total de lloguers.
SELECT ROUND(100*(SELECT COUNT(ll.lloguer_id)
            FROM lloguer ll
            JOIN copia cp ON (ll.copia_id = cp.copia_id)
            JOIN pelicula p ON (p.pelicula_id = cp.pelicula_id)
            WHERE p.qualificacio = 'PG'
            ) / (
            SELECT COUNT(ll.lloguer_id)
            FROM lloguer ll), 2) AS PERCENTATGE_PG
FROM dual;



-- 3.(0.5 PUNTS) Mostrar els títols de les pel·lícules amb el cost diari 
-- més petit de totes, ordenats alfabèticament.
SELECT p.titol
FROM pelicula p
WHERE cost_diari <= ALL (SELECT cost_diari
                         FROM pelicula)
ORDER BY p.titol ASC;

-- 4.(0.5 PUNTS) Mostrar el nom i el cognom dels treballadors que són 
-- encarregats de videoclub, ordenats per cognom i nom (SENSE FER JOIN A CAP TAULA)
SELECT t.nom, t.cognom
FROM treballador t
WHERE t.treballador_id = ANY (SELECT v.encarregat_id
                              FROM videoclub v)
ORDER BY t.cognom, t.nom;

-- 5.(0.5 PUNTS) Mostrar el nom, el cognom, el carrer i la ciutat dels 
-- clients que viuen al mateix carrer i ciutat que el client JEREMY HURTADO. 
-- JEREMY HURTADO no s'ha de mostrar al resultat. (SENSE FER JOIN A CAP TAULA).
SELECT c.nom, c.cognom, c.carrer, c.ciutat
FROM client c
WHERE c.carrer = (SELECT c2.carrer
                  FROM client c2
                  WHERE c2.nom = 'JEREMY' AND c2.cognom = 'HURTADO') AND
     c.ciutat = (SELECT c2.ciutat
                  FROM client c2
                  WHERE c2.nom = 'JEREMY' AND c2.cognom = 'HURTADO');

-- 6.(0.5 PUNTS) Mostrar per cada pel·lícula el seu títol i quantes vegades s'ha llogat. 
-- El resultat s'ha de mostrar ordenat per la quantitat de vegades, de més gran a més petita.
-- (No es pot fer JOIN a la taula PELICULA).

SELECT p.titol, (SELECT COUNT(ll.lloguer_id) AS vegades_llogada
                 FROM lloguer ll
                 JOIN copia cp ON (ll.copia_id = cp.copia_id)
                 WHERE p.pelicula_id = cp.pelicula_id )
FROM pelicula p;

-- 7.(1 PUNT) Mostrar la mitja de lloguers realitzats a l'any, es a dir, 
-- quants lloguers es fan en mitja durant un any.
SELECT AVG(mitja_lloguers) AS mitja_lloguers_al_any
FROM (SElECT TO_CHAR(data_lloguer, 'yyyy'), COUNT(ll.lloguer_id) mitja_lloguers
      FROM lloguer ll
      GROUP BY TO_CHAR(data_lloguer, 'yyyy'));

