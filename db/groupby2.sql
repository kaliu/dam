-- 1. Mostra per cada producte que s’ha venut alguna vegada, el seu codi 
-- i la quantitat de producte venut. Ordenats de major a menor quantitat.
SELECT i.producto_no, SUM(i.cantidad)
FROM item i
GROUP BY i.producto_no
ORDER BY SUM(i.cantidad) desc;

-- 2. Mostra per cada producte que s’ha venut alguna vegada, el seu codi, 
-- nom i la quantitat de producte venut. Ordenats de major a menor quantitat.
SELECT i.producto_no, p.descripcion, SUM(i.cantidad)
FROM item i
JOIN productos p ON (p.producto_no = i.producto_no)
GROUP BY i.producto_no, p.descripcion
ORDER BY SUM(i.cantidad) desc;

-- 3. Mostra per cada departament: nom, codi, localitat i sou_mig dels seus 
-- treballadors. Han de sortir fins i tot els departaments que no tenen empleats. 
-- Ordena'ls de més sou mig a menys sou_mig.
SELECT d.dnombre, d.dept_no, d.loc, ROUND(AVG(e.salario))
FROM depart d
LEFT JOIN emple e ON (e.dept_no = d.dept_no)
GROUP BY d.dnombre, d.dept_no, d.loc
ORDER BY AVG(e.salario) DESC;

-- 4. Mostra per cada client: codi, cognom, nif, límit de crèdit, número de 
-- les seves comandes i import total de les seves comandes. Mostra només els 
-- clients que hagin fet més de 3 comandes.
SELECT DISTINCT c.cliente_no, c.nombre, c.cliente_nif, c.limite_credito, COUNT(pe.pedido_no), SUM(i.importe_uni * i.cantidad) AS gastado
FROM clientes c
JOIN pedidos pe ON (pe.cliente_no = c.cliente_no)
JOIN item i ON (i.pedido_no = pe.pedido_no)
GROUP BY c.cliente_no, c.nombre, c.cliente_nif, c.limite_credito
HAVING COUNT(pe.pedido_no) > 3;

-- 5. Ordena els productes segons el nombre total d'unitats venudes (de més a menys).Mostra el número de producte i la seva descripció. 
-- Han de sortir també els productes que no s'han venut mai.
SELECT pr.producto_no, pr.descripcion
FROM productos pr
FULL JOIN item i ON (i.producto_no = pr.producto_no)
GROUP BY pr.producto_no, pr.descripcion, i.cantidad
ORDER BY NVL(i.cantidad, 0) DESC;

-- 6. Mostra els departaments que tenen més de 5 treballadors. Mostra dept_no, nom, localitat i número de treballadors 
-- (no utilitzar la columna depart.num_treballadors, perque està a 0).
SELECT d.dept_no, d.dnombre, d.loc, COUNT(e.emp_no)
FROM depart d
JOIN emple e ON (e.dept_no = d.dept_no)
GROUP BY d.dept_no, d.dnombre, d.loc
HAVING COUNT(e.emp_no) > 5;

-- 7. Mostra aquelles comandes que tenen més d'una línia de comanda. Mostra número i data de la comanda, i nif del client.
SELECT p.pedido_no, p.fecha_pedido
FROM pedidos p 
JOIN item i ON (i.pedido_no = p.pedido_no)
GROUP BY p.pedido_no, p.fecha_pedido
HAVING COUNT(i.pedido_no) > 1;

-- 8. Mostra quins empleats han realitzat més de 4 comandes, (es a dir, els seus clients han fet més de 4 comandes). 
-- Mostra cognom, ofici, num_depart i número de comandes.
SELECT e.apellido, e.oficio, e.dept_no, COUNT(pe.pedido_no) as ventas
FROM emple e
JOIN clientes c ON (c.vendedor_no = e.emp_no)
JOIN pedidos pe ON (pe.cliente_no = c.cliente_no)
GROUP BY e.apellido, e.oficio, e.dept_no
HAVING COUNT(pe.pedido_no) > 4;


-- 9. Mostra quantes comandes s'han venut a cada departament l'any 1999.Mostra número de departament i número de comandes venudes.
SELECT d.dept_no, COUNT(p.pedido_no) as comandes
FROM depart d
JOIN emple e ON (e.dept_no = d.dept_no)
JOIN clientes c ON (c.vendedor_no = e.emp_no)
JOIN pedidos p ON (p.cliente_no = c.cliente_no)
WHERE TO_CHAR(p.fecha_pedido, 'yyyy') = 1999
GROUP by d.dept_no;

-- 10. Mostra per cada any, quantes comandes s'han venut a l'empresa i l'import total de les comandes venudes aquell any.
SELECT TO_CHAR(p.fecha_pedido, 'yyyy'), COUNT(p.pedido_no), SUM(i.importe_uni * i.cantidad) AS import_total
FROM pedidos p
JOIN item i ON (i.pedido_no = p.pedido_no)
GROUP BY TO_CHAR(p.fecha_pedido, 'yyyy')
ORDER BY TO_CHAR(p.fecha_pedido, 'yyyy');

-- 11. Mostra per cada empleat director: codi, cognom, sou, número de subordinats, i sou_mig dels seus subordinats
SELECT ed.emp_no, ed.apellido, ed.salario, COUNT(e.emp_no) AS num_subordinats, AVG(e.salario) AS salari_mitja_subordinats
FROM emple ed
JOIN emple e ON (e.dir = ed.emp_no)
GROUP BY ed.emp_no, ed.apellido, ed.salario;

-- 12. Mostra per cada província la mitjana del límit de crèdit dels seus clients.Mostra província i límit de crèdit promig. 
-- Ordena les files de més a menys límit de crèdit.
SELECT c.provincia, ROUND(NVL(AVG(c.limite_credito), '0'))
FROM clientes c
GROUP BY c.provincia
ORDER BY AVG(c.limite_credito);

-- 13. Mostra per cada client el seu nom, any  i l'import total de les seves comandes a l'any 1999, a l'any 2000 i a l'any 2001. 
-- ‘Any’ ha de ser el valor: 1999, 2000 o 2001.
SELECT c.nombre, TO_CHAR(p.fecha_pedido, 'yyyy') AS an_y, SUM(i.importe_uni * i.cantidad)
FROM clientes c
JOIN pedidos p ON (p.cliente_no = c.cliente_no)
JOIN item i ON (i.pedido_no = p.pedido_no)
GROUP BY c.nombre, TO_CHAR(p.fecha_pedido, 'yyyy')
HAVING TO_CHAR(p.fecha_pedido, 'yyyy') >= 1999 AND TO_CHAR(p.fecha_pedido, 'yyyy') <= 2001;

-- 14. Llisteu les dades: número, nom, localitat i número d'empleats dels departaments amb més de 5 empleats.
SELECT d.dept_no, d.dnombre, d.loc, COUNT(e.emp_no)
FROM depart d
JOIN emple e ON (e.dept_no = d.dept_no)
GROUP BY d.dept_no, d.dnombre, d.loc
HAVING COUNT(e.emp_no) > 5;

-- 15.  Mostrar el nom de la companyia client que més diners ha gastat.
SELECT c.nombre AS mejor_cliente
FROM clientes c
JOIN pedidos p ON (p.cliente_no = c.cliente_no)
JOIN item i ON (i.pedido_no = p.pedido_no)
GROUP BY c.nombre
ORDER BY SUM(i.importe_uni * i.cantidad) desc
FETCH FIRST 1 ROW ONLY;

-- 16.  Mostrar quin va ser l’any amb les vendes més altes: mostrar any i total recaptat.
SELECT TO_CHAR(p.fecha_pedido, 'yyyy'), SUM(i.importe_uni * i.cantidad) AS generado
FROM pedidos p
JOIN item i ON (i.pedido_no = p.pedido_no)
GROUP BY TO_CHAR(p.fecha_pedido, 'yyyy')
ORDER BY SUM(i.importe_uni * i.cantidad) desc
FETCH FIRST 1 ROW ONLY;

-- 17. Indica el nom del producte del qual s'ha venut més quantitat.
SELECT pr.descripcion AS producto_mas_vendido
FROM productos pr
JOIN item i ON (i.producto_no = pr.producto_no)
GROUP BY pr.descripcion
ORDER BY SUM(i.cantidad) desc
FETCH FIRST 1 ROW ONLY;