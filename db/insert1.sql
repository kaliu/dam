--1) Doneu-vos d'alta com a empleats de l'empresa, afegiu-vos al departament número 20 i poseu-vos d'ofici ANALISTA i sou 5000. La data d'alta del contracte és 01 de Gener de 2011
INSERT INTO emple (emp_no, apellido, oficio, fecha_alt, salario, dept_no)
           VALUES ((SELECT MAX(emp_no) FROM emple) + 1, 'Gras', 'ANALISTA', TO_DATE('01/01/2011', 'DD/MM/YYYY'), 5000, 20);
           
--2) Doneu d'alta el IES la Ferreria com a client amb les dades (111111111A, Montcada i Reixac, 08110, C/Progrés numero 3, 
-- límit de crèdit = 10000) i NULL els altres camps
INSERT INTO clientes (cliente_no, cliente_nif, nombre, direccion, localidad, codigo_postal, limite_credito)
              VALUES ((SELECT MAX(cliente_no) FROM clientes) + 1, '111111111A', 'IES La Ferreria', 'C/Progrés Numero 3', 'Montcada i Reixac',
              '08110', 10000);
              
--3) Inventeu-vos 3 productes
INSERT INTO productos (producto_no, descripcion, precio_actual, stock_disponible)
               VALUES ((SELECT MAX(producto_no) FROM productos) + 1, 'PORTATIL', 120, 1);
INSERT INTO productos (producto_no, descripcion, precio_actual, stock_disponible)
               VALUES ((SELECT MAX(producto_no) FROM productos) + 1, 'PANTALLA', 39, 7);
INSERT INTO productos (producto_no, descripcion, precio_actual, stock_disponible)
               VALUES ((SELECT MAX(producto_no) FROM productos) + 1, 'ORDENADOR', 50, 9);
               
--4) Fixeu el sou de tots els venedors (ofici = ‘VENDEDOR’)  a 2200€
UPDATE emple SET salario = 2200
WHERE oficio = 'VENDEDOR';

--5) Canvieu totes les descripcions dels productes a format INITCAP
UPDATE productos SET descripcion = INITCAP(descripcion);

--6) Poseu el preu a 0 tots els productes dels quals no hi hagi unitats en stock.
UPDATE productos SET precio_actual = 0
WHERE stock_disponible = 0;

--7) Poseu la data d'enviament de cada comanda, 1 setmana posterior a la data de la comanda, per totes les comandes.
UPDATE pedidos SET fecha_envio = fecha_pedido + 7;

--8) Doneu d'alta la següent factura a la base de dades:
INSERT INTO pedidos(pedido_no, fecha_pedido, cliente_no, fecha_envio)
            VALUES  (150, TO_DATE('01-02-2017', 'DD-MM-YYYY'), 2,  TO_DATE('04-02-2017', 'DD-MM-YYYY'));
INSERT INTO item(pedido_no, item_no, producto_no, importe_uni, cantidad)
            VALUES(150, 
            1, 
            (SELECT producto_no FROM productos WHERE descripcion = 'Carrito Oficina'),
            (SELECT precio_actual FROM productos WHERE descripcion = 'Carrito Oficina'),
            3);
INSERT INTO item(pedido_no, item_no, producto_no, importe_uni, cantidad)
            VALUES(150, 
            2, 
            (SELECT producto_no FROM productos WHERE descripcion = 'Mesa Modelo Uni¿N'),
            (SELECT precio_actual FROM productos WHERE descripcion = 'Mesa Modelo Uni¿N'),
            1);
INSERT INTO item(pedido_no, item_no, producto_no, importe_uni, cantidad)
            VALUES(150, 
            3, 
            (SELECT producto_no FROM productos WHERE descripcion = 'Mesa Director'),
            (SELECT precio_actual FROM productos WHERE descripcion = 'Mesa Director'),
            2);

--9)  Augmenta el salari un 5% dels empleats que han fet alguna comanda amb un valor total superior a 2000€ 
UPDATE emple e SET salario = salario + (salario*5)/100
WHERE 2000 > ANY (SELECT (i.importe_uni * i.cantidad)
                  FROM item i
                  JOIN pedidos pe ON (pe.pedido_no = i.pedido_no)
                  JOIN clientes c ON (pe.cliente_no = c.cliente_no)
                  JOIN emple e2 ON (c.vendedor_no = e2.emp_no)
                  WHERE e.emp_no = e2.emp_no); 

--10) Elimineu els departaments que no tenen treballadors.
DELETE FROM depart 
WHERE dept_no NOT IN (SELECT dept_no FROM emple);

--11) Elimina totes les dades introduïdes a l'enunciat 8.
DELETE FROM item
WHERE pedido_no = 150;
DELETE FROM pedidos
WHERE pedido_no = 150;

--12) Decrementeu en un 10% el preu actual dels productes que no s'han venut mai
UPDATE productos pr SET pr.precio_actual = pr.precio_actual - (precio_actual*0.1)
WHERE pr.producto_no NOT IN (SELECT producto_no FROM item);

--13) Feu que tots els analistes tinguin el mateix sou (poseu com a sou el valor mig del sou actual dels analistes)
UPDATE emple e SET salario = (SELECT AVG(salario) FROM emple e WHERE e.oficio = 'ANALISTA')
WHERE e.oficio = 'ANALISTA';
--14) Creeu la següent taula: Historic_vendes (year, total_recaptat) amb la següent sentència SQL
CREATE TABLE Historic_vendes(
       Year NUMBER(4,0) PRIMARY KEY,
       Total_recaptat NUMBER(10,2));
--Ompliu-la de manera consistent amb una única sentència SQL (poseu files  només als anys que hi ha hagut comandes).
INSERT INTO Historic_vendes
SELECT TO_NUMBER(TO_CHAR(p.fecha_pedido, 'yyyy')), SUM(i.importe_uni * i.cantidad)
         FROM pedidos p
         JOIN item i ON (i.pedido_no = p.pedido_no) 
         GROUP BY TO_NUMBER(TO_CHAR(p.fecha_pedido, 'yyyy'));

--15) Modifiqueu la columna dir de la taula emple de manera que el cap (“jefe”) de cada treballador sigui el treballador que més cobra del seu departament.
UPDATE emple e1
SET e1.dir = (SELECT e2.emp_no
              FROM emple e2
              WHERE MAX(e1.salario) > e2.salario AND e1.dept_no = e2.dept_no);
