/* 1. Doneu d’alta els esports “Motociclisme” amb codi 4 i “Natació” amb el primer 
codi d’esport que estigui lliure, tots dos de tipus individual. Assigneu a Mireia 
Belmonte l’esport “Natació”. */
INSERT INTO deportes
VALUES (4, 'Motociclisme', 1);

INSERT INTO deportes
VALUES ((SELECT MAX(deporte_id) + 1 FROM deportes), 'Natació', 1);

UPDATE deportistas
SET deporte_id = (SELECT deporte_id FROM deportes WHERE nom_depor = 'Natació')
WHERE nombre = 'Mireia Belmonte';

/* 2. Passar a majúscules tots els noms d'esport. */
UPDATE deportes
SET nom_depor = UPPER(nom_depor);

/* 3. Donar d’alta les competicions “Mundial de motociclisme” i “Mundial de 
natació”. */
INSERT INTO competiciones
VALUES ((SELECT MAX(competicion_id) + 1 FROM competiciones), 'Mundial de motociclisme');
INSERT INTO competiciones
VALUES ((SELECT MAX(competicion_id) + 1 FROM competiciones), 'Mundial de natació');
/* 4. Guardar el fet de que Marc Marquez  hagi guanyat “MotoGP” l’any actual i 
Mireia Belmonte hagi guanyat el “Mundial de Natació” els anys 2010 i 2016. */
INSERT INTO victorias
VALUES ((SELECT deportista_id FROM deportistas WHERE nombre = 'Marc Marquez'),
        (SELECT competicion_id FROM competiciones WHERE nombre = 'Mundial de motocicleta'),
        TO_NUMBER(TO_CHAR(current_date, 'yyyy')));
INSERT INTO victorias
VALUES ((SELECT deportista_id FROM deportistas WHERE nombre = 'Mireia Belmonte'),
        (SELECT competicion_id FROM competiciones WHERE nombre = 'Mundial de natació'),
        2010);
INSERT INTO victorias
VALUES ((SELECT deportista_id FROM deportistas WHERE nombre = 'Mireia Belmonte'),
        (SELECT competicion_id FROM competiciones WHERE nombre = 'Mundial de natació'),
        2016);

/* 5. Corregir el fet que Mireia Belmonte va guanyar el “Mundial de natació” 
l’any 2017 enlloc del 2016 i que el nom correcte de “Mundial de motociclisme” 
és “MotoGP”. */
UPDATE victorias
SET anyo_victoria = 2017
WHERE deportista_id = (SELECT deportista_id FROM deportistas WHERE nombre = 'Mireia Belmonte') AND
anyo_victoria = 2016;

UPDATE competiciones
SET nombre = 'MotoGP'
WHERE nombre = 'Mundial de motociclisme';

/* 6. Donar d’alta com esportista de futbol a Quique Setièn (edat 61 anys, 
el seu email que sigui null) i assigneu-lo com entrenador de Gerard Pique 
I Andres Iniesta. */
INSERT INTO deportistas
VALUES ((SELECT MAX(deportista_id) + 1 FROM deportistas), 'Quique Setièn', 61, null, 
        (SELECT deporte_id FROM deportes WHERE nom_depor = 'FUTBOL'), null);

UPDATE deportistas
SET entrenador_id = (SELECT deportista_id FROM deportistas WHERE nombre = 'Quique Setièn')
WHERE deportista_id = (SELECT deportista_id FROM deportistas WHERE nombre = 'Gerard Pique') OR
      deportista_id = (SELECT deportista_id FROM deportistas WHERE nombre = 'Andres Iniesta');

/* 7. Crear la taula GUANYADORS: */

CREATE TABLE GUANYADORS(
CODI_GUANYADOR NUMBER(4,0) PRIMARY KEY,
NOM_GUANYADOR VARCHAR2(100),
NOM_ESPORT VARCHAR2(100),
NOM_COMPETICIO VARCHAR2(100),
NUM_VEGADES_GUANYADOR NUMBER(4,0));

/*  · La columna codi_guanyador s'ha d'omplir amb el codi de l'esportista
    · La columna NUM_VEGADES_GUANYADOR és la quantitat de vegades que un esportista ha guanyat una competició.

Amb una sola ordre omplir aquesta taula a partir de les taules existents. */
INSERT INTO guanyadors
SELECT d.deportista_id, d.nombre, de.nom_depor, c.nombre, COUNT(v.deportista_id)
FROM deportistas d
JOIN deportes de ON (d.deporte_id = de.deporte_id)
JOIN victorias v ON (v.deportista_id = d.deportista_id)
JOIN competiciones c ON (c.competicion_id = v.competicion_id) 
GROUP BY d.deportista_id, d.nombre, de.nom_depor, c.nombre;

/* 8. Modificar l'edat de Andres Iniesta a l'edat promig dels esportistes que 
practiquen Futbol. El valor promig ha d'estar arrodonit al valor enter 
immediatament superior. */
UPDATE deportistas
SET edad = (SELECT CEIL(AVG(edad)) FROM deportistas)
WHERE nombre = 'Andres Iniesta';

/* 9. Esborrar a l'esportista Andres Iniesta. Si estiguessin implementades les 
restriccions definides al principi de l’enunciat, es podria realitzar aquesta 
acció? Raona el perquè. */
DELETE FROM deportistas
WHERE nombre = 'Andres Iniesta';

-- RESPOSTA: Si, ja que no hi han normes especifiques sobre esborrar files


/* 10. Esborreu la competició “MotoGP”. Si s’apliquessin les restriccions 
definides al principi de l’enunciat, indica els canvis que es donarien sobre 
la base de dades, a part de l'eliminació d'aquesta competició. */
DELETE FROM competiciones
WHERE nombre = 'MotoGP';


/* 11. Esborreu de la taula GUANYADORS els esportistes que no existeixen a 
la taula DEPORTISTAS. */
DELETE FROM guanyadors
WHERE codi_guanyador NOT IN (SELECT deportista_id FROM deportistas);