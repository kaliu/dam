-- 1.(0.5 PUNTS) Doneu d’alta el client amb nom NOM1 i cognom COGNOM1, codi 1, 
-- telèfon 666111222, sense email, DNI 12345678C, a la ciutat de MONTCADA i 
-- que viu al carrer PROGRÉS número 17. Ha de quedar en estat actiu.

INSERT INTO client
VALUES (1, 'NOM1', 'COGNOM1', null , 1, '12345678C', 'MONTCADA', 666111222, 'PROGRÉS', 17);

/* 2.(1 PUNT) Feu que hi hagi 2 còpies de la película CHARIOTS CONSPIRACY al 
videoclub HOLLYWOOD 2. La primera copia amb codi 1, l’altra de forma 
automàtica amb es codi lliure disponible següent. */
INSERT INTO copia
VALUES (1, 
       (SELECT pelicula_id FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY'),
       (SELECT videoclub_id FROM videoclub WHERE nom = 'HOLLYWOOD 2'));

INSERT INTO copia
VALUES ((SELECT MAX(copia_id) + 1 FROM copia),
        (SELECT pelicula_id FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY'),
        (SELECT videoclub_id FROM videoclub WHERE nom = 'HOLLYWOOD 2'));
        
/* 3.(1 PUNT) Associeu el gènere Drama i l'actor REESE KILMER a la pel·lícula CHARIOTS CONSPIRACY. */
INSERT INTO pelicula_genere
VALUES ((SELECT pelicula_id FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY'),
       (SELECT genere_id FROM genere WHERE nom = 'Drama'));

INSERT INTO pelicula_actor
VALUES ((SELECT actor_id FROM actor WHERE nom = 'REESE' AND cognom = 'KILMER'),
        (SELECT pelicula_id FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY'));

/* 4.(1 PUNT) Donar d'alta amb codi 1 el lloguer que gestiona el treballador Jon Stephens de la copia 1 de la pel·lícula 
CHARIOTS CONSPIRACY agafada pel client NOM1 COGNOM1. Poseu com data de lloguer el dia actual i com data de retorn prevista, 
7 dies més tard. La resta de columnes amb valor nul. */

INSERT INTO lloguer
VALUES (1,
        (SELECT current_date from dual),
        (SELECT copia_id FROM copia WHERE pelicula_id = (SELECT pelicula_id FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY') AND copia_id = 1),
        (SELECT client_id FROM client WHERE nom = 'NOM1' AND cognom = 'COGNOM1'),
        (SELECT current_date + 7 from dual),
        (SELECT treballador_id FROM treballador WHERE nom = 'Jon' AND cognom = 'Stephens'),
        null,
        null,
        null);

/* 5.(1 PUNT) El client NOM1 COGNOM1 torna la copia amb codi 1, 10 dies més tard de la data de lloguer.  
El rep Jon Stephens, que li fa pagar el número de dies que ha trigat en tornar-la multiplicat pel cost diari de la pel·lícula. 
Guardeu les dades anteriors. */
UPDATE lloguer
SET quantitat_pagada = (SELECT cost_diari FROM pelicula WHERE titol = 'CHARIOTS CONSPIRACY') * (data_retorn_real - data_lloguer),
    data_retorn_real = data_lloguer + 10,
    treballador_retorn_id = (SELECT treballador_id FROM treballador WHERE nom = 'Jon' AND cognom = 'Stephens');
    
/* 6.(1 PUNT) Poseu en estat inactiu a l'encarregat del videoclub HOLLYWOOD 1 */
UPDATE treballador
SET actiu = 0
WHERE treballador_id = (SELECT encarregat_id FROM videoclub WHERE nom = 'HOLLYWOOD 1');

/* 7.(1 PUNT) Assignar com preu de compra de les pel·lícules amb qualificació PG 
    el preu de compra més petit de les pel·lícules amb qualificació PG-13 */
UPDATE pelicula
SET preu_compra = (SELECT MIN(preu_compra) FROM pelicula WHERE qualificacio = 'PG-13')
WHERE qualificacio = 'PG';

/* 8.(1 PUNT) Incrementeu un 15% el cost diari de les pel·lícules amb duració major de 100 minuts, fetes en ANGLES. */
UPDATE pelicula
SET cost_diari = cost_diari + (cost_diari *15/100)
WHERE duracio_pelicula > 100 AND idioma_id = (SELECT idioma_id FROM idioma WHERE nom = 'ANGLES');

/* 9.(1 PUNT) Eliminar els treballadors que ni son encarregats ni han gestionat lloguers ni han gestionat retorns. */
DELETE FROM treballador
WHERE treballador_id NOT IN (SELECT encarregat_id FROM videoclub) AND
treballador_id NOT IN (SELECT treballador_lloguer_id FROM lloguer) AND
treballador_id NOT IN (SELECT treballador_retorn_id FROM lloguer);

/* 10.(1 PUNT) Esborreu la participació de l'actor REESE KILMER a qualsevol pel·lícula amb qualificació PG. */
DELETE FROM pelicula_actor
WHERE actor_id = (SELECT actor_id FROM actor WHERE nom = 'REESE' AND cognom = 'KILMER') AND
      pelicula_id = ANY (SELECT pelicula_id FROM pelicula WHERE qualificacio = 'PG');

/* 11.(1 PUNT) Crear la taula RESUM_GENERE amb la següent sentencia: */
  CREATE TABLE RESUM_GENERES 
   (	GENERE_ID NUMBER(*,0), 
        NOM VARCHAR2(25), 
        TOTAL_PELICULES NUMBER
   ) ;
   
/* La taula anterior guarda la quantitat de pel·lícules que té cada gènere.
Inserir amb una única sentència SQL les files de la nova taula. */
INSERT INTO resum_generes
SELECT g.genere_id, g.nom, COUNT(p.pelicula_id)
FROM genere g
FULL JOIN pelicula_genere pg ON (pg.genere_id = g.genere_id)
FULL JOIN pelicula p ON (p.pelicula_id = pg.pelicula_id)
GROUP BY  g.genere_id, g.nom;

/* 12.(0.5 PUNTS) Eliminar els generes que no s'utilitzen a cap pel.licula. */

DELETE FROM genere
WHERE genere_id NOT IN (SELECT genere_id FROM pelicula_genere);