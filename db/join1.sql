--1. Mostrar el nom i l'adre�a de correu dels esportistes que practiquin "Tenis"
SELECT d.nombre, d.email
FROM deportistas d
CROSS JOIN deportes s
WHERE s.nom_depor = 'Tenis';
--2. Mostra el nom de cada esport i el nom dels esportistes que els practiquen, ordenats per nom d'esport ascendent.
SELECT s.nom_depor, d.nombre
FROM deportes s
CROSS JOIN deportistas d
ORDER BY s.nom_depor ASC;
--3. Mostrar el nom de cada esportista amb els noms de la competicions que hagi guanyat i l'any en que ho va fer.
SELECT d.nombre, v.competicion_id, v.anyo_victoria
FROM deportistas d
INNER JOIN victorias v USING (deportista_id);
--4. Mostra el nom i l'edat dels entrenadors amb el nom dels esportistes que cadasc� entrena.
SELECT d.nombre, d.edad, de.nombre AS nombre_entrenador
FROM deportistas d
JOIN deportistas de ON (d.entrenador_id = de.deportista_id);

--5. Mostrat TOTS els esportistes, amb el nom de l'esport que practiquin. Han de sortir tots, fins i tot els que no tenen un esport assignat.
SELECT d.nombre, s.nom_depor
FROM deportistas d
FULL OUTER JOIN deportes s USING (deporte_id);
--6. Mostrar el nom de l'etapa de la vida a la que es troben Gerard Pique i Andres Iniesta.
SELECT d.nombre, d.edad, e.nombre AS etapa
FROM deportistas d
JOIN etapasvida e 
ON(d.edad >= e.desde AND d.edad <= e.hasta)
WHERE d.deportista_id = 2 OR d.deportista_id = 3;
--7. Mostrat TOTS els noms d'esports amb el nom dels esportistes que els practiquin. 
--Han de sortir tots els esports, fins i tot els que no tinguin esportistes assignats.
SELECT s.nom_depor, d.nombre 
FROM deportes s
FULL OUTER JOIN deportistas d USING (deporte_id);
--8. Mostrar el noms dels esports als quals no es poden associar esportistes
SELECT s.nom_depor AS deportes_sin_jugadores
FROM deportistas d
RIGHT OUTER JOIN deportes s ON (d.deporte_id = s.deporte_id)
WHERE d.deporte_id IS NULL;
--9. Mostrar els noms i edats dels esportistes, l'edat dels quals sigui menor que la de Marc Marquez.
SELECT d.nombre, d.edat
FROM deportistas d
WHERE d.edad < 36;
--10. Mostrar el nom i l'edat dels esportistes als quals no es pot associar un esport.
SELECT d.nombre, d.edad
FROM deportistas d
LEFT OUTER JOIN deportes s 
ON (d.deporte_id = s.deporte_id)
WHERE s.deporte_id IS NULL;