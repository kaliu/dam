/* 1. Demanar l'identificador d'una comanda i mostrar 
la informació associada a cada línia de la comanda 
(item): codi del producte comprat, descripció del 
producte, preu de la unitat, quantitat i total pagat.*/
SELECT p.pedido_no, i.item_no, i.producto_no, i.cantidad, i.importe_uni 
FROM Pedidos p
INNER JOIN Item i ON (p.pedido_no = i.pedido_no)
WHERE i.pedido_no = &pedidoNum;

/* 2. Mostrar per cada comanda el codi, 
la data de la comanda, la data d'enviament, 
el cognom del treballador que l’ha realitzada 
i el nom del client que l’ha adquirida. */
SELECT p.pedido_no, p.fecha_pedido, p.fecha_envio, e.apellido, c.nombre
FROM Pedidos p
INNER JOIN Clientes c ON (c.cliente_no = p.cliente_no)
INNER JOIN Emple e ON (c.vendedor_no = e.emp_no);

/* 3. Mostrar pel venedor anomenat ‘TOVAR’ quina quantitat 
de diners ha recaptat en total. */
SELECT SUM(i.importe_uni * i.cantidad) AS GENERADO_POR_TOVAR
FROM emple e
INNER JOIN clientes c ON (c.vendedor_no = e.emp_no)
INNER JOIN pedidos p ON (p.cliente_no = c.cliente_no)
INNER JOIN item i ON (i.pedido_no = p.pedido_no)
WHERE e.apellido = 'TOVAR';

/* 4.  Mostrar el codi, nom , localitat i província dels 
clients (sense repetir) que han realitzar 
comandes l’any 2001. La sortida s'ha de mostrar ordenada 
per codi de client.*/
SELECT UNIQUE c.cliente_no, c.nombre, c.localidad, c.provincia, p.fecha_pedido
FROM clientes c
LEFT JOIN pedidos p ON (p.cliente_no = c.cliente_no)
WHERE TO_CHAR(p.fecha_pedido, 'yyyy') = 2001
ORDER BY c.cliente_no;

/* 5. Mostrar les dades dels clients que no han fet cap 
comanda.*/
SELECT c.*
FROM clientes c
LEFT JOIN pedidos p ON (c.cliente_no = p.cliente_no)
WHERE p.cliente_no IS NULL;

/* 6. Mostrar el nom, localitat, província i telèfon dels 
clients que algun cop han comprat els productes 
'ARCHIVADOR CEREZO' o 'MESA DIRECTOR'. */
SELECT DISTINCT c.nombre, c.localidad, c.provincia, c.telefono
FROM clientes c 
INNER JOIN pedidos p ON (p.cliente_no = c.cliente_no)
INNER JOIN item i ON (i.pedido_no = p.pedido_no)
INNER JOIN productos pr ON (i.producto_no = pr.producto_no)
WHERE pr.descripcion = 'MESA DIRECTOR' OR pr.descripcion = 'ARCHIVADOR CEREZO';

/* 7. Mostrar la descripció, el preu actual i 
les unitats en stock dels productes que no s'han venut mai.*/
SELECT pr.descripcion, pr.precio_actual, pr.stock_disponible
FROM productos pr
LEFT JOIN item i ON (i.producto_no = pr.producto_no)
WHERE i.producto_no IS NULL;

/* 8. Mostrar el codi i el cognom dels empleats 
que han venut alguna vegada una cadira. */
SELECT DISTINCT e.emp_no, e.apellido
FROM emple e
JOIN clientes c ON (e.emp_no = c.vendedor_no)
JOIN pedidos p ON (c.cliente_no = p.cliente_no)
JOIN item i ON (i.pedido_no = p.pedido_no)
INNER JOIN productos pr ON (pr.producto_no = i.producto_no)
WHERE pr.descripcion LIKE 'SILLA%';

/* 9. Mostrar el número de comandes realitzades 
i el total recaptat a l’any 2002 . */
SELECT COUNT(p.pedido_no), SUM(i.importe_uni)
FROM item i
RIGHT JOIN pedidos p ON (p.pedido_no = i.pedido_no)
WHERE TO_CHAR(p.fecha_pedido, 'yyyy') = '2002';

/* 10. Mostrar el codi, nom i telèfon dels clients 
(sense repetir) que han realitzat comandes on 
s'ha trigat més de 4 dies en enviar els productes. */
SELECT UNIQUE c.cliente_no, c.nombre, c.telefono
FROM clientes c
INNER JOIN pedidos p ON (p.cliente_no = c.cliente_no)
WHERE (p.fecha_envio - p.fecha_pedido) > 4;

/* 11. Mostrar el codi i cognom dels empleats que 
han venut alguna comanda en dimecres. Feu la consulta 
sense utilitzar el nom del dia "dimecres" (o miercoles) 
per evitar que la consulta depengui del vostre idioma. */
SELECT UNIQUE e.emp_no, e.apellido
FROM emple e
JOIN clientes c ON (e.emp_no = c.vendedor_no)
JOIN pedidos p ON (c.cliente_no = p.cliente_no)
WHERE TO_CHAR(p.fecha_pedido, 'd') = 3;
