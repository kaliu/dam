/*    1. Mostrar la llista de les peces subministrades pels diferents fabricants (Nom peça, Nom fabricant) 
ordenada per nom de peça i nom de fabricant. */
SELECT p.pnombre, f.fnombre
FROM piezas p
INNER JOIN fab_piezas fp ON (p.codigo = fp.codigo) 
INNER JOIN fabricantes f ON (f.fid = fp.fid)
ORDER BY p.pnombre, f.fnombre;

/*    2. Mostrar les diferents ciutats a les que pertanyen els fabricants de la peça Bolt ordenades per nom, 
de forma descendent. */
SELECT f.ciudad
FROM fabricantes f
JOIN fab_piezas fp ON (fp.fid = f.fid)
JOIN piezas p ON (p.codigo = fp.codigo)
WHERE p.pnombre = 'Bolt'
ORDER BY f.ciudad DESC;
/*    3. Mostrar les diferents ciutats on tenim peces, ordenades de forma ascendent. */
SELECT DISTINCT f.ciudad
FROM fabricantes f
JOIN fab_piezas fp ON (fp.fid = f.fid)
JOIN piezas p ON (p.codigo = fp.codigo)
WHERE fp.cantidad > 0;


/*    4. Mostrar la llista de les diferents peces subministrades pel proveïdor Smith junt amb la quantitat 
subministrada, ordenades de forma descendent per la quantitat.*/
SELECT p.*, fp.cantidad
FROM piezas p
INNER JOIN fab_piezas fp ON (p.codigo = fp.codigo) 
INNER JOIN fabricantes f ON (f.fid = fp.fid)
WHERE f.fnombre = 'Smith'
ORDER BY fp.cantidad DESC;

/*    5. Mostrar el nom dels fabricants, el nom de la peça i la quantitat per aquells subministres de més de 
200 peces. Ordena el resultat per nom del fabricant ascendent, nom de la peça ascendent i quantitat descendent.*/
SELECT f.fnombre, p.pnombre, fp.cantidad
FROM fabricantes f
JOIN fab_piezas fp ON (fp.fid = f.fid)
JOIN piezas p ON (p.codigo = fp.codigo)
WHERE fp.cantidad > 200
ORDER BY f.fnombre, p.pnombre, fp.cantidad DESC;
/*    6. Mostrar el nom de les diferents peces subministrades pels fabricants de London.*/
SELECT DISTINCT p.pnombre
FROM piezas p
JOIN fab_piezas fp ON (p.codigo = fp.codigo) 
JOIN fabricantes f ON (f.fid = fp.fid)
WHERE f.ciudad = 'London';

/*    7. Mostrar les dades dels fabricants de London que subministren peces de color 'blue'.*/
SELECT f.*
FROM fabricantes f
JOIN fab_piezas fp ON (fp.fid = f.fid)
JOIN piezas p ON (p.codigo = fp.codigo)
WHERE p.color = 'Blue' AND f.ciudad = 'London';
/*    8. Mostrar el nom de les peces i el nom del seu fabricant per aquelles peces que siguin de color 'red'.*/
SELECT p.pnombre, f.fnombre
FROM piezas p
JOIN fab_piezas fp ON (p.codigo = fp.codigo) 
JOIN fabricantes f ON (f.fid = fp.fid)
WHERE p.color = 'Red';
/*    9. Mostrar el nom i el color de les peces subministrades per fabricants que siguin de Paris.*/
SELECT p.pnombre, p.color
FROM piezas p
JOIN fab_piezas fp ON (p.codigo = fp.codigo) 
JOIN fabricantes f ON (f.fid = fp.fid)
WHERE f.ciudad = 'Paris';
/*    10. Mostrar la llista amb la identitat, el nom i la ciutat de TOTS els fabricants i el codi de les peces 
que subministren. Han d'aparèixer tots els fabricants encara que no ens hagin subministrat cap peça.*/

/*    11. Mostrar la llista de TOTES les peces (codi, nom i color) juntament amb la identitat del fabricant que 
la subministra, ordenades per codi ascendent. Han d'aparèixer totes les peces encara que no els subministri cap proveïdor.*/ 

/*    12. Mostrar la llista amb la identitat, el nom i la ciutat de TOTS els fabricants i el nom de les peces 
que subministren, ordenats per identitat de fabricant.*/ 


