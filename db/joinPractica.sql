-- 1. Mostrar el nom dels idiomes que no s'han utilitzat a cap pel·lícula.
SELECT i.nom
FROM idioma i
LEFT JOIN pelicula p ON (p.idioma_id = i.idioma_id)
WHERE p.idioma_id IS NULL;

-- 2. Demanar el títol d'una pel·lícula i mostrar el nom i cognom dels
-- actors que hagin treballat, ordenats alfabèticament per cognom i nom dels actors. */
SELECT a.nom, a.cognom
FROM actor a
JOIN Pelicula_actor pa ON (a.actor_id = pa.actor_id)
JOIN pelicula p ON (p.pelicula_id = pa.pelicula_id)
WHERE p.titol = UPPER('&titol_peli')
ORDER BY a.cognom, a.nom;

-- 3. Demanar el títol d'una pel·lícula i mostrar el nom i 
-- cognom dels actors que hagin treballat, ordenats alfabèticament per cognom i nom dels actors. */
SELECT COUNT(ll.lloguer_id)
FROM lloguer ll
JOIN copia cp ON (ll.copia_id = cp.copia_id) 
JOIN pelicula p ON (cp.pelicula_id = p.pelicula_id)
WHERE p.qualificacio = 'R';

-- 4. Mostrar per cada pel·lícula estrenada entre els anys 2006 i 2008 (inclosos) el seu títol, 
-- l'any d'estrena i el seu idioma, ordenats per any d'estrena (de més antic a més recent) 
-- i alfabèticament pel títol.
SELECT p.titol, p.any_estrena, i.nom
FROM pelicula p
JOIN idioma i ON (i.idioma_id = p.idioma_id)
WHERE p.any_estrena BETWEEN 2006 AND 2008
ORDER BY p.any_estrena, titol;

-- 5. Mostrar el nom, el cognom i el DNI dels clients que viuen a 
-- la mateixa ciutat que el client amb DNI 33344381H.
SELECT c.nom, c.cognom, c.dni
FROM client c
JOIN client cc ON (cc.dni = '33344381H')
WHERE c.ciutat = cc.ciutat;


-- 6. Mostrar el títol i la data de lloguer de la primera pel·lícula llogada que no ha estat retornada.
SELECT p.titol, ll.data_lloguer
FROM lloguer ll
JOIN copia cp ON (ll.copia_id = cp.copia_id) 
JOIN pelicula p ON (cp.pelicula_id = p.pelicula_id)
WHERE ll.data_retorn_real IS NULL
FETCH FIRST 1 ROWS ONLY;

-- 7. Indica el nom i cognoms dels treballadors que alguna vegada han tramitat el 
-- lloguer de la copia d'una pel·lícula, però després no han gestionat el seu retorn.
SELECT DISTINCT t.nom, t.cognom
FROM treballador t
JOIN lloguer ll ON (t.treballador_id = ll.treballador_lloguer_id)
WHERE ll.treballador_lloguer_id != ll.treballador_retorn_id;

-- 8. Mostrar el nom i el cognom dels clients que han trigat més de 2 dies del que estava 
-- previst en retornar una copia. El resultat s'ha de mostrar ordenat alfabèticament per cognom i nom.
SELECT DISTINCT c.nom, c.cognom
FROM client c
JOIN lloguer ll ON (ll.client_id = c.client_id)
WHERE ll.data_retorn_real NOT BETWEEN ll.data_retorn_prevista AND ll.data_retorn_prevista + 2
ORDER BY c.cognom ASC, c.nom DESC;

-- 9. Demanar el nom d'un gènere i mostrar el títol, l'any d'estrena i 
-- la qualificació de les pel·lícules d'aquest gènere, ordenades per any 
-- d'estrena (de més recent a més antiga) i alfabèticament pel títol. 
SELECT p.titol, p.any_estrena, p.qualificacio
FROM pelicula p
JOIN pelicula_genere pg ON (pg.pelicula_id = p.pelicula_id)
JOIN genere g ON (g.genere_id = pg.genere_id)
WHERE g.nom = INITCAP('&genere')
ORDER BY p.any_estrena DESC, p.titol;

-- 10. Mostrar el nom i el cognom dels treballadors que mai han gestionat el retorn d'una copia.
SELECT t.nom, t.cognom
FROM treballador t
LEFT JOIN lloguer ll ON (t.treballador_id = ll.treballador_retorn_id)
WHERE ll.treballador_retorn_id IS NULL;

-- 11. Mostrar el nom i el cognom dels treballadors que mai han gestionat el retorn d'una copia.
SELECT t.nom, t.cognom
FROM treballador t
JOIN videoclub v ON (t.videoclub_id = v.videoclub_id)
WHERE v.nom = 'HOLLYWOOD 2';

-- 12.Demanar un nom d'idioma i mostrar el nom i el cognom dels treballadors que han 
--llogat alguna pel·lícula que utilitzi aquest idioma. La sortida s'ha de mostrar ordenada 
--alfabèticament per cognom i nom del treballador. 
SELECT DISTINCT t.nom, t.cognom
FROM treballador t
JOIN lloguer ll ON (t.treballador_id = ll.treballador_lloguer_id)
JOIN copia c ON (c.copia_id = ll.copia_id)
JOIN pelicula p ON (p.pelicula_id = c.pelicula_id)
JOIN idioma i ON (i.idioma_id = p.idioma_id)
WHERE i.nom = UPPER('&idiom');

-- 13.Demanar un nom i cognom de client i mostrar els generes de les pel·lícules que ha llogat, ordenats alfabèticament
SELECT DISTINCT g.nom
FROM genere g
JOIN pelicula_genere pg ON (pg.genere_id = g.genere_id)
JOIN pelicula p ON (p.pelicula_id = pg.pelicula_id)
JOIN copia cp ON (cp.pelicula_id = p.pelicula_id)
JOIN lloguer ll ON (ll.copia_id = cp.copia_id)
JOIN client c ON (c.client_id = ll.client_id)
WHERE c.nom = UPPER('&nom') AND c.cognom = UPPER('&cognom')
ORDER BY g.nom;

-- 14. Demanar un nom i un cognom d'actor i mostrar els noms dels videoclubs on hi han pel·lícules on hagi treballat aquest actor.
SELECT DISTINCT v.nom
FROM videoclub v
JOIN copia c ON (c.videoclub_id = v.videoclub_id)
JOIN pelicula p ON (p.pelicula_id = c.pelicula_id)
JOIN pelicula_actor pa ON (pa.pelicula_id = p.pelicula_id)
JOIN actor a ON (a.actor_id = pa.actor_id)
WHERE a.nom = UPPER('&actornom') AND a.cognom = UPPER('&actorcognom');

-- 15. Realitzar una consulta que mostri per cada client una línea com la següent:
-- El client RAFAEL ABNEY ha llogat l'any 2005 la pel·lícula FREDDY STORM al videoclub HOLLYWOOD 1 
-- El resultat s'ha de mostrar ordenat alfabèticament pel cognom i el nom del client.
SELECT 'El client ' || c.nom || ' ' || c.cognom || ' ha llogat l,any ' || TO_CHAR(ll.data_lloguer, 'yyyy') || ' la pel·licula ' || p.titol || ' al videoclub ' || v.nom
FROM client c
JOIN lloguer ll ON (ll.client_id = c.client_id)
JOIN copia cp ON (cp.copia_id = ll.copia_id)
JOIN pelicula p ON (p.pelicula_id = cp.pelicula_id)
JOIN videoclub v ON (v.videoclub_id = cp.videoclub_id)
ORDER BY c.cognom, c.nom;

