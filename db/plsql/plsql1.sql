SET SERVEROUTPUT ON;
SET VERIFY OFF;


--IMPORTANT:
-- - Mentres no es digui el contrari, els exercicis PLSQL es fan amb els vostre
--   usuari XYZ2019, que heu creat al vostre servidor ORACLE local durant la instal.laci�.
-- - Si es treballa amb la m�quina virtual, el teu l'usuari XYZ2019 l'has de
--   crear.

-- - Si no es disposa ni de servidor ORACLE local ni m�quina virtual, es pot utilitzar
--   el servidor ORACLE de l'institut. L'incovenient es que quan a algun exercici
--   es demani treballar connectat amb l'usuari SYSTEM, no es podr� fer.

--IMPORTANT:
--Connectat amb l'usuari XYZ2019, abans de comen�ar els exercicis, s'han d'executar
--els scripts situats a la carpeta del Moodle anomenada:
--"Material Complementari: Fitxers SQL per treballar amb PL/SQL"


/* EXERCICI 1:
Realitzeu un programa que demani un codi de producte per teclat i mostri les dades
seg�ents: codi, descripci� i preu actual.
Si el producte no existeix s'ha de mostrar el missatge:
'ERROR: Producte no existent'.
*/

DECLARE

    v_codi_in FLOAT := '&introdueix_codi';
    v_codi productos.producto_no%TYPE;
    v_descripcio productos.descripcion%TYPE;
    v_precio_actual productos.precio_actual%TYPE;

BEGIN

    SELECT producto_no, descripcion, precio_actual
    INTO v_codi, v_descripcio, v_precio_actual
    FROM productos
    WHERE producto_no = v_codi_in;

    DBMS_OUTPUT.PUT_LINE ('Codi: ' || v_codi || ', Descripci�: ' || v_descripcio || ', Precio: ' || v_precio_actual || '�.');

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Error: Producte no existent');

    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error no identificat.');
END;

/* EXERCICI 2:
Programa que demanani per teclat un codi de producte, la
descripci� i el preu d'un producte i el doni d'alta (l'insereixi) a la base de dades.
El valor de stock disponible ha de ser 0.

Una vegada inserit el producte, s'ha de mostrar un missatge semblant al seg�ent:
'Inserit el producte amb codi XXXX, descripci� YYYYY i preu actual ZZZZ'
on XXXX, YYYY i ZZZZ s�n els valors del codi, descripci� i preu

Si s'intenta inserir amb un codi que ja es troba a la base de dades, es produir�
l'excepci� DUP_VAL_ON_INDEX. En aquest cas s'hauria de mostrar el missatge
'ERROR: Valor Duplicat'
*/

DECLARE

    v_codi_in productos.producto_no%TYPE := '&introdueix_codi';
    v_descripcio_in productos.descripcion%TYPE := '&introdueix_descripcio';
    v_preu_in productos.precio_actual%TYPE := '&introdueix_preu';

BEGIN

    INSERT INTO productos
    VALUES (v_codi_in, v_descripcio_in, v_preu_in, 0);

    DBMS_OUTPUT.PUT_LINE('Inserit el producte amb codi '||v_codi_in||', descripci� '||v_descripcio_in||' i preu actual '||v_preu_in);

    COMMIT;

EXCEPTION

    WHEN DUP_VAL_ON_INDEX THEN
        DBMS_OUTPUT.PUT_LINE ('ERROR: Valor Duplicat');

    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('ERROR: Error indefinit');

END;

/* EXERCICI 3:
Programa que demanani per teclat un codi de producte i un valor de stock disponible,
i canvii el stock del producte al valor indicat (nom�s si aquest �s major o igual que 0)
Una vegada canviat, s'ha de mostrar el missatge seg�ent:
'El producte amb codi XXXX te un stock disponible de YYYYY'

Si no es troba el producte, s'haur� de mostrar un missatge indicant
'ERROR: Producte no existent'
Si el valor de stoc introduit �s menor que 0 s'ha de mostrar el missatge
'ERROR: el valor ha de ser major o igual que 0'
*/

DECLARE

    v_codi_in productos.producto_no%TYPE := '&introdueix_codi';
    v_stock_in productos.stock_disponible%TYPE := '&introdueix_stock';

    r_productos productos%ROWTYPE;

BEGIN

    SELECT producto_no, stock_disponible
    INTO r_productos.producto_no, r_productos.stock_disponible
    FROM productos
    WHERE producto_no = v_codi_in;

    IF v_stock_in >= 0 THEN
        UPDATE productos
        SET stock_disponible = v_stock_in
        WHERE producto_no = v_codi_in;

        DBMS_OUTPUT.PUT_LINE ('El producte amb codi '||r_productos.producto_no||' te un stock disponible de '||r_productos.stock_disponible);

        COMMIT;

    ELSE
        DBMS_OUTPUT.PUT_LINE ('ERROR: el valor ha de ser major o igual que 0');
    END IF;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ERROR: Producte no existent.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('ERROR: Error indefinit.');

END;

/* EXERCICI 4:
Programa que demana un nom de departament per teclat i actualitza la columna NUM_TREBALLADORS
de la taula DEPART amb la quantitat d'empleats que t� el departament.
Si el departament no te empleats, s'ha d'actualitzar la columna a 0.

S'ha de mostrar per pantalla el codi i el nom del departament, i la quantitat d'empleats.
Si el departament no existeix, ha de mostrar un missatge indicant-lo. */

DECLARE

    v_depart_in depart.dnombre%TYPE := '&nom_depart';
    r_depart depart%ROWTYPE;

    v_count_emples NUMBER;
BEGIN

    SELECT dept_no
    FROM depart
    WHERE dnombre = v_depart_in

    SELECT COUNT(emp_no)
    INTO v_count_emples
    FROM emple
    WHERE dnombre = v_depart_in;

    UPDATE depart
    SET num_treballadors = v_count_emples
    WHERE dnombre = v_depart_in;

    SELECT dept_no, dnombre, num_treballadors
    INTO r_depart.dept_no, r_depart.dnombre, r_depart.num_treballadors
    FROM depart
    WHERE dnombre = v_depart_in;

    DBMS_OUTPUT.PUT_LINE('El departament '||r_depart.dnombre||' amb codi '||r_depart.dept_no||' te '||r_depart.num_treballadors||' treballadors.');

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ERROR: El departament no existeix');

END;

/* EJERCICIO 5:
Programa que demana un NIF de client i un valor de limit de cr�dit per teclat.
Canviar el limit de credit al valor introdu�t per teclat.
El programa ha de permitir nom�s assignar nous valors que estiguien entre el
m�s petit i el m�s gran dels que hi ha a la taula.
- Despr�s de assignar el nou valor, s'ha de mostrar el missatge:
'El client amb nom XXXXX ha canviat el seu l�mit de YYYY a ZZZZ'
on YYYY �s el valor antic i ZZZZ el valor nou.
- Si el client no existeix ha de mostrar el missatge: 'Client no existent'.
- Si s'intenta assignar un valor que no acompleix la condici� anterior, s'ha de
mostrar el missatge: 'ERROR: No est� perm�s assignar aquest valor'
*/

DECLARE

    v_nif_in clientes.cliente_nif%TYPE := '&nif_cliente';
    v_limit_credit_in clientes.limite_credito%TYPE := '&limite_credito';

    v_lcredit_min NUMBER;
    v_lcredit_max NUMBER;
    v_lcredit_origin NUMBER;

    r_cliente clientes%ROWTYPE;

BEGIN

    SELECT MIN(limite_credito), MAX(limite_credito)
    INTO v_lcredit_min, v_lcredit_max
    FROM clientes;

    SELECT cliente_nif, limite_credito
    INTO r_cliente.cliente_nif, v_lcredit_origin
    FROM clientes
    WHERE cliente_nif = v_nif_in;

    IF v_limit_credit_in > v_lcredit_min AND v_limit_credit_in < v_lcredit_max THEN
        UPDATE clientes
        SET limite_credito = v_limit_credit_in
        WHERE cliente_nif = v_nif_in;

        COMMIT;

        DBMS_OUTPUT.PUT_LINE ('El client amb nom '||r_cliente.cliente_nif||' ha canviat el seu l�mit de '||v_lcredit_origin||' a '||v_limit_credit_in);

    ELSE
        DBMS_OUTPUT.PUT_LINE ('ERROR: No est� perm�s assignar aquest valor');
    END IF;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('ERROR: Client no trobat');

END;


/*EXERCICI 6:
programa que demana un codi d'empleat i omple
la columna asterisc de la taula emple segons la seg�ent taula

OFICI	                ASTERISC
VENDEDOR	                **
ANALISTA	                ***
DIRECTOR                    ****
PRESIDENTE                  *****

Si l'empleat t� qualsevol altre ofici, s'assinar� nom�s 1 asterisc.
Si l'empleat no t� ofici, s'assignar� a asterisc el valor NULL.

Una vegada assignat el valor a la columna, es mostrar� el seg�ent missatge
depenent de si te ofici o no:
- Si l'empleat t� ofici:
'L'empleat amb cognom XXXXX i ofici YYYYY t� aquests asteriscos ZZZZZZ'
- Si l'empleat no t� ofici:
'L'empleat amb cognom XXXXX no te ofici ni asteriscos'

Si l'empleat no existeix, s'ha de mostrar un missatge indicant-lo

*/

DECLARE

    v_codi_emple_in emple.emp_no%TYPE := '&codi_emple';
    v_asteriscs VARCHAR2(5);

    r_emple emple%ROWTYPE;

BEGIN

    SELECT oficio, apellido
    INTO r_emple.oficio, r_emple.apellido
    FROM emple
    WHERE emp_no = v_codi_emple_in;

    CASE r_emple.oficio
        WHEN 'VENDEDOR' THEN v_asteriscs := '**';
        WHEN 'ANALISTA' THEN v_asteriscs := '***';
        WHEN 'DIRECTOR' THEN v_asteriscs := '****';
        WHEN 'PRESIDENTE' THEN v_asteriscs := '*****';
        WHEN IS NULL THEN v_asteriscs := NULL;
        ELSE v_asteriscs := '*';
    END CASE;

    UPDATE emple
    SET asterisc = v_asteriscs
    WHERE emp_no = v_codi_emple_in;

    COMMIT;

    IF r_emple.oficio IS NULL THEN
        DBMS_OUTPUT.PUT_LINE ('L empleat amb cognom '||r_emple.apellido||' no te ni ofici ni asteriscos.');
    ELSE
        DBMS_OUTPUT.PUT_LINE ('L empleat amb cognom '||r_emple.apellido||' i ofici '||r_emple.oficio||' t� aquests asteriscos '||v_asteriscs);
    END IF;

END;
