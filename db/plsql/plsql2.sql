SET SERVEROUTPUT ON;
SET VERIFY OFF;

/* EXERCICI 1: 
Programa que demani un n�mero per teclat i generi un quadrat
amb tantes files i columnes com el n�mero introdu�t.
Realitzar totes les comprovacions necessaries sobre el n�mero 
introdu�t per que no hagin errors (si detecteu un error, mostreu
missatge informant)
Per exemple, si el n�mero introdu�t �s 5
s'hauria de mostrar el seg�ent:
* * * * * 
* * * * * 
* * * * * 
* * * * * 
* * * * * 


*/

DECLARE
    v_numero NUMBER := '&num';
    v_contador1 INTEGER := 0;
    v_contador2 INTEGER := 0;
    
BEGIN
    WHILE v_contador1 < v_numero
    LOOP
        v_contador1 := v_contador1 + 1;
        
        WHILE v_contador2 < v_numero
        LOOP
            DBMS_OUTPUT.PUT('*');
            v_contador2 := v_contador2 + 1;
        END LOOP;
        v_contador2 := 0;
        DBMS_OUTPUT.NEW_LINE;

    END LOOP;
END;



--EXERCICI 2: 
/*Programa que mostri el nom de cada client i el cognom del venedorque te assignat. La consulta que realitzis ha d'anar dins un cursor
anomenat c_client_venedor.
Defineix una excepci� propia anomenada e_client_sense_venedor
que es generi al detectar que hi ha algun client sense venedor associat.
En aquest cas, el programa ha de mostrar el missatge 
"El client XXXXX no te assignat un venedor"
i acabar.
*/

DECLARE
    v_nombre clientes.nombre%TYPE;
    v_apellido_ve emple.apellido%TYPE;

    e_client_sense_venedor EXCEPTION;
    
    v_contador INTEGER := 0;
    
    CURSOR c_client_venedor IS
        SELECT c.nombre as nombre_cliente, e.apellido as apellido_vendedor
        FROM clientes c
        LEFT OUTER JOIN emple e ON (c.vendedor_no = e.emp_no);

BEGIN
    OPEN c_client_venedor;
    
    LOOP
        FETCH c_client_venedor INTO v_nombre, v_apellido_ve;
        EXIT WHEN (c_client_venedor%NOTFOUND);
        
        IF (v_apellido_ve IS NULL) THEN
            RAISE e_client_sense_venedor;
        ELSE
            DBMS_OUTPUT.PUT_LINE ('El client '||v_nombre||' t� asignat el venedor '||v_apellido_ve);
        END IF;
        
    END LOOP;
EXCEPTION
    WHEN e_client_sense_venedor THEN
        DBMS_OUTPUT.PUT_LINE ('El client '||v_nombre||' no te assignat un venedor');
END;


--EXERCICI 3 (XXX): 
/*Crear un programa que assigni per cada departament la quantitat de treballadors 
que t�.
Una vegada fets TOTS els canvis necesaris, el programa ha de mostrar el nom i la quantitat de 
treballadors de cada departament*/

DECLARE
    
    v_num_emple NUMBER;

BEGIN
    
    FOR r_depart IN (SELECT * FROM depart)
    LOOP
        SELECT COUNT(emp_no) INTO v_num_emple
        FROM emple
        WHERE dept_no = r_depart.dept_no;
        
        UPDATE depart
        SET num_treballadors = v_num_emple
        WHERE dept_no = r_depart.dept_no;

        DBMS_OUTPUT.PUT_LINE ('El departament '||r_depart.dnombre||' t� '||r_depart.num_treballadors||' treballadors.');

    END LOOP;
    
EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No existeix cap departament.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Error no definit');

END;

/* EXERCICI 4: 
Realitzar un programa que demani un valor de preu per teclat i mostri
el codi, la descripci� i el preu actual dels productes amb un preu major
que l'introdu�t. 
Si no hi ha cap, s'ha de mostrar el missatge: 
'No hi ha cap producte amb un preu major que XXXX'
on XXXX �s el preu introdu�t.

*/

DECLARE
    
    v_preu_in NUMBER := '&preu';
    v_num_columnes INTEGER;
    
BEGIN
    
    SELECT COUNT(*) INTO v_num_columnes
    FROM productos
    WHERE precio_actual > v_preu_in;
    
    IF v_num_columnes > 0 THEN
        FOR r_productos IN (SELECT producto_no, descripcion, precio_actual FROM productos WHERE precio_actual > v_preu_in)
        LOOP
                    
            DBMS_OUTPUT.PUT('Codi: '||r_productos.producto_no);
            DBMS_OUTPUT.PUT(' Descripci�: '||r_productos.descripcion);
            DBMS_OUTPUT.PUT_LINE(' Preu: '||r_productos.precio_actual);
        
        END LOOP;
    ELSE
        DBMS_OUTPUT.PUT_LINE('No hi ha cap producte amb un preu major que '||v_preu_in);
    END IF;        
END;

/* EXERCICI 5:   
Realitzar un programa que demani un valor de salari per teclat i mostri el cognom i el sou 
de l'empleat (o empleats) que cobrin el salari m�s proper al valor introdu�t*/

DECLARE
    v_sou_in NUMBER := '&sou';
    v_salario emple.salario%TYPE;
    v_apellido emple.apellido%TYPE;
    v_aux_men NUMBER;
    v_aux_maj NUMBER;
    v_ape_men emple.apellido%TYPE;
    v_ape_maj emple.apellido%TYPE;
    v_salario_max NUMBER;
    v_apellido_max emple.apellido%TYPE;
    
    CURSOR c_emple IS
        SELECT salario, apellido
        FROM emple 
        ORDER BY salario;
    
    
BEGIN
    -- Buscar valors maxims
    SELECT MAX(salario) INTO v_salario_max
    FROM emple;
    SELECT apellido INTO v_apellido_max
    FROM emple
    WHERE salario = v_salario_max;
    ------
    OPEN c_emple;
    
    -- Buscar els dos nombres mes propers al introduit
    LOOP
        FETCH c_emple INTO v_salario, v_apellido;
        
        IF v_salario <= v_sou_in THEN
            v_aux_men := v_salario;
            v_ape_men := v_apellido;
        END IF;
        IF v_salario >= v_sou_in THEN
            v_aux_maj := v_salario;
            v_ape_maj := v_apellido;
        END IF;
        IF v_sou_in > v_salario_max THEN
            v_aux_men := null;
        END IF;
        
        EXIT WHEN (v_aux_maj = v_salario OR v_sou_in > v_salario_max); 
    END LOOP;
    ------
    
    -- Output
    IF v_aux_maj IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('El salari ' ||v_sou_in|| ' �s major que tota la resta i el m�s proper �s de '
                            || v_apellido_max || ' amb un sou de ' || v_salario_max );
    ELSIF v_aux_men IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('El salari ' ||v_sou_in|| ' �s menor que tota la resta i el m�s proper �s de '
                            || v_ape_maj || ' amb un sou de ' || v_aux_maj );
    ELSE
        DBMS_OUTPUT.PUT_LINE('Els salaris m�s propers a ' ||v_sou_in|| ' s�n el de ' || v_ape_men || ' amb un sou de ' || v_aux_men
                            || ' i el de ' || v_ape_maj || ' amb un sou de ' || v_aux_maj );
    END IF;
    ------
    CLOSE c_emple;
END;

/* EXERCICI 6: 
IMPORTANT: Aquest programa s'ha de'executar des de la conexi� amb SYSTEM

Realitzar un programa que acceptant el nom d�un usuari per teclat, llisti la seg�ent 
informaci�:

- Llistat dels noms de les seves taules. 
Si no te cap, s'ha de mostrar el missatge "Aquest usuari no te cap taula"
- Llistat dels noms dels seus privilegis de sistema. 
Si no te cap, s'ha de mostrar el missatge "Aquest usuari no te cap privilegi de sistema"
- Llistat dels noms dels seus privilegis sobre objectes. 
Si no te cap, s'ha de mostrar el missatge "Aquest usuari no te cap privilegi sobre objectes"
- Llistat dels noms dels seus privilegis sobre columnes d'objectes. 
Si no te cap, s'ha de mostrar el missatge "Aquest usuari no te cap privilegi sobre columnes d'objectes"
- Llistat dels noms dels seus roles. 
Si no te cap, s'ha de mostrar el missatge "Aquest usuari no te cap rol"


Si l'usuari introdu�t no existeix, s'ha de mostrar un missatge indicant:
"Aquest usuari no existeix"

P�sta: El programa ha de fer servir les seg�ents vistes:
dba_users, dba_tables, dba_sys_privs, dba_tab_privs, dba_col_privs i dba_role_privs

El resultats s'ha de mostrar de forma similar a la seg�ent sortida:

LLISTAT DE DADES DE l'USUARI JFGB2019
------------------------------------------
Llistat de taules:
TAULA: RESUM_SUPERFICIE
TAULA: RESUM_GENERES
TAULA: TEST_TABLE
TAULA: ITEM
TAULA: PRODUCTOS
TAULA: PEDIDOS
TAULA: DEPART
TAULA: CLIENTES
TAULA: EMPLE
TAULA: DEPORTISTAS
TAULA: DEPORTES
------------------------------------------
Llistat de privilegis de sistema:
PRIVILEGI: UNLIMITED TABLESPACE
------------------------------------------
Llistat de privilegis sobre objectes:
PRIVILEGI: INSERT --> TAULA: DEPART
------------------------------------------
Llistat de privilegis sobre columnes d'objectes:
PRIVILEGI: UPDATE --> TAULA: DEPART   COLUMNA: LOC
------------------------------------------
Llistat de rols:
ROL: RESOURCE
ROL: CONNECT
------------------------------------------

*/


DECLARE
    v_username_in dba_users.username%TYPE := UPPER('&username');
    v_user_found BOOLEAN := false;
    v_num_files NUMBER := 0;
    e_user_not_found EXCEPTION;
    
BEGIN
    -- VERIFICACI� DEL USUARI
    FOR r_users IN (SELECT username
                    FROM dba_users)
    LOOP
        IF r_users.username = v_username_in THEN
            v_user_found := true;  
        END IF;
    END LOOP;
    
    IF v_user_found = false THEN
        RAISE e_user_not_found;
    END IF;
    ---------------
    
    DBMS_OUTPUT.PUT_LINE('LLISTAT DE DADES DE L USUARI ' || v_username_in );
    DBMS_OUTPUT.PUT_LINE('--------------------------------------');
    
    -- PRINTAR TAULES
    DBMS_OUTPUT.PUT_LINE('Llistat de taules:');
    SELECT COUNT(table_name) INTO v_num_files FROM dba_tables WHERE owner = v_username_in;
    
    IF v_num_files > 0 THEN
        FOR r_tables IN (SELECT table_name
                         FROM dba_tables
                         WHERE owner = v_username_in)
        LOOP
            DBMS_OUTPUT.PUT_LINE('TAULA: ' || r_tables.table_name);
        END LOOP;
    ELSE
        DBMS_OUTPUT.PUT_LINE('Aquest usuari no te cap taula.');
    END IF;
    -----------------
    
    DBMS_OUTPUT.PUT_LINE('--------------------------------------');
    
    -- PRINTAR PRIVILEGIS DEL SISTEMA
    DBMS_OUTPUT.PUT_LINE('Llistat de privilegis del sistema:');
    SELECT COUNT(privilege) INTO v_num_files FROM dba_sys_privs WHERE grantee = v_username_in;
    
    IF v_num_files > 0 THEN
        FOR r_sys_privs IN (SELECT privilege
                     FROM dba_sys_privs
                     WHERE grantee = v_username_in)
        LOOP
            DBMS_OUTPUT.PUT_LINE('PRIVILEGI: ' || r_sys_privs.privilege);
        END LOOP;
    ELSE
        DBMS_OUTPUT.PUT_LINE('Aquest usuari no te cap privilegi de sistema.');
    END IF;
    -----------------
    
    DBMS_OUTPUT.PUT_LINE('--------------------------------------');   
    
    -- PRINTAR PRIVILEGIS SOBRE LA TAULA
    DBMS_OUTPUT.PUT_LINE('Llistat de privilegis sobre objectes:');
    SELECT COUNT(privilege) INTO v_num_files FROM dba_tab_privs WHERE grantee = v_username_in;
    IF v_num_files > 0 THEN
        FOR r_tab_privs IN (SELECT privilege, table_name
                        FROM dba_tab_privs
                        WHERE grantee = v_username_in)
        LOOP
            DBMS_OUTPUT.PUT_LINE('PRIVILEGI: ' || r_tab_privs.privilege || '---> TAULA: ' || r_tab_privs.table_name);
        END LOOP;
    ELSE 
        DBMS_OUTPUT.PUT_LINE('Aquest usuari no te cap privilegi sobre objectes');
    END IF;
    -----------------
    
    DBMS_OUTPUT.PUT_LINE('--------------------------------------');   
    
    -- PRINTAR PRIVILEGIS SOBRE COLUMNES D'OBJECTES
    DBMS_OUTPUT.PUT_LINE('Llistat de privilegis sobre columnes d objectes');
    SELECT COUNT(grantee) INTO v_num_files FROM dba_col_privs WHERE grantee = v_username_in;
    IF v_num_files > 0 THEN
        FOR r_col_privs IN (SELECT privilege, table_name, column_name
                        FROM dba_col_privs
                        WHERE grantee = v_username_in)
        LOOP
            DBMS_OUTPUT.PUT_LINE('PRIVILEGI: ' || r_col_privs.privilege || '---> TAULA: ' || r_col_privs.table_name || '    COLUMNA: '||r_col_privs.column_name);
        END LOOP;
    ELSE 
        DBMS_OUTPUT.PUT_LINE('Aquest usuari no te cap privilegi sobre objectes.');
    END IF;
    -----------------
    
    DBMS_OUTPUT.PUT_LINE('--------------------------------------');   
    
    -- PRINTAR LLISTA DE ROLS
    DBMS_OUTPUT.PUT_LINE('Llistat de rols:');
    SELECT COUNT(grantee) INTO v_num_files FROM dba_role_privs WHERE grantee = v_username_in;
    IF v_num_files > 0 THEN
        FOR r_role_privs IN (SELECT granted_role
                        FROM dba_role_privs
                        WHERE grantee = v_username_in)
        LOOP
            DBMS_OUTPUT.PUT_LINE('ROL: ' || r_role_privs.granted_role );
        END LOOP;
    ELSE 
        DBMS_OUTPUT.PUT_LINE('Aquest usuari no te cap rol.');
    END IF;
    -----------------
    
EXCEPTION

    WHEN e_user_not_found THEN
        DBMS_OUTPUT.PUT_LINE ('Error: Usuari no trobat.');
    
END;
             
             