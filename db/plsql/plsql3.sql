SET SERVEROUTPUT ON;
SET VERIFY OFF;

--------------------------------------------
--IMPORTANT: SOTA CADA PROCEDIMENT O FUNCI�
--HA D'HAVER UN BLOQ PLSQL QUE ELS CRIDI.
--PROCUREU PROVAR EL CASOS DE FUNCIONAMENT 
--CORRECTE I ELS CASOS D'ERROR
--------------------------------------------

--------------------------------------------
-----------PROCEDIMENTS
--------------------------------------------

--1) Crear un procediment anomenat llista_productes que acceptant com par�metres 
--dues dates, llisti la seg�ent informaci� dels productes que hagin estat venuts 
--entre aquestes 2 dates: codi producte,descripcio i la quantitat total d'unitats 
--venudes entre les dues dates.
--
--La sortida ha de ser semblant a la seg�ent:
--NOTA: Per donar format pots utilitzar les funcions LPAD() i RPAD()
/*
Codi     Descripcio                         Total
---------------------------------------------------------------------
    3     ARMARIO NOGAL DOS PUERTAS              3
    7     ESTANTERIA METALICA                    1
    8     ARCHIVADOR MADERA                      1
    9     CARRITO OFICINA                        2
    4     MESA MODELO UNI�N                      1
    5     ARCHIVADOR CEREZO                      2
   11     MESA DIRECTOR                          2
    6     CAJA SEGURIDAD MOD B222                1
    2     SILLA DIRECTOR MOD. B�FALO             5
   10     SILLA GIRATORIA                        1
*/

CREATE OR REPLACE PROCEDURE llista_productes
    (p_data_inici VARCHAR2, p_data_final VARCHAR2)
    IS
        v_codi item.producto_no%TYPE;
        v_descripcion productos.descripcion%TYPE;
        v_cantidad NUMBER;
        
        CURSOR c_pedidos_item IS
            SELECT i.producto_no, pr.descripcion, SUM(i.cantidad)
            FROM item i
            JOIN productos pr ON (i.producto_no = pr.producto_no)
            JOIN pedidos pe ON (i.pedido_no = pe.pedido_no)
            WHERE fecha_pedido > TO_DATE(p_data_inici, 'DD-MM-YYYY') and fecha_pedido < TO_DATE(p_data_final, 'DD-MM-YYYY')
            GROUP BY i.producto_no, pr.descripcion;
    BEGIN
        OPEN c_pedidos_item;
        
        DBMS_OUTPUT.PUT_LINE (RPAD('Codi', 8, ' ') || RPAD('Descripcio', 30, ' ') || 'Total');
        DBMS_OUTPUT.PUT_LINE ('--------------------------------------------');
        LOOP
            FETCH c_pedidos_item INTO v_codi, v_descripcion, v_cantidad;
            EXIT WHEN (c_pedidos_item%NOTFOUND);
            
            DBMS_OUTPUT.PUT_LINE (RPAD(v_codi, 8, ' ')|| RPAD(v_descripcion, 30, ' ') || v_cantidad);

        END LOOP;

END llista_productes;

BEGIN
    llista_productes(&data_inici, &data_fi);
END;


--2) Realitzar un procediment anomenat inserir_departament que acceptant 
--com par�metres d'entrada el codi, el nom del departament i la seva localitat 
--insereixi un departament a la base de dades. Com n�mero de treballadors s'ha 
--assignar el valor 0.
--IMPORTANT: no est� perm�s fer cap comprovaci� pr�via a la inserci�.
--El procediment ha de tenir una secci� EXCEPTION on NOM�S es realitzi el tractament 
--del cas OTHERS
--Dins de OTHERS s'ha d'analitzar el valor de la variable predefinida SQLCODE
--per detectar els diferents errors que es poden produ�r a ORACLE depenent del 
--valor del codi de departament utilitzat per inserir.
--El procediment te dos par�metres de sortida:
-- a) un parametre p_error_code, un enter que guardar� el valor de SQLCODE
-- b) un par�metre p_error_msg, un text el valor del qual dependr�  
-- del valor de SQLCODE. Per omplir-lo has de tenir en compte la taula seg�ent:

--  SQLCODE		P_ERROR_MSG                 Situaci�
---------------------------------------------------------------------------------------------------------
-- 	??? 	NULL                        S'ha inserit amb un valor de codi de departament v�lid. No hi ha error
--  ???		"ERROR: valor duplicat"     S'ha intentat inserir amb un valor de codi de departament ja existent
--  ???		"ERROR: valor NULL"         S'ha intentat inserir amb un valor de codi de departament NULL
--  ???		"ERROR: valor massa gran"   S'ha intentat inserir amb un valor de codi de departament m�s gran del que
--  ???                            		la columna pot guardar (2 digits)
--  ???		"ERROR: error desconegut"   Qualsevol altre situaci� d'error

-- Reprodueix les situacions indicades, obt� el valor de SQLCODE i omple els dos par�metres de sortida

CREATE OR REPLACE PROCEDURE inserir_departament
(p_codi_depart IN NUMBER, p_nom_depart IN VARCHAR2, p_loc_depart IN VARCHAR2,
 p_error_code OUT NUMBER, p_error_msg OUT VARCHAR2) IS
    
    BEGIN
        INSERT INTO depart
        VALUES (p_codi_depart, p_nom_depart, p_loc_depart, 0);
        
    EXCEPTION
        WHEN OTHERS THEN
            p_error_code := SQLCODE;
            CASE p_error_code
                WHEN -1 THEN p_error_msg := 'ERROR: Valor duplicat';
                WHEN -1400 THEN p_error_msg := 'ERROR: Valor null';
                WHEN -1438 THEN p_error_msg := 'ERROR: Valor massa gran';
                ELSE p_error_msg := 'Error desconegut.';
            END CASE;
END inserir_departament;
    
DECLARE
    v_error_code NUMBER;
    v_error_msg VARCHAR2(50) := 'jeje';
BEGIN
    inserir_departament('&codi' , '&nom', '&loc', v_error_code, v_error_msg);
    
    IF v_error_code IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE('Codi error: ' || v_error_code || '. Missatge error: ' || v_error_msg);
    END IF;
END;

--------------------------------------------
-----------FUNCIONS
--------------------------------------------

--3) Funci� anomenada total_comandes_producte que a partir del codi d'un producte, 
--torni la quantitat de pedidos on s'ha venut aquest producte. 
--S'ha de tenir en compte que actualment la base de dades permet que a una mateixa
--comanda es demani el producte m�s d'una vegada (per exemple una quantitat amb el 
--preu normal i un altra quantitat amb un preu rebaixat per comprar molts).
--Si el codi de producte existeix per� no s'ha venut a cap pedido, s'ha de 
--retornar el valor 0
--Si el codi de producte no existeix, s'ha de retornar el valor NULL.

CREATE OR REPLACE FUNCTION total_comandes_producte (p_codi_producte IN NUMBER)
RETURN NUMBER
IS
    v_quantitat NUMBER;
BEGIN
    SELECT COUNT(pedido_no) INTO v_quantitat
    FROM item
    WHERE producto_no = p_codi_producte
    GROUP BY producto_no;
    
    RETURN v_quantitat;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END;

BEGIN
    DBMS_OUTPUT.PUT_LINE(total_comandes_producte(&codi_producte));
END;

--4) Funci� anomenada millor_client_per_producte que a partir del codi d'un
--producte torni el nom del client que m�s unitats ha comprat. 
--Per simplificar, encara que podria haver m�s d'un client, nom�s tornar� 
--el nom d'un.
--Si no hi cap client que hagi comprat aquest producte, ha de tornar NULL
--Si el producte no existeix o es dona qualsevol error a la base de dades, 
--tamb� ha de tornar NULL.

CREATE OR REPLACE FUNCTION millor_client_per_producte (p_producte_no IN NUMBER)
RETURN VARCHAR2
IS
    v_nom_client VARCHAR2;
    v_existe NUMBER;
BEGIN
    SELECT COUNT(producto_no) INTO v_existe
    FROM productos
    WHERE product_no = p_producte_no;
    
    IF v_existte > 0 THEN
        SELECT c.nombre INTO v_nom_client
        FROM productos pr
        JOIN item i ON (i.producto_no = pr.producto_no)
        JOIN pedidos pe ON (pe.pedido_no = i.pedido_no)
        JOIN clientes c ON (c.cliente_no = pe.cliente_no)
        WHERE pr.producto_no = p_producte_no
        GROUP BY c.nombre
        ORDER BY SUM(i.cantidad) desc
        FETCH FIRST 1 ROW ONLY;
        
        RETURN v_nom_client;
    ELSE
        RETURN NULL;
    END IF;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
END;

BEGIN
    DBMS_OUTPUT.PUT_LINE(total_comandes_producte(&codi_producte));
END;


--5) Aprofitant les funcions creades anteriorment, crear una vista anomenada 
--dades_producte que per cada producte llisti: codi, descripci�, quantitat de
--comandes amb aquest producte i el nom del client que m�s unitats hagi comprat
--del producte, ordenats per codi de producte.
--Nota: recordeu que si el SELECT de la vista inclou funcions, heu de definir
--alies de columna a cada funci�.

CREATE OR REPLACE VIEW dades_producte AS
SELECT codi, descripcio, total_comandes_producte(codi), millor_client_per_producte(codi)
FROM productos
ORDER BY codi;
 
--------------------------------------------
-----------VISTES I PERMISSOS
--------------------------------------------


--6) Executa la sent�ncia que permet mostrar el codi de la funci� millor_client_per_producte

SELECT object_id
FROM dba_procedures
WHERE object_name = 'millor_client_per_producte';

--7) Executa la sent�ncia que permet mostrar el nom i la data de creaci� del procediments
--del teu usuari. 
--A sota de la consulta, copia i pega el seu resultat.

SELECT object_name, created
FROM dba_objects
WHERE object_type = 'PROCEDURE'
      AND owner = 'RGM2019';
      
-- LLISTA_PRODUCTES	15/05/20
-- INSERIR_DEPARTAMENT 15/05/20

--8) Executa la sent�ncia que permet executar el teu procediment inserir_departament
--a l'usuari JFGB2020.

ALTER SESSION SET CURRENT_SCHEMA = JFGB2020;
DECLARE
    v_error_code NUMBER;
    v_error_msg VARCHAR2(50) := 'jeje';
BEGIN
    RGM2019.inserir_departament('&codi' , '&nom', '&loc', v_error_code, v_error_msg);
    
    IF v_error_code IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE('Codi error: ' || v_error_code || '. Missatge error: ' || v_error_msg);
    END IF;
END; 