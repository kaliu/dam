SET SERVEROUTPUT ON;
SET VERIFY OFF;

/******************/
/*EXCEPCIONS (I)  */
/******************/
--Els errors s'anomenen excepcions.
--Les excepcions que creiem que es poden donar QUAN S'EXECUTI el nostre codi 
--les hem d'afegir en la secci� EXCEPTIONS
--IMPORTANT: si es produeix una excepci�, es fa un ROLLBACK de tots els canvis
--fets a la transacci�.

/* Ejemplo: Programa que demana dos numeros i fa la seva divisi�
Comprovar que pasa si es divideix entre 0*/
DECLARE
    v_num1 INTEGER := &sv_num1;
    v_num2 INTEGER := &sv_num2;
    v_result NUMBER;
BEGIN
    v_result := v_num1 / v_num2;
    DBMS_OUTPUT.PUT_LINE ('v_result: '||v_result);
END;


/* EXEMPLE: Programa que demana dos numeros i fa la seva divisi�
Si es divideix entre 0, ens mostra un missatge avisant-nos. */
DECLARE
    v_num1 INTEGER := &sv_num1;
    v_num2 INTEGER := &sv_num2;
    v_result NUMBER;
BEGIN
    v_result := v_num1 / v_num2;
    DBMS_OUTPUT.PUT_LINE ('v_result: '||v_result);
    
EXCEPTION

	WHEN ZERO_DIVIDE THEN
		DBMS_OUTPUT.PUT_LINE ('Divisi� per zero');

	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;

/* EXEMPLE: Programa que assigna una cadena de caracters amb
una longitud m�s gran del perm�s
Ens mostra un missatge avisant-nos. */
DECLARE
    v_test_var CHAR(3) := '123';
BEGIN
    v_test_var := '1234';
    DBMS_OUTPUT.PUT_LINE ('v_test_var: '||v_test_var);
EXCEPTION
    WHEN INVALID_NUMBER OR VALUE_ERROR THEN
        DBMS_OUTPUT.PUT_LINE ('Valor incorrecte');
		
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;
