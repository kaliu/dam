SET SERVEROUTPUT ON;
SET VERIFY OFF;
/*********************/
/*  SELECT ... INTO  */
/*********************/

/* 
Dins de PL-SQL es poden fer consultes SELECT
El resultat s'ha de guardar a variables especificades dins la cl�usula INTO
Per aix� ens referirem com SELECT..INTO

SELECT INTO nom�s funciona si retorna una SOLA FILA,
- Si retorna 0 filas da error NO_DATA_FOUND, 
- Si retorna >1 da el error TOO_MANY_ROOWS */


/* Ejemplo: Programa que demana un codi treballador (per exemple el 7521)
per teclat i mostra el seu sou i el seu cognom.
Si el treballador no existeix, ens mostra un missatge avisant-nos. */

DECLARE

	v_salario NUMBER;     -- El tipus de dada ha de ser igual al de la columna     
	v_apellido VARCHAR2(50);     
	v_num_emp NUMBER := &codigo_empleado; 

BEGIN

	SELECT apellido, salario INTO v_apellido, v_salario
	FROM emple
	WHERE v_num_emp = emp_no; 
		/* Un SELECT INTO nom�s funciona correctament si retorna 1 FILA,
		Si retorna 0 filas dona l'error NO_DATA_FOUND, 
		Si devuelve > 1 fila dona l'error TOO_MANY_ROOWS */

	DBMS_OUTPUT.PUT_LINE ('L''empleat amb codi ' || v_num_emp || ' t� sou '|| v_salario);
	DBMS_OUTPUT.PUT_LINE (' i cognom ' || v_apellido);

EXCEPTION

	WHEN NO_DATA_FOUND THEN
		DBMS_OUTPUT.PUT_LINE ('No existeix un treballador amb el codi ' || v_num_emp);

	WHEN TOO_MANY_ROWS THEN
		DBMS_OUTPUT.PUT_LINE ('Molts empleats amb aquest codi');

	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');

END;

