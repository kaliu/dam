SET SERVEROUTPUT ON;
SET VERIFY OFF;
/*******************/    
/* ATRIBUT %TYPE  */
/*******************/
-- L'atribut %TYPE permet declarar variables amb el mateix tipus
-- de dades que una columna d'una taula.


/* Exemple: programa que demana un codi d'empleat (per exemple el 7521)
per teclat, i mostra el cognom, el n�mero de departament,
l'ofici i el salari d'aquest empleat.
Si l'empleat no existeix mostra el missatge "Empleat inexistent" */


DECLARE
    -- v_codi tiene el mismo tipo de dato que la columna emple.emp_no
	v_codi emple.emp_no%TYPE := &introdueix_codi;
	
	v_apellido emple.apellido%TYPE;
	v_oficio emple.oficio%TYPE;
	v_dept_no emple.dept_no%TYPE;
	v_salario emple.salario%TYPE;

BEGIN

	SELECT apellido, oficio, dept_no, salario 
	INTO v_apellido, v_oficio, v_dept_no, v_salario
	FROM emple
	WHERE emp_no = v_codi;

	DBMS_OUTPUT.PUT_LINE ('emp_no: ' || v_codi || ' Apellido: ' || v_apellido);
	DBMS_OUTPUT.PUT_LINE ('oficio: ' || v_oficio || ' Salario: ' ||v_salario);
	DBMS_OUTPUT.PUT_LINE ('dept_no: ' || v_dept_no);

EXCEPTION

	WHEN NO_DATA_FOUND THEN
		DBMS_OUTPUT.PUT_LINE ('No existeix un treballador amb el codi ' || v_num_emp);

	WHEN TOO_MANY_ROWS THEN
		DBMS_OUTPUT.PUT_LINE ('Molts empleats amb aquest codi');

	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');

END;






