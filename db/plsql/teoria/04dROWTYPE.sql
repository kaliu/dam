SET SERVEROUTPUT ON;
SET VERIFY OFF;
/**************************/
/* ATRIBUTO %ROWTYPE (II) */
/**************************/
--L'atribut %ROWTYPE tamb� es pot fer servir per guardar columnes
--concretas de la taula
    
/* Exemple: programa que demana un codi d'empleat (per exemple el 7521)
per teclat, i mostra el cognom, el n�mero de departament,
l'ofici i el salari d'aquest empleat.
Si l'empleat no existeix mostra el missatge "Empleat inexistent" */

DECLARE
	v_codi emple.emp_no%TYPE := &introdueix_codi;
    
	-- r_registre permite guardar totes les columnes d'una fila 
    -- de la taula emple
	r_registre emple%ROWTYPE;

BEGIN
    --L'atribut %ROWTYPE tamb� es pot fer servir per guardar columnes
    --concretas de la taula
	SELECT emp_no, apellido
	INTO r_registre.emp_no, r_registre.apellido
	FROM emple
	WHERE emp_no = v_codi;

	DBMS_OUTPUT.PUT_LINE ('emp_no: ' || r_registre.emp_no );
    DBMS_OUTPUT.PUT_LINE ('apellido: ' || r_registre.apellido);
    

EXCEPTION

	WHEN NO_DATA_FOUND THEN
		DBMS_OUTPUT.PUT_LINE ('No existeix un treballador amb el codi ' || v_num_emp);

	WHEN TOO_MANY_ROWS THEN
		DBMS_OUTPUT.PUT_LINE ('Molts empleats amb aquest codi');

	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');

END;






