SET SERVEROUTPUT ON;
SET VERIFY OFF;
/**************/
/* INSERT, UPDATE, DELETE  */
/**************/

-- EJEMPLO
-- Ejecutamos un INSERT, UPDATE y DELETE


DECLARE
    v_emp_no EMPLE.EMP_NO%TYPE;
    v_apellido EMPLE.APELLIDO%TYPE;
    v_oficio EMPLE.OFICIO%TYPE;
    v_salario EMPLE.SALARIO%TYPE;

BEGIN

    v_emp_no := 5001;
	  v_salario := 2000;
    -- INSERT:
    -- Insertamos un trabajador
    INSERT INTO emple (emp_no, apellido, oficio, salario)
    VALUES (v_emp_no, 'Mart�nez', 'Analista', v_salario);

    -- UPDATE:
    -- Aumentamos un 10% el sueldo del trabajador insertado
    UPDATE emple
    SET salario = salario * 1.1
    WHERE emp_no = v_emp_no;

    SELECT salario INTO v_salario
    FROM emple
    WHERE emp_no = v_emp_no;
    DBMS_OUTPUT.PUT_LINE ('Nuevo salario: '|| v_salario);

    -- DELETE:
    -- Por ultimo se borra ese empleado
    DELETE FROM EMPLE
    WHERE emp_no = v_emp_no;

    COMMIT;
END;
