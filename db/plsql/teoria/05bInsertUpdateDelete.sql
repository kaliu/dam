SET SERVEROUTPUT ON;
SET VERIFY OFF;
/**************/
/* RETURNING  */
/**************/
-- Sovint cal tenir informaci� sobre les files modificades
-- per una instrucci� DML (insert, update, delete) despr�s d'haver la executat.

-- La cl�usula RETURNING es pot afegir al final de qualsevol instrucci� DML
-- i s'utilitza per obtenir informaci� sobre la fila o les files que
-- s'acaben de processar.

--IMPORTANT: En el seg�ent exemple veiem com s'utilitza RETURNING quan
--solamente �s afectada una fila. Si fossin afectades m�s files, s'hauria d'utilitzar
--COLLECTIONS (l'equivalent a un array), tema que no donarem per� que podeu investigar.

-- EXEMPLE
-- Executem INSERT, UPDATE i DELETE, i mitjan�ant la cl�usula RETURNING
-- accedim a informaci� de las fila afectada

DECLARE
    v_rowid ROWID;
    v_apellido EMPLE.APELLIDO%TYPE;
    v_oficio EMPLE.OFICIO%TYPE;
    v_emp_no EMPLE.EMP_NO%TYPE;
	v_salario EMPLE.SALARIO%TYPE;
    
BEGIN
   
    v_emp_no := 5001;
	v_salario := 2000;
    -- Nota: el ROWID �s una columna virtual (no existeix f�sicament) el valor de la qual
    -- es crea a partir del fitxer de dades, bloc dins el fitxer, posici� de
    -- la fila dins el bloc, etc.
    -- El seu valor identifica una fila de manera directa dins de la base de
    -- dades: dues files (de la mateixa taula o diferents) no poden tenir
    -- el mateix valor de ROWID.

    -- INSERT:
    -- Inserin un empleat i obtenim el ROWID de la fila creada
    INSERT INTO emple (emp_no, apellido, oficio, salario) 
    VALUES (v_emp_no, 'Mart�nez', 'Analista', v_salario)
    RETURNING ROWID INTO v_rowid;
    
    DBMS_OUTPUT.PUT_LINE ('El nuevo empleado insertado tiene por Rowid '|| v_rowid);
    
    -- UPDATE:
    -- Incrementem un 10% el sou de l'empleat creat
    UPDATE emple
    SET salario = salario * 1.1
    WHERE emp_no = v_emp_no
    RETURNING apellido, oficio, salario INTO v_apellido, v_oficio, v_salario;
    
    DBMS_OUTPUT.PUT_LINE ('Apellido: ' || v_apellido);
    DBMS_OUTPUT.PUT_LINE ('Oficio: '|| v_oficio);
    DBMS_OUTPUT.PUT_LINE ('Nuevo salario: '|| v_salario);
    

    -- DELETE:
    -- Finalment esborrem l'empleat creat.
	-- Es pot fer identificant la fila pel seu ROWID
    DELETE FROM EMPLE 
    WHERE rowid = v_rowid
    RETURNING emp_no INTO v_emp_no;
    
    DBMS_OUTPUT.PUT_LINE ('El empleado borrado es el '|| v_emp_no);
    
    COMMIT;
    
END;