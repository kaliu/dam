SET SERVEROUTPUT ON;
SET VERIFY OFF;
/******/
/* IF */
/******/

/*SINTAXI:

    IF condici� 
    THEN
        ...
    END IF;
*/


/*Exemple: programa que demana un codi_treballador (per exemple el 7566)
i una comisi�. Amb aquestes dades actualitza aquesta comisi� al nou valor. 
Si la comisi� anterior era nul�la, feu que escrigui un missatge d'advert�ncia */

DECLARE 

v_emp_no emple.emp_no% TYPE := &p_num_emp;
-- A v_comision guardarem la comisio actual a la base de dades
v_comision emple.comision%TYPE; 
-- A v_nova_comision guardarem la comisio que volem assignar
v_nova_comision emple.comision%TYPE := &p_nova_comisio; 

BEGIN

    SELECT comision INTO v_comision
    FROM emple
    WHERE emp_no = v_emp_no;
    
    IF v_comision IS NULL 
    THEN
        DBMS_OUTPUT.PUT_LINE ('Atencion, comision anterior nula');
    END IF;
    
    UPDATE emple SET comision = v_nova_comision
    WHERE emp_no = v_emp_no;
    --Es necessari fer un commit final per tancar la transacci� i
    --guardar definitivament el canvi fet amb UPDATE 
    COMMIT;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No existe ese trabajador');
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;



