SET SERVEROUTPUT ON;
SET VERIFY OFF;
/***********/
/* IF-ELSE */
/***********/

/*SINTAXI:

    IF condici� 
    THEN
        ...
    ELSE
        ...
    END IF;
*/

/*Exemple: Programa que demani un codi de treballador (per exemple el 7521) 
per teclat i mostra les dades del seu departament */

DECLARE
    -- %ROWTYPE indica que aquesta variable guardar� el contingut d'una fila
	r_depart depart%ROWTYPE;
	v_emp_no emple.emp_no%TYPE := &p_num_emp;
	existe NUMBER;

BEGIN
    --Aquesta es una forma de comprovar si existeix el que necessitem
    --sense que es generi l'excepci� NOT_FOUND
	SELECT COUNT(emp_no) INTO existe
	FROM emple
	WHERE emp_no = v_emp_no;

	IF (existe = 1)
	THEN
        -- Totes les columnes de la fila es guarden a r_depart, 
        -- que havia estat definida com %ROWTYPE
		SELECT depart.* INTO r_depart
		FROM depart
        INNER JOIN emple ON emple.dept_no = depart.dept_no
		WHERE  emple.emp_no = v_emp_no;

		DBMS_OUTPUT.PUT_LINE ('Datos departamento del trabajador num: '|| v_emp_no);
		DBMS_OUTPUT.PUT_LINE ('Numero departamento : ' || r_depart.dept_no);
		DBMS_OUTPUT.PUT_LINE ('Nombre departamento : ' || r_depart.dnombre);
		DBMS_OUTPUT.PUT_LINE ('Localidad : ' || r_depart.loc);
	ELSE
		DBMS_OUTPUT.PUT_LINE ('Ese trabajador no existe!');
	END IF;

EXCEPTION

	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;



