SET SERVEROUTPUT ON;
SET VERIFY OFF;
/**************/
/* CASE WHEN  */
/**************/

/* SINTAXI

    CASE expressi�
        WHEN valor1 THEN
            ordres_SQL
        WHEN valor2 THEN
            ordres_SQL
        ...
        ELSE
            ordres_SQL
    END CASE;
*/

/*Exemple: programa que demana un codi d'empleat
per teclat i actualitza la seva comissi� depenent
de la seg�ent taula

Codi departament	Comissio
10	                0
20 	                15
30	                10
40                  5

Si l'empleat pertany a un altre departament, 
s'ha d'indicar amb el missatge 
'No hi ha comissio predefinida per aquest departament'
*/

DECLARE
	v_comision EMPLE.COMISION%TYPE;
	V_emp_no EMPLE.EMP_NO%TYPE := &p_emple;
	v_depart EMPLE.DEPT_NO%TYPE;

BEGIN

	SELECT dept_no INTO v_depart
	FROM emple
	WHERE emp_no = v_emp_no;

	CASE v_depart
		WHEN 10 THEN v_comision:= 0;
		WHEN 20 THEN v_comision:= 15;
		WHEN 30 THEN v_comision:= 10;
		WHEN 40 THEN v_comision:= 5;
		ELSE DBMS_OUTPUT.PUT_LINE ('No hi ha comissio predefinida per aquest departament');
	END CASE;

	IF v_comision IS NOT NULL THEN
		UPDATE EMPLE SET comision = v_comision 
        WHERE emp_no = v_emp_no;
		COMMIT;
	END IF;

EXCEPTION

	WHEN NO_DATA_FOUND THEN
		DBMS_OUTPUT.PUT_LINE ('Ese empleado no existe');
    WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END; 