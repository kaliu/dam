SET SERVEROUTPUT ON;
SET VERIFY OFF;
/**************/
/* CASE amb cerca  */
/**************/

--Es diu CASE amb cerca per que no s'avalua un valor si no que
--es cerca la primera condici� que dona TRUE.
/* SINTAXI

    CASE
        WHEN condici�_1 THEN ordresSQL_1
        WHEN condici�_2 THEN ordresSQL_2
        ...
        WHEN condici�_N THEN ordresSQL_N
      [ ELSE  ordresSQL ]
    END CASE;
*/


/*Exemple: programa que demana un codi d'empleat (per exemple 7521)
per teclat i augmenta el seu salari segons la seg�ent taula.
S

Salari          	% Augment
<1000	                 3%
>=1000 i <=2000 	     2%
>2000	                 1%

si no trova el'empleat introdu�t, ha de sortir un missatge indicant-lo
*/

DECLARE 
    v_emp_no emple.emp_no% TYPE := &p_num_emp;
    v_salario emple.salario%TYPE; 
    v_porcentaje NUMBER := 0; 

BEGIN

    SELECT salario INTO v_salario
    FROM emple
    WHERE emp_no = v_emp_no;
    
	CASE
        WHEN v_salario < 1000 THEN v_porcentaje := 3;	
        WHEN v_salario >= 1000 AND v_salario <= 2000 THEN  v_porcentaje := 2;
        WHEN v_salario > 2000 THEN v_porcentaje := 1;
	END CASE;


	UPDATE emple
    SET salario = v_salario + (v_porcentaje/100)*v_salario
    WHERE emp_no = v_emp_no;

    COMMIT;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No existeix aquest empleat');
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;