SET SERVEROUTPUT ON;
SET VERIFY OFF;

/**********/
/* BUCLES */
/**********/
--Hi ha els mateixos tipus de bucle que en java i un d'addicional
--que �s el bucle de cursor (ho veurem m�s endavant, al tema CURSORS)

/**************/
/* BUCLE LOOP */
/**************/
-- equivalent al do while de Java. Bucle infinit amb condici�  de sortida

/*SINTAXI

    LOOP
        ....
        EXIT WHEN condici�;
        ....
    END LOOP;

*/

DECLARE
	v_contador INTEGER := 0;

BEGIN

	LOOP

		v_contador := v_contador + 1;
		DBMS_OUTPUT.PUT_LINE (v_contador || '.Nunca pondr� un DELETE FROM sin el WHERE');
		
		/* La sentencia exit; equivale a break y abandona el bucle */
		IF (v_contador = 10) THEN EXIT;
		END IF;
		-- EXIT WHEN (v_contador = 10);
		
	END LOOP;

END;

--*************************************************
-- OTRO EJEMPLO: antes creamos una tabla y una secuencia
--*************************************************
DROP TABLE TEST_BUCLES;

CREATE TABLE TEST_BUCLES(
id NUMBER(5,0) PRIMARY KEY,
frase VARCHAR2(20));

DROP SEQUENCE SEQ_TEST;

CREATE SEQUENCE SEQ_TEST
START WITH 1 
INCREMENT BY 1
NOCACHE;

-- EJEMPLO: un bucle infinito con condici�n de salida 
-- en el que insertamos 5 filas a la tabla test_bucles.
DECLARE
    v_contador INTEGER:=0;
BEGIN
    LOOP
        v_contador := v_contador + 1;
        -- Dentro del INSERT llamamos a la secuencia seq_test definida anteriormente
        -- para obtener un nuevo valor consecutivo para la columna id   
        INSERT INTO test_bucles (id, frase) 
        VALUES (seq_test.NEXTVAL, 'Frase ' || v_contador);
        --El bucle se termina cuando v_contador llega a 5
        EXIT WHEN (v_contador = 5);
    END LOOP;
    COMMIT;
END;
