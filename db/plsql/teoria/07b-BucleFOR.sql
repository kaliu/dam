SET SERVEROUTPUT ON;
SET VERIFY OFF;

/*************/
/* BUCLE FOR */
/*************/

/*SINTAXI

    FOR comptador IN [REVERSE] valor_mes_petit .. valor_mes_gran
    LOOP
        ...
    END LOOP;

    comptador: �s un valor INTEGER declarat impl�citament (no fa falta
    declarar-lo a la secci� DECLARE). El seu valor augmenta (o disminueix 
    si s'utilitza la paraula clau REVERSE) en una unitat per cada iteraci� 
    del bucle entre el rang de valors inicial i final definits en el FOR
*/


-- EJEMPLO: muestra 10 veces una frase 

BEGIN
	FOR v_contador IN 1..10
	LOOP
		DBMS_OUTPUT.PUT_LINE ('No te olvides el WHERE en el DELETE FROM');
	END LOOP;

END;


-- OTRO EJEMPLO: Insertar 10 filas m�s en la tabla test_bucles 
BEGIN
 
    FOR v_cont IN 1..10
    LOOP
        -- Llamamos a la secuencia seq_test definida anteriormente
        -- para obtener un nuevo valor consecutivo        
        INSERT INTO test_bucles (id, frase) 
            VALUES (seq_test.NEXTVAL, 'Frase ' || v_cont);
     
    END LOOP;
	COMMIT;
END;