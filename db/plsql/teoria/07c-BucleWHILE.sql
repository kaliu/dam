SET SERVEROUTPUT ON;
SET VERIFY OFF;

/***************/
/* BUCLE WHILE */
/***************/

/*SINTAXI

    WHILE condici� 
    LOOP
        ....
    END LOOP;

*/

-- EJEMPLO: mostrar una frase 10 veces 

DECLARE
	v_contador INTEGER := 0;

BEGIN

	WHILE (v_contador < 10)
	LOOP
		v_contador := v_contador + 1;
		DBMS_OUTPUT.PUT_LINE ('Nunca escribir� un DELETE FROM sin el WHERE');
	END LOOP;

END;


-- OTRO EJEMPLO: Insertar 10 filas, utilizando el bucle while.


DECLARE
    v_contador INTEGER := 0;
BEGIN

    WHILE (v_contador < 10)
    LOOP
        -- Llamamos a la secuencia seq_test definida anteriormente
        -- para obtener un nuevo valor consecutivo
        INSERT INTO test_bucles 
        VALUES (seq_test.NEXTVAL, 'Frase ' || v_contador);
        
        v_contador := v_contador + 1;    
    END LOOP;
    COMMIT;
END;