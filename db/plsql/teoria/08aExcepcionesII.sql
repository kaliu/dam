SET SERVEROUTPUT ON;
SET VERIFY OFF;

/*******************/
/*EXCEPCIONS (II): ERRORS DEFINITS PER L'USUARI  */
/*******************/
--A part de les predefinides, podem crear excepcions pr�pies
--A la secci� DECLARE indiquem la nostra excepci�
--Al cos del programa (BEGIN), quan detectem l'error, disparem l'excepci�
--amb la clausula RAISE
--RAISE tamb� es pot fer servir dins de la secci� EXCEPTION

/* EXEMPLE: Programa que demana un numero
Si no �s positiu, ens mostra un missatge avisant-nos. */
DECLARE
    v_num1 INTEGER := &sv_num1;
    --indiquem la nostra excepci�
    e_mi_excepcion EXCEPTION;
BEGIN
    IF (v_num1 <= 0) THEN
        --quan detectem l'error, disparem l'excepci�
        RAISE e_mi_excepcion;
    END IF;
    DBMS_OUTPUT.PUT_LINE ('v_num1: '||v_num1);
    
EXCEPTION
	WHEN e_mi_excepcion THEN
		DBMS_OUTPUT.PUT_LINE ('El valor ha de ser positiu');
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE ('Error diferent a qualsevol dels anteriors');
END;




