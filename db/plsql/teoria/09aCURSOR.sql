SET SERVEROUTPUT ON;
SET VERIFY OFF;

/************/
/* CURSORS  */
/************/
/*
Cada ordre SQL executada t� associada un �rea de mem�ria
on es guarda informaci� del seu resultat.

Un CURSOR �s un puntero (una direcci�) a l'area de mem�ria
associada a una ordre SQL.

- Els cursors associats a les ordres INSERT, UPDATE, DELETE i SELECT INTO no s'han de
declarar, es creen autom�ticament (cursors impl�cits).

- Els cursors associats a l'ordre SELECT han de ser creats pel
programador (cursors expl�cits). Aquest cursor permetr� accedir a cadascuna de
les files resultat del SELECT.

1) Declaraci� del cursor.
Els cursors es declaren a la zona declarativa del bloc an�nim, procediment o funci�
mitjan�ant la seg�ent sintaxi:
    CURSOR nom_cursor IS
    sent�ncia SELECT

2) Per poder treballar amb un cursor cal obrir-lo mitjan�ant la sent�ncia OPEN.
La sintaxi de la sent�ncia OPEN �s la seg�ent:
    OPEN nom_cursor;

La sent�ncia OPEN realitza les seg�ents operacions:
a) Assigna mem�ria din�micament per emmagatzemar les files resultat de la consulta.
b) Analitza la sent�ncia SELECT
c) Enlla�a les variables d'entrada. Identifica les adreces de mem�ria de les variables per
establir el seu valor.
d) Identifica les files resultat. No obstant aix� no carrega cap.
e) Estableix un punter a la primera fila.


3) Obtenci� de les files resultat.
Per recuperar 1 a 1 les files d'un cursor s'utilitza la sent�ncia FETCH ... INTO.
La sent�ncia FETCH nom�s recupera una fila. Per tant, si la sentencia SELECT retorna
m�s d'una,  cal incloure FETCH dins d'un bucle.

La sintaxi de la sent�ncia FETCH ... INTO �s id�ntica a la SELECT .... INTO
per� utilitzant FETCH enlloc de SELECT:
    FETCH nom_cursor INTO [variable1, variable2, .... | record o ROWTYPE name];


4) Tancament de l'cursor.
Es realitza mitjan�ant la sent�ncia:
    CLOSE nom_cursor;
*/

/*  Exemple: Cursor que torna una fila nom�s
Programa que mostra les columnes nombre, direccion i limite_credito
d'un client
*/

DECLARE
    v_nombre clientes.nombre%TYPE;
    v_direccion clientes.direccion%TYPE;
    v_limite_credito clientes.limite_credito%TYPE;
    -- Pels cursors el conveni pel seu nom es c_XXXXX
    CURSOR c_cliente IS
        SELECT nombre, direccion, limite_credito
        FROM clientes
        --WHERE cliente_nif = '000055555E';
        WHERE cliente_nif = '1234';

BEGIN
    -- Obrim el cursor
    OPEN c_cliente;CU
    -- Carrega 1 fila a les variables del INTO
    FETCH c_cliente INTO v_nombre, v_direccion, v_limite_credito;
    --Un cursor no genera l'excepci� NO_DATA_FOUND
    --Te un atribut anomenat NOTFOUND que podem consultar
    IF (c_cliente%NOTFOUND) THEN
        DBMS_OUTPUT.PUT_LINE ('Client no trobat');
    ELSE
        DBMS_OUTPUT.PUT_LINE ('NOM: ' || v_nombre );
        DBMS_OUTPUT.PUT_LINE ('ADRE�A: ' || v_direccion );
        DBMS_OUTPUT.PUT_LINE ('LIMIT CREDIT: ' || v_limite_credito);
    END IF;

    --Al tancar el cursor, alliberem la mem�ria associada
    CLOSE c_cliente;
END;
