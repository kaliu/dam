SET SERVEROUTPUT ON;
SET VERIFY OFF;

/*************************************/
/* CURSOR QUE TORNA M�S D'UNA FILA   */
/*************************************/
/*

Per recuperar 1 a 1 les files d'un cursor s'utilitza la sent�ncia FETCH ... INTO.
La sent�ncia FETCH nom�s recupera una fila. 
Per tant, si la sentencia SELECT retorna m�s d'una,  cal incloure FETCH dins d'un bucle.

*/

/***************************************/
/* ATRIBUTS DEL CURSOR                 */
/***************************************/

--Un cursor t� definida una zona de mem�ria amb un conjunt d'atributs que guarden 
--informaci� del resultat de la seva execuci�. Els principals s�n:
--  NOM_CURSOR%FOUND    val TRUE si FETCH ha tornat una fila. 
--                      En cas contrari, FALSE.
--  NOM_CURSOR%NOTFOUND val TRUE si l'ordre FETCH no torna cap fila. 
--                      En cas contrari, FALSE.
--  NOM_CURSOR%ROWCOUNT N�mero de files afectades pel FETCH des de l'apertura del cursor. 


/*  Exemple: Programa que mostra les columnes emp_no, apellido, oficio i salario 
dels empleats amb un salari major que l'introdu�t per teclat
*/
DECLARE
    v_salario emple.salario%TYPE := &introduce_salario;
    v_apellido emple.apellido%TYPE;
    v_oficio emple.oficio%TYPE;
    v_total_filas INTEGER;
    -- Pels cursors el conveni pel seu nom es c_XXXXX
    CURSOR c_emple IS 
        SELECT apellido, oficio, salario
        FROM emple
        WHERE salario > v_salario;

BEGIN
    -- Obrim el cursor
    OPEN c_emple;
    --Iniciem el bucle
    LOOP
        -- Carrega 1 fila a les variables del INTO
        FETCH c_emple INTO v_apellido, v_oficio, v_salario;
        
        -- sortim del bucle quand FETCH no troba m�s files
        EXIT WHEN (c_emple%NOTFOUND); 
        DBMS_OUTPUT.PUT_LINE ('COGNOM: ' || v_apellido || '   OFICI: ' 
            || v_oficio || '    SOU: ' || v_salario);
    END LOOP;
    v_total_filas := c_emple%ROWCOUNT;
    DBMS_OUTPUT.PUT_LINE( v_total_filas || ' files seleccionades '); 
    --Al tancar el cursor, alliberem la mem�ria associada
    CLOSE c_emple;
END;