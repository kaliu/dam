SET SERVEROUTPUT ON;
SET VERIFY OFF;
/***************************************/
/* SQL%FOUND, SQL%NOTFOUND, SQL%ROWCOUNT  */
/***************************************/

--Els cursors associats a les ordres INSERT, UPDATE, DELETE i SELECT INTO no s'han de
--declarar, es creen autom�ticament (cursors impl�cits).
--Es pot accedir als atributs del cursor impl�cit:
--  SQL%FOUND       val TRUE si una sent�ncia INSERT, UPDATE o DELETE ha afectat a una o m�s files 
--                  o si una instrucci� SELECT INTO ha tornat una o m�s files. 
--                  En cas contrari, FALSE.
--  SQL%NOTFOUND    val TRUE si una sent�ncia INSERT, UPDATE o DELETE no ha afectat a cap fila 
--                  o si una instrucci� SELECT INTO no ha tornat cap fila. 
--                  En cas contrari, FALSE.
--  SQL%ROWCOUNT    N�mero de files afectades per una instrucci� INSERT, UPDATE o DELETE 
--                  o retornades per una instrucci� SELECT INTO.   

DECLARE
    v_emp_no EMPLE.EMP_NO%TYPE;
    v_apellido EMPLE.APELLIDO%TYPE;
    v_oficio EMPLE.OFICIO%TYPE;
    v_salario EMPLE.SALARIO%TYPE;
    v_total_filas INTEGER;
    
BEGIN
   
    v_emp_no := 5001;
	v_salario := 2000;
    -- INSERT:
    -- Insertamos un trabajador
    INSERT INTO emple (emp_no, apellido, oficio, salario) 
    VALUES (v_emp_no, 'Mart�nez', 'Analista', v_salario);
    
    IF SQL%NOTFOUND THEN 
        DBMS_OUTPUT.PUT_LINE('Ninguna fila insertada'); 
    ELSIF SQL%FOUND THEN 
        v_total_filas := SQL%ROWCOUNT;
        DBMS_OUTPUT.PUT_LINE( v_total_filas || ' filas insertadas '); 
    END IF;     
  
    -- UPDATE:
    -- Aumentamos un 10% el sueldo del trabajador insertado
    UPDATE emple
    SET salario = salario * 1.1
    WHERE emp_no = v_emp_no;
    
    IF SQL%NOTFOUND THEN 
        DBMS_OUTPUT.PUT_LINE('Ninguna fila modificada'); 
    ELSIF SQL%FOUND THEN 
        v_total_filas := SQL%ROWCOUNT;
        DBMS_OUTPUT.PUT_LINE( v_total_filas || ' filas modificadas '); 
    END IF;    
    
    SELECT salario INTO v_salario
    FROM emple
    WHERE emp_no = v_emp_no;
    DBMS_OUTPUT.PUT_LINE ('Nuevo salario: '|| v_salario);

    IF SQL%NOTFOUND THEN 
    --Aqu� no s'arriba a entrar, hi haur� excepci� NO_DATA_FOUND
        DBMS_OUTPUT.PUT_LINE('Ninguna fila seleccionada'); 
    ELSIF SQL%FOUND THEN 
        v_total_filas := SQL%ROWCOUNT;
        DBMS_OUTPUT.PUT_LINE( v_total_filas || ' filas seleccionadas '); 
    END IF;    
    
    -- DELETE:
    -- Por ultimo se borra ese empleado   
    DELETE FROM EMPLE 
    WHERE emp_no = v_emp_no;
    
    IF SQL%NOTFOUND THEN 
        DBMS_OUTPUT.PUT_LINE('Ninguna fila borrada'); 
    ELSIF SQL%FOUND THEN 
        v_total_filas := SQL%ROWCOUNT;
        DBMS_OUTPUT.PUT_LINE( v_total_filas || ' filas borradas '); 
    END IF;    
    COMMIT;
END;