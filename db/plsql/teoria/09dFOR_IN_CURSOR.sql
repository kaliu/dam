SET SERVEROUTPUT ON;
SET VERIFY OFF;
/************/
/* CURSOR DE BUCLE FOR  */
/************/

/* 
At�s que la utilitzaci� de cursors va lligada sempre a bucles, �s possible simplificar el tractament
dels cursors mitjan�ant el bucle FOR de cursor.
La sintaxi del bucle FOR �s la seg�ent:
    FOR nom_registre IN nom_cursor
    LOOP
        Sent�ncia 1;
        Sent�ncia 2;
    END LOOP;
    
    El CURSOR de bucle FOR gestiona autom�ticament l'apertura, el FETCH i el
    tancament del cursor
*/

/*  Exemple: Programa que mostra les columnes emp_no, apellido, oficio i salario 
dels empleats amb un salari major que l'introdu�t per teclat
*/
DECLARE
	v_salario emple.salario%TYPE := &introduce_salario;
    -- Declaraci� del cursor
	CURSOR c_emple IS 
		SELECT emp_no, apellido, oficio, salario
		FROM emple
		WHERE salario > v_salario;

BEGIN
    -- Usamos el cursor en el FOR
    -- r_empleado �s una variable de tipo c_emple%ROWTYPE que recoge los datos del cursor. 
    -- No es preciso declararla.
    -- OJO! La variable r_empleado nom�s existeix al bucle FOR. Fora del bucle no es accesible.
	FOR r_empleado IN c_emple
	LOOP
		DBMS_OUTPUT.PUT('emp_no: ' || r_empleado.emp_no);
		DBMS_OUTPUT.PUT(' apellido: ' || r_empleado.apellido);
		DBMS_OUTPUT.PUT(' oficio: '|| r_empleado.oficio);
		DBMS_OUTPUT.PUT_LINE(' salario: ' || r_empleado.salario);
	END LOOP;
    --IMPORTANT: Si el SELECT del cursor no trova cap fila, no es produeix cap excepci�,
    --simplement no s'entra dins el bucle FOR IN

END;



