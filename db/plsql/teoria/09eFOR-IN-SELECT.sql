SET SERVEROUTPUT ON;
SET VERIFY OFF;
/***********************/
/* FOR IN SELECT */
/***********************/

--Encara es pot simplificar m�s, no declarem el cursor
--i utilitzem directament el SELECT al FOR IN
--Internament es crea un cursor impl�cit (creat autom�ticament,
--mo per l'usuari).

--T� l'incovenient respecte l'anterior que els SELECTS, enlloc
--d'estar nom�s definits a la zona DECLARE en forma de cursor, 
--poden estar a qualsevol lloc, i per tant �s m�s dif�cil de mantenir el codi.

--******************************************************************
--Exemple: Programa que mostra les columnes emp_no, apellido, oficio i salario 
--dels empleats amb un salari major que l'introdu�t per teclat
--******************************************************************

DECLARE
    v_salario emple.salario%TYPE := &introduce_salario;

BEGIN
    /* r_empleat es un valor %ROWTYPE declarado impl�citamente 
    (no se declara en DECLARE) cuyo valor contiene una fila del SELECT
    por cada iteraci�n del bucle.
    OJO! La variable r_empleat nom�s existeix al bucle FOR. Fora del bucle no es accesible.*/
    FOR r_empleat IN (SELECT emp_no, apellido, oficio, salario
						FROM emple
						WHERE salario > v_salario)
    LOOP
        DBMS_OUTPUT.PUT('emp_no: ' || r_empleat.emp_no);
        DBMS_OUTPUT.PUT(' apellido: ' || r_empleat.apellido);
        DBMS_OUTPUT.PUT(' oficio: '||r_empleat.oficio);
        DBMS_OUTPUT.PUT_LINE(' salario: ' || r_empleat.salario);
    END LOOP;
    --IMPORTANT: Si el SELECT del FOR IN no trova cap fila, no es produeix cap excepci�,
    --simplement no s'entra dins el bucle FOR IN

END;

--******************************************************************
-- OTRO EJEMPLO: programa que muestra datos de los empleados que 
--tienen el oficio ANALISTA
--******************************************************************
BEGIN
	FOR r_empleat IN (select * from emple where oficio = 'ANALISTA')
	LOOP
		DBMS_OUTPUT.PUT_LINE ( 'codigo: ' || r_empleat.emp_no || ' apellido: ' || r_empleat.apellido);
	END LOOP;

END;



