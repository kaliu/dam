create table fabricantes(
fid varchar(2) primary key,
fnombre varchar(30) not null,
status number(2),
ciudad varchar(30)
);

create table piezas(
codigo varchar(2) primary key,
pnombre varchar(30) not null,
color varchar(10),
peso number(2)
);

create table fab_piezas(
fid varchar(2) not null,
codigo varchar(2) not null,
cantidad number(3) not null,
constraint fab_piezas_pk primary key (fid, codigo),
foreign key (fid) references fabricantes(fid),
foreign key (codigo) references piezas(codigo)
);

insert into fabricantes values ('F1','Smith',20,'London');
insert into fabricantes values ('F2','Jones',10,'Paris');
insert into fabricantes values ('F3','Blake',30,'Paris');
insert into fabricantes values ('F4','Clark',20,'London');
insert into fabricantes values ('F5','Adams',30,'Athens');

insert into piezas values ('P1','Nut','Red',12);
insert into piezas values ('P2','Bolt','Green',17);
insert into piezas values ('P3','Screw','Blue',17);
insert into piezas values ('P4','Screw','Red',14);
insert into piezas values ('P5','Cam','Blue',12);
insert into piezas values ('P6','Cog','Red',19);
insert into piezas values ('P7','Cog','Red',19);

insert into fab_piezas values ('F1','P1',300);
insert into fab_piezas values ('F1','P2',200);
insert into fab_piezas values ('F1','P3',400);
insert into fab_piezas values ('F1','P4',200);
insert into fab_piezas values ('F1','P5',100);
insert into fab_piezas values ('F1','P6',100);
insert into fab_piezas values ('F2','P1',300);
insert into fab_piezas values ('F2','P2',400);
insert into fab_piezas values ('F3','P2',200);
insert into fab_piezas values ('F4','P2',200);
insert into fab_piezas values ('F4','P4',300);
insert into fab_piezas values ('F4','P5',400);

commit;
