-- 1. Mostra el cognom i el salari dels empleats que guanyen més que l'empleat amb codi 
-- 7698
SELECT apellido, salario
FROM emple
WHERE salario > (
    SELECT salario 
    FROM emple 
    WHERE emp_no = 7698
);

-- 2. Mostra el codi i el nom dels departaments que es trobin a la mateixa localitat 
-- que el departament de CONTABILIDAD
SELECT dept_no, dnombre
FROM depart
WHERE loc = (
    SELECT loc 
    FROM depart 
    WHERE dnombre = 'CONTABILIDAD'
);

-- 3. Mostra el cognom i la data de contracte per tots els empleats contractats 
-- després de l'empleat MUÑOZ
SELECT apellido, fecha_alt
FROM emple 
WHERE fecha_alt > (
    SELECT fecha_alt 
    FROM emple 
    WHERE UPPER(apellido) = 'MU¿OZ'
);

-- 4. Mostra el cognom i el número de departament de tots els empleats que treballin 
-- al departament de 'VENTAS'
SELECT apellido, dept_no
FROM emple 
WHERE dept_no = (   
    SELECT dept_no 
    FROM depart 
    WHERE dnombre = 'VENTAS'
);

-- 5. Mostra el cognom, el salari i el codi de departament de tots els empleats que 
-- cobrin més que la mitja del salari dels empleats.
SELECT apellido, salario, dept_no
FROM emple
WHERE salario > (
    SELECT AVG(salario) 
    FROM emple
);

-- 6.	Mostra el cognom, el salari i el codi de departament de tots els empleats  
-- que guanyin menys que el salari promig i treballin al mateix departament que 
-- l'empleat 'CEREZO'
SELECT apellido, salario, dept_no
FROM emple
WHERE salario < (
    SELECT AVG(salario) 
    FROM emple
) AND dept_no = (
    SELECT dept_no
    FROM emple 
    WHERE apellido = 'CEREZO'
);

-- 7. Mostra totes les dades de la última comanda que s'ha fet
SELECT *
FROM pedidos
ORDER BY fecha_pedido desc
FETCH FIRST 1 ROW ONLY;

-- 8. Mostra el cognom, el salari i el codi de departament de tots els 
-- empleats que tinguin un salari igual a algun salari dels empleats al departament 
-- amb codi 20.
SELECT apellido, salario, dept_no
FROM emple
WHERE salario = (
    SELECT salario
    FROM emple
    WHERE dept_no = 20
);

-- 9.	Mostra el cognom, el salari i el codi de departament de tots els empleats
-- que guanyen més que qualsevol dels empleats del departament amb codi 30.
SELECT apellido, salario, dept_no
FROM emple
WHERE salario > (
    SELECT salario
    FROM emple
    WHERE dept_no = 30
);

-- 10.	 Mostra el cognom, el salari i el codi de departament de tots els empleats 
-- que guanyen més que algun dels empleats del departament amb codi 20 i no siguin d'aquest departament
SELECT apellido, salario, dept_no
FROM emple
WHERE salario > (
    SELECT dept_no
    FROM emple
    WHERE dept_no = 20)
    AND dept_no != 20;
