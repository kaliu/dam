-- 1. Mostra el cognom, el salari i el codi de departament de tots els empleats 
-- que guanyen menys que qualsevol dels empleats del departament amb codi 30,
-- i no siguin d'aquest departament
SELECT apellido, salario, dept_no
FROM emple
WHERE salario < ANY (
    SELECT salario
    FROM emple 
    WHERE dept_no = 30) AND dept_no <> 30;
-- 2. Mostra el cognom, el salari i el codi de departament de tots els 
-- empleats que treballin a un departament que es trobi a una localitat 
-- on també hi hagin clients
SELECT e.apellido, e.salario, e.dept_no
FROM emple e
JOIN depart d ON (e.dept_no = d.dept_no)
WHERE d.loc = ANY
    (SELECT c.localidad
     FROM clientes c);
-- 3. Sense fer servir JOIN, mostra totes les dades dels productes que no 
-- s'han comprat mai. 
SELECT pr.*
FROM productos pr
WHERE pr.producto_no NOT IN (
        SELECT i.producto_no
        FROM item i
        );

-- 4. Sense fer servir JOIN, mostra totes les dades dels departaments 
-- que no tenen treballadors.
SELECT d.*
FROM depart d
WHERE d.dept_no NOT IN (
        SELECT e.dept_no
        FROM emple e
        );
        
-- 5. Mostra el cognom de l'empleat que va realitzar la comanda més cara.
SELECT e.apellido
FROM emple e
JOIN clientes c ON (c.vendedor_no = e.emp_no)
JOIN pedidos pe ON (pe.cliente_no = c.cliente_no)
JOIN item i ON (i.pedido_no = pe.pedido_no)
WHERE (i.cantidad * i.importe_uni) > 
       ANY (SELECT i.cantidad * i.importe_uni
          FROM item); 

-- 6. Mostra el % d'empleats al departament de VENTAS respecte al total 
-- de empleats: 100*Número d'empleats de VENTAS /Número total de empleats
SELECT 100*(SELECT COUNT(e.emp_no)
            FROM emple e
            JOIN depart d ON (e.dept_no = d.dept_no)
            WHERE d.dnombre = 'VENTAS'
            ) / (
            SELECT COUNT(e.emp_no)
            FROM emple e) AS PERCENTATGE_VENTAS
FROM dual;

-- 7. Mostra per cada producte la seva descripció, el seu preu actual i 
-- la diferència entre el seu preu actual i el preu actual del producte més barat.
SELECT p.descripcion, p.precio_actual - ( SELECT MIN(p.precio_actual)
                                           FROM productos p) AS DIFERENCIA_MAS_BARATO
FROM productos p;

-- 8. Crear una taula derivada a partir d'una consulta on es mostri el codi, 
-- la data i l'import total associat a cada  comanda. Sobre la taula derivada 
-- anterior, realitzar una consulta que mostri quins són els imports màxim i 
-- mínim de les comandes realitzades a l'any 2002.
SELECT pedido_no, importe, fecha_pedido
FROM (SELECT  (i.cantidad * importe_uni) importe, pedido_no
      FROM item i),(
      SELECT fecha_pedido
      FROM pedidos p
      WHERE TO_CHAR(fecha_pedido, 'yyyy') = 2002)
WHERE importe >= ALL (SELECT (i.cantidad * importe_uni)
                     FROM item i)
OR importe <= ALL (SELECT (i.cantidad * importe_uni)
                     FROM item i)
ORDER BY pedido_no;
      
-- 9. Mostrar el nom, el límit de crèdit i la província dels clients que 
-- tenen un límit de crèdit més gran que el promig de límit de crèdit dels 
-- clients de la seva mateixa província.
SELECT c.nombre, c.limite_credito, c.provincia
FROM clientes c
WHERE c.limite_credito > (SELECT AVG(cc.limite_credito)
                          FROM clientes cc
                          WHERE c.provincia = cc.provincia);

-- 10. Mostrar els noms del clients que han fet alguna comanda amb un 
-- cost total inferior a 200€. Està permès fer JOINs a totes les taules 
-- excepte a CLIENTES.
SELECT DISTINCT c.nombre
FROM (SELECT i.cantidad * i.importe_uni
      FROM item i
      WHERE i.cantidad * i.importe_uni < 200), clientes c;

-- 11. Mostrar per cada departament quantes comandes ha realitzat l'any 2000. 
-- Mostrar nom departament i número de comandes realitzades.
SELECT d.dnombre, (SELECT COUNT(pe.pedido_no)
                    FROM pedidos pe
                    JOIN clientes c ON (pe.cliente_no = c.cliente_no)
                    JOIN emple e ON (c.vendedor_no = e.emp_no)
                    WHERE TO_CHAR(fecha_pedido, 'yyyy') = 2002) AS pedidos
FROM depart d;