/* 1. Crear una vista anomenada "telefons_agencia" que mostri per cada agencia
 el seu nom i el seu telefon, ordenats alfabeticament pel nom de l'agencia 
 Una vegada creada la vista, executa un SELECT sobre la vista que mostri totes 
 les seves files. */
CREATE OR REPLACE VIEW telefons_agencia AS
    SELECT codi_agencia, nom_agencia, telefon
    FROM agencia
    ORDER BY nom_agencia asc;

SELECT * FROM telefons_agencia;
/* 2. Crear una vista anomenada "tickets_per_vol" que mostri per cada codi de vol
 la quantitat de tickets que te el vol, ordenats de major quantitat a menor.
 Una vegada creada la vista, executa un SELECT sobre la vista que mostri totes 
 les seves files. */
CREATE OR REPLACE VIEW tickets_per_vol AS
    SELECT codi_vol, COUNT(codi_vol) as num_tickets
    FROM tickets
    GROUP BY codi_vol
    ORDER BY num_tickets desc;

SELECT * FROM tickets_per_vol;
/* 3. Modifica la vista "telefons_agencia" per que a més a més, mostri el codi
 de la agencia. 
 Una vegada creada la vista, executa un SELECT sobre la vista que mostri totes 
 les seves files. */
CREATE OR REPLACE VIEW tickets_per_vol AS
    SELECT codi_vol, agencia, COUNT(codi_vol) as num_tickets
    FROM tickets
    GROUP BY codi_vol, agencia
    ORDER BY num_tickets desc;
    
SELECT * FROM tickets_per_vol;
/* 4. Insereix utilitzant la vista "telefons_agencia" una nova agencia amb codi 6, nom "Ferreria" i telèfon 123456
 a) Una vegada feta la inserció sobre la vista, executa un SELECT sobre la vista per comprovar que la nova agencia 
 s'ha inserit correctament.
 b)També executa un SELECT sobre la taula agencia per comprovar que la nova agencia s'ha 
 inserit correctament a la taula real. */
INSERT INTO telefons_agencia (codi_agencia, nom_agencia, telefon)
VALUES (6, 'Ferreria', 123456);

SELECT * FROM telefons_agencia;
SELECT * FROM agencia;
/* 5. Elimina les vistes telefons_agencia i tickets_per_vol. */
DROP VIEW telefons_agencia;
DROP VIEW tickets_per_vol;

/* 6. Crea un índex que no sigui únic anomenat idx_cognom_nom sobre les columnes 
cognom i nom (en aquest ordre) de la taula 'PASSATGER'.*/
CREATE INDEX idx_cognom_nom
ON passatger(cognom, nom);

/* 7. Crea un índex únic anomenat idx_nom_agencia sobre la columna nom_agencia 
de la taula agencia. */
CREATE UNIQUE INDEX idx_nom_agencia
ON agencia(nom_agencia);

/* 8. Elimina els índexs idx_cognom_nom i idx_nom_agencia */
DROP INDEX idx_cognom_nom;
DROP INDEX idx_nom_agencia;

/* 9. Crea una seqüencia anomenada agencia_seq amb valor inicial 50, que s'incrementi de 2 en 2, i amb valor màxim 
1000. Una vegada arribat al valor màxim, la seqüencia no ha de poder generar més números.
Crea una nova agencia anomenada 'Crash' a la localitat de Montcada amb data de fundació el 1 de Febrer del 2018, 
utilitzant la seqüència anterior per generar el codi de l'agencia. */
CREATE SEQUENCE agencia_seq
INCREMENT BY 2
START WITH 50
MAXVALUE 1000
NOCYCLE;

INSERT INTO agencia (codi_agencia, nom_agencia, localitat, data_fundacio)
VALUES (agencia_seq.NEXTVAL, 'Crash', 'Montcada', TO_DATE('01-02-2018', 'DD-MM-YYYY'));

/*10. Crea una seqüència anomenada vol_seq amb valor inicial 20000, que s'incrementi de 1 en 1. 
 Crear un vol amb codi avió JKL-522, sortida Barcelona, destinació Madrid, data de sortida el dia 
 d'avui i preus igual a 100, utilitzant la seqüència anterior per generar el codi del vol. */
CREATE SEQUENCE vol_seq
INCREMENT BY 1
START WITH 20000;

INSERT INTO vol (codi_vol, codi_avio, sortida, destinacio, data_sortida, preu_finestra, preu_interior)
VALUES (vol_seq.NEXTVAL, 'JKL-522', 'Barcelona', 'Madrid', SYSDATE, 100, 100);
/* 11. Mostra els valors actuals de las dos seqüències creades anteriorment. */
SELECT vol_seq.CURRVAL, agencia_seq.CURRVAL
FROM dual;

/* 12. Crea un ticket pel passatger amb passaport 987654321, tipus Interior, imports igual a 100, 
descompte igual a 0, utilitzant com codi de-vol i codi d'agencia els valors actuals de les dos 
seqüències creades anteriorment. */
INSERT INTO tickets
VALUES (vol_seq.CURRVAL, 987654321, 'Interior', 100, 0, 100, agencia_seq.CURRVAL);

/* 13. Modifica la seqüència agencia_seq per que l'increment sigui de 1 en 1. */
ALTER SEQUENCE agencia_seq
INCREMENT BY 1;

/* 14. Elimina les dues seqüències anteriors. */
DROP SEQUENCE agencia_seq;
DROP SEQUENCE vol_seq;

/* 15. Crea un sinònim anomenat viatger per la taula passatger i fes la consulta
que mostri totes les dades de la taula utilitzant el sinònim. */
CREATE OR REPLACE SYNONYM viatger FOR passatger;

SELECT * FROM viatger;

-- 16. Elimina el sinònim creat anteriorment.
DROP SYNONYM viatger;

-- 17. Executa l'ordre que permeti descriure l'estructura de la taula passatger.
DESCRIBE passatger;

/* 18. Canvia el nom de la taula passatger per viatger.
Mostra les dades de la taula viatger i finalment torna a deixar la taula amb el nom de passatger. */
RENAME passatger TO viatger;

SELECT * FROM viatger;

RENAME viatger TO passatger;

/* 19. Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti mostrar 
la QUANTITAT de taules que te el teu usuari. */
SELECT COUNT(table_name) FROM user_tables;

/* 20. Torna a crear la vista telefons_agencia.
Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti 
mostrar només el nom i el SELECT associat de les vistes que te definides el teu usuari. */
CREATE OR REPLACE VIEW telefons_agencia AS
    SELECT codi_agencia, nom_agencia, telefon
    FROM agencia
    ORDER BY nom_agencia asc;

SELECT view_name, text_vc FROM user_views;
/* 21. Torna a crear l'index idx_cognom_nom sobre la taula PASSATGER.
Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti mostrar els noms 
dels índexs que te la taula PASSATGER */
CREATE INDEX idx_cognom_nom
ON passatger(cognom, nom);

SELECT index_name 
FROM user_indexes
WHERE table_name = 'PASSATGER';
/* 22. Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti mostrar 
les columnes que tenen índexs a la taula PASSATGER. */
SELECT column_name
FROM user_ind_columns
WHERE table_name = 'PASSATGER';

/* 23. Les ordres que hagis executat abans haurien d'haver mostrat les dades de
l'index que has creat (idx_cognom_nom), però a més a més surt informació d'un altre. 
Explica quan s'ha creat aquest altre índex (escriu l'ordre SQL del fitxer aereopuerto.sql que ho ha fet). */

-- AL FER LA COLUMNA NUM_PASSAPORT UNA CLAU PRINCIPAL, S'HA INDEXAT
ALTER TABLE "PASSATGER" ADD CONSTRAINT "PK_PASSATGER" PRIMARY KEY ("NUM_PASSAPORT");


/* 24. Torna a crear la seqüència vol_seq.
Utilitzant la vista adequada del diccionari de dades, executa l'ordre  que permeti mostrar les 
dades de la seqüència anterior. */
CREATE SEQUENCE vol_seq
INCREMENT BY 1
START WITH 20000;

SELECT * 
FROM user_sequences
WHERE sequence_name = 'VOL_SEQ';
/* 25. Torna a crear el sinònim viatger per la taula passatger.
Utilitzant la vista adequada del diccionari de dades, executa l'ordre  que permeti mostrar 
les dades del sinònim anterior. */
CREATE OR REPLACE SYNONYM viatger FOR passatger;

SELECT *
FROM user_synonyms;