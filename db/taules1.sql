/* 1. Crea la taula PELICULA amb els següents camps:		

PELICULA : 
codi (nombre enter de 3 xifres)
nom (text amb un màxim de 50 caràcters)
qualificacio_edat (nombre enter de 2 xifres)

La clau primària és codi
nom no pot ser NULL.
nom ha de ser únic
qualificacio_edat ha de ser un valor positiu
qualificacio_edat té com valor per defecte 18

Nota: totes les restriccions han d’estar en forma de restricció de taula (situades al final, desprès de haver definit totes 
les columnes) i han de tenir un nom que segueixi les recomanacions de la teoria. */

CREATE TABLE movies (
    codi NUMBER (3,0),
    nom VARCHAR2 (50) NOT NULL,
    qualificacio_edat NUMBER (2,0) DEFAULT 18,
    CONSTRAINT pk_codi2 PRIMARY KEY (codi),
    CONSTRAINT uq_nom2 UNIQUE (nom),
    CONSTRAINT ck_qualificacio_edat CHECK (qualificacio_edat > 0)
);

/* 2.Insereix dues pel·lícules: una indicant totes les seves columnes i un altra sense 
indicar la qualificacio_edat (comprova que es crea amb el valor per defecte). */
INSERT INTO movies
VALUES (1, 'Zootopia', 10);
INSERT INTO movies
       (codi, nom)
VALUES (3, 'Hercules');

/* 3. Crea la taula SALA amb els següents camps: 			

SALA : 
codi (nombre enter de 2 xifres)
nom (text amb un màxim de 20 caràcters)
codi_pelicula (nombre enter de 3 xifres)

La clau primària és codi i és autonumèrica (l'usuari no pot assignar-la)
nom no pot ser NULL.
nom ha de ser únic.
codi_pelicula és una clau forana i fa referència a la taula PELICULA

Nota: totes les restriccions han d’estar en forma de restricció de taula (situades al final,
desprès de haver definit totes les columnes) i han de tenir un nom que segueixi les 
recomanacions de la teoria. */
CREATE TABLE sala (
    codi NUMBER (2, 0) GENERATED ALWAYS AS IDENTITY,
    nom VARCHAR2 (20) NOT NULL,
    codi_pelicula NUMBER (3, 0),
    CONSTRAINT pk_sala PRIMARY KEY (codi),
    CONSTRAINT uq_nom_sala UNIQUE (nom),
    CONSTRAINT fk_sala_movies FOREIGN KEY (codi_pelicula) REFERENCES movies (codi)
);

/* 4. Insereix una sala. */
INSERT INTO sala
       (nom, codi_pelicula)
VALUES ('Apolo', 1);

/* 5. Crea una nova columna anomenada planta a la taula SALA per registrar la planta 
on es troba cada sala (de tipus nombre enter de 1 xifra) */
ALTER TABLE sala ADD planta NUMBER (1,0);

/* 6. Modifica el tipus de dada de la columna PELICULA.NOM per que passi a 
ser un text de 100 caràcters. */
ALTER TABLE movies MODIFY (nom VARCHAR2(100));

/* 7. Insereix una nova restricció la columna planta per tal que els seus 
valors no es puguin repetir. */
ALTER TABLE sala ADD CONSTRAINT uq_planta UNIQUE (planta);

/* 8. Canvia el nom de la columna qualificacio_edat per qualificacio. */
ALTER TABLE movies RENAME COLUMN qualificacio_edat TO qualificacio;

/* 9. Elimina la taula PELICULAS, pots? Que passa? */
DROP TABLE movies;
-- No la puc eliminar de forma normal, tinc que utilitzar l'opcio CASCADE CONSTRAINTS
DROP TABLE movies CASCADE CONSTRAINTS;


/* 10. Crea les taules CIENTIFIC, ASSIGNACIO i PROJECTE amb els següents camps:										
-------------------
CIENTIFIC : 
dni (text amb un màxim de 9 caràcters)
nom (text amb un màxim de 50 caràcters)
La clau primària és dni
nom no pot ser NULL.
nom ha de ser únic.
------------------ */
CREATE TABLE cientific (
    dni VARCHAR2(9),
    nom VARCHAR2(50) NOT NULL,
    CONSTRAINT pk_cientific PRIMARY KEY (dni),
    CONSTRAINT uq_nom_cientific UNIQUE (nom)
);
    
/* ASSIGNACIO : 
dni_cientific (text amb un màxim de 9 caràcters)
codi_projecte (text amb 4 caràcters)
La clau primària és dni_científic + codi_projecte
dni_científic és una clau forana i fa referència a la taula CIENTIFIC
codi_projecte és una clau forana i fa referència a la taula PROJECTE
------------------ */
CREATE TABLE assignacio (
    dni_cientific VARCHAR2(9),
    codi_projecte VARCHAR2(4),
    CONSTRAINT pk_assignacio PRIMARY KEY (dni_cientific, codi_projecte),
    CONSTRAINT fk_assi_cient FOREIGN KEY (dni_cientific) REFERENCES cientific (dni),
    CONSTRAINT fk_assi_proje FOREIGN KEY (codi_projecte) REFERENCES projecte (codi)
);

/* PROJECTE : 
codi (text amb 4 caràcters)
nom (text amb un màxim de 20 caràcters)
hores (nombre enter)
La clau primària és codi.
nom no pot ser NULL.
nom ha de ser únic.
hores ha de ser un valor positiu
hores té com valor per defecte 100
-------------------- */
CREATE TABLE projecte (
    codi VARCHAR2 (4),
    nom VARCHAR2 (20) NOT NULL,
    hores NUMBER (4, 0) DEFAULT 100,
    CONSTRAINT pk_projecte PRIMARY KEY (codi),
    CONSTRAINT uq_nom_projecte UNIQUE (nom),
    CONSTRAINT ck_hores CHECK (hores > 0)
);

/* Nota: totes les restriccions han d’estar en forma de restricció de taula 
(situades al final, desprès de haver definit totes les columnes) i han de tenir 
un nom que segueixi les recomanacions de la teoria. */

