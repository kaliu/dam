-- BLOC 1

/* 1. (1 punt) Crea la taula ESTADI amb les següents columnes:
•	codi (número enter de 3 xifres; columna autonumèrica)
•	nom (text amb un màxim de 50 caràcters)
•	amplada (número de 5 xifres, dels quals 2 són decimals)
•	longitud (número de 5 xifres, dels quals 2 són decimals)
•	capacitat (número de 6 xifres sense decimals)

- la clau primària és codi
- nom no pot ser NULL.
- nom ha de ser únic
- amplada ha de ser major que 0
- longitud ha de ser major que 0
- capacitat ha de ser major que 0
Nota: especifica totes les restriccions com restricció de columna 
(a continuació de la definició de cada columna); no esteu obligats a 
utilitzar noms de restricció. */

CREATE TABLE estadi (
    codi NUMBER (3,0) GENERATED ALWAYS AS IDENTITY
        CONSTRAINT pk_codiEst PRIMARY KEY,
    nom VARCHAR2 (50) NOT NULL
        CONSTRAINT uq_nomEst UNIQUE,
    amplada NUMBER (5,2)
        CONSTRAINT ck_amplada CHECK (amplada > 0),
    longitud NUMBER (5,2)
        CONSTRAINT ck_longitud CHECK (longitud > 0),
    capacitat NUMBER (6,0)
        CONSTRAINT ck_capacitat CHECK (capacitat > 0)
);

/* 2. (1 punt) Crea la taula EQUIP amb les següents columnes:
•	codi (número enter de 2 xifres; columna autonumèrica)
•	nom (text amb un màxim de 100 caràcters)
•	societat_anonima (1 caracter)
•	codi_estadi (número enter de 3 xifres)

- La clau primària és codi 
- nom no pot ser NULL.
- nom ha de ser únic.
- societat_anonima només pot valer 'S' o 'N'
- societat_anonima te com valor per defecte 'S'
- codi_estadi és una clau forania i fa referència a la taula ESTADI

Nota: totes les restriccions han d’estar en forma de restricció de taula (situades al final, 
desprès de haver definit totes les columnes) i han de tenir un nom que segueixi les recomanacions
de la teoria. */
CREATE TABLE equip(
    codi NUMBER (2,0) GENERATED ALWAYS AS IDENTITY,
    nom VARCHAR2 (100) NOT NULL,
    societat_anonima VARCHAR2 (1) DEFAULT 'S',
    codi_estadi NUMBER (3),
    CONSTRAINT pk_codiEq PRIMARY KEY (codi),
    CONSTRAINT uq_nomEq UNIQUE (nom),
    CONSTRAINT ck_societat_anonima CHECK (societat_anonima = 'S' OR societat_anonima = 'N'),
    CONSTRAINT fk_equip_estadi FOREIGN KEY (codi_estadi) REFERENCES estadi (codi)
);

-- 3. (0.5 punts) Canvia el tipus de la columna nom de la taula ESTADI per que passi a ser un text de 100 caràcters.
ALTER TABLE estadi MODIFY nom VARCHAR(100);

-- 4. (0.5 punts) Executa l'ordre que mostri l'estructura de la taula EQUIP.
DESCRIBE equip;

-- 5. (0.5 punts) Elimina la columna societat_anonima de la taula EQUIP.
ALTER TABLE equip DROP COLUMN societat_anonima;

-- 6. (0.5 punts) Canvia el nom de la columna nom de la taula ESTADI per nom_estadi.
ALTER TABLE estadi RENAME COLUMN nom to nom_estadi;

-- 7. (0.5 punts) Desactiva la restricció de clau forània de la taula EQUIP.
ALTER TABLE equip DISABLE CONSTRAINT fk_equip_estadi;

-- 8. (1 punt) Crea una nova columna a la taula ESTADI que indiqui el tipus de superfície. 
-- Aquesta columna només pot tenir els valors 'gespa natural', 'gespa artificial', o 'sintetic'.  
-- Preferiblement fer-lo amb una ordre només.
ALTER TABLE estadi
ADD superficie VARCHAR(20)
CONSTRAINT ck_superificie CHECK (superficie = 'gespa natural' OR superficie = 'gespa artifical' OR superficie = 'sintetic');


-- BLOC 2

/* 9. (1 punt) 
a) Crea una seqüència anomenada seq_profe amb valor inicial 10, que s'incrementi 
de 1 en 1 i amb valor màxim 1000 
b) Crea dos nous professors:  Julián Gregori, nascut el 5 de Març del 1975, i 
Juan Fco González, nascut el 25 de Juny del 1965, tots dos al departament 
d'Informàtica, utilitzant la seqüència anterior per generar el codi del professor. */

-- a
CREATE SEQUENCE seq_profe
INCREMENT BY 1
START WITH 10
MAXVALUE 1000;

-- b
INSERT INTO professor
VALUES (seq_profe.NEXTVAL, 'Julián', 'Gregori', TO_DATE('05/03/1975', 'dd/mm/yyyy'), 'Informàtica');
INSERT INTO professor
VALUES (seq_profe.NEXTVAL, 'Juan Fco', 'González', TO_DATE('25/06/1965', 'dd/mm/yyyy'), 'Informàtica');

-- 10. (0.5 punts) Mostra el valor actual de la seqüència professor_seq
SELECT seq_profe.CURRVAL
FROM dual;

/* 11. (0.5 punts) Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti mostrar 
les dades de la seqüència anterior */
SELECT * FROM user_sequences
WHERE sequence_name = 'SEQ_PROFE';

/* 12. (0.5 punts) Crea un sinònim anomenat assignatura per la taula modul.
 A continuació executa l'ordre per eliminar el sinònim creat. */
CREATE SYNONYM assignatura FOR modul;

DROP SYNONYM assignatura;

/* 13. (1 punt) 
a) Crear una vista anomenada "promig_convocatoria_2" que mostri per cada mòdul, 
el seu nom i la nota mitja a la segona convocatòria, ordenats per nom de mòdul.
Nota: per que funcioni correctament és necessari assignar una alies de columna 
a la nota mitja.
b) Utilitzant la vista adequada del diccionari de dades, executa l'ordre 
que permeti mostrar només el SELECT associat de la vista anterior. */

-- a 
CREATE VIEW promig_convocatoria_2 (nom_modul, nota_mitjana) AS
    SELECT codi_modul, AVG(convocatoria_2)
    FROM matricula
    GROUP BY codi_modul
    ORDER BY codi_modul;

-- b
SELECT text
FROM user_views
WHERE view_name = 'PROMIG_CONVOCATORIA_2';

/* 14. (1 punt) 
a) Crea un índex anomenat idx_nom_modul sobre la columna nom de la taula modul. L'índex ha d'assegurar 
que el nom no es pugui repetir.
b) Utilitzant la vista adequada del diccionari de dades, executa l'ordre que permeti mostrar només 
les columnes que formen l'índex creat. */

-- a
CREATE UNIQUE INDEX idx_nom_modul
ON modul(nom);

-- b 
SELECT * FROM user_ind_columns
WHERE index_name = 'IDX_NOM_MODUL';