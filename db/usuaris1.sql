/*  1. Connexi� de l'usuari SYSTEM: crear un usuari anomenat EMPRESA. 
La contrasenya coincidir� amb les ha de ser 1111. El tablespace per defecte 
ser� USERS i la quota de disc sobre ell ser� de 100M. */
alter session set "_ORACLE_SCRIPT"=true;  

CREATE USER empresa
  IDENTIFIED BY 1111
  DEFAULT TABLESPACE USERS
  QUOTA 100M ON USERS;

GRANT CREATE SESSION TO empresa;

/*  2. Connexi� de l'usuari SYSTEM: esborrar l'usuari creat i 
torna'l a crear amb les mateixes dades. */

DROP USER empresa;

CREATE USER empresa
  IDENTIFIED BY 1111
  DEFAULT TABLESPACE USERS
  QUOTA 100M ON USERS;

/*  3. Connexi� de l'usuari SYSTEM: modificar la grand�ria de la quota sobre
el tablespace USERS, ara ser� il�limitada. */

ALTER USER empresa
QUOTA UNLIMITED ON USERS; 

/*  4. Connexi� de l'usuari SYSTEM: modificar la contrasenya a 1234. */

ALTER USER empresa
  IDENTIFIED BY 1234;

/*  5. Connexi� de l'usuari SYSTEM: realitzar una consulta sobre la vista 
dba_users que mostri les dades de l'usuari EMPRESA. Afegeix amb l�nies 
comentades el resultat de la consulta. */
SELECT * FROM DBA_USERS
WHERE username = 'EMPRESA';


/*  6. Crear una nova connexi� al vostre servidor de base de dades amb 
    l'usuari EMPRESA.  
    Connecteu-vos amb aquesta connexi�. T�ha de donar error. Indicar 
    l'error que es produeix (copia'l amb les l�nies comentades) */

-- ORA-01045: user lacks CREATE SESSION privilege; logon denied

/*    7. Connexi� de l'usuari SYSTEM: Assignar a l'usuari EMPRESA el perm�s
per poder connectar-se. */

GRANT CREATE SESSION TO empresa;

/*    8. Connexi� de l'usuari SYSTEM: Consulteu a la vista dba_sys_privs 
els privilegis que t� l'usuari EMPRESA. Afegeix amb l�nies comentades 
el resultat de la consulta. */

SELECT * FROM dba_sys_privs
WHERE grantee = 'EMPRESA';

-- EMPRESA	CREATE SESSION	NO	YES	NO

/*    9. Connexi� de l'usuari EMPRESA: consultar en la vista user_sys_privs 
els permisos que t�. Afegeix amb l�nies comentades el resultat de la consulta. 
Tanca aquesta connexi�. */

SELECT * FROM user_sys_privs;

-- EMPRESA	CREATE SESSION	NO	YES	NO

/*    10. Connexi� de l'usuari SYSTEM: bloquejar l'usuari EMPRESA. */

ALTER USER EMPRESA
  ACCOUNT LOCK;

/*    11. Connecteu-vos amb la connexi� de l'usuari EMPRESA. T�ha de donar 
error. Inclou nom�s la l�nia amb el codi d�error que es produeix 
(copia-la comentada). */

-- ORA-28000: the account is locked

/*    12. Connexi� de l'usuari SYSTEM: desbloquejar l'usuari EMPRESA. */

ALTER USER EMPRESA
  ACCOUNT UNLOCK;

/*    13. Connecteu-vos amb la connexi� de l'usuari EMPRESA. 
Ara t�hauria de deixar. Des de aquesta connexi�, consulteu utilitzant 
la taula dual el nom de l'usuari amb el que esteu connectats. Afegeix 
amb l�nies comentades el resultat de la consulta. */ 

SELECT user FROM dual;

-- EMPRESA