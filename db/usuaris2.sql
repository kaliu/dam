/*  1. Connexi� de l'usuari EMPRESA: executeu l�ordre seg�ent per crear la taula DEPART
T�hauria de donar error. Copia comentat l�error produ�t. */
 
CREATE TABLE  DEPART (    
    DEPT_NO NUMBER(2,0) NOT NULL ENABLE, 
    DNOMBRE VARCHAR2(14), 
    LOC VARCHAR2(14), 
    NUM_TREBALLADORS NUMBER(4,0) DEFAULT 0, 
    CONSTRAINT "PK_DEPART" PRIMARY KEY ("DEPT_NO"), 
    CONSTRAINT "UQ_DNOMBRE" UNIQUE ("DNOMBRE")
   );

-- ORA-01031: privilegios insuficientes

/*  2. Connexi� de l'usuari SYSTEM: assigneu a l�usuari EMPRESA el perm�s per crear taules. */

GRANT CREATE TABLE TO EMPRESA;

/*  3. Connexi� de l'usuari EMPRESA: torneu a executar l�ordre CREATE TABLE anterior, i ara hauria de 
funcionar correctament. */

-- Table DEPART creado.

/*  4. Connexi� de l'usuari EMPRESA: realitzar la seg�ent inserci� de dades. 
    No dona error per que la taula pertany a aquest usuari. */

    INSERT INTO DEPART (DEPT_NO,DNOMBRE,LOC) 
    VALUES(10,'CONTABILIDAD','MONTCADA');


/*  5. Connexi� de l'usuari SYSTEM: crear un usuari anomenat EMPLEAT. La contrasenya ha de ser 1234. El 
tablespace per defecte ser� USERS i la quota de disc sobre ell ser� de 100M. */
CREATE USER empleat
  IDENTIFIED BY 1234
  DEFAULT TABLESPACE users
  QUOTA 100M ON USERS;

/*  6. Connexi� de l'usuari SYSTEM: assignar a l�usuari EMPLEAT el perm�s per connectar-se (tal com vam 
fer quan vam crear l�usuari EMPRESA a la pr�ctica anterior). */

GRANT CREATE SESSION TO empleat;

/*  7. Crear una connexi� per l'usuari EMPLEAT.
Connexi� de l'usuari EMPLEAT: realitzar la seg�ent inserci� de dades sobre la taula DEPART de l�usuari EMPRESA. 
T�hauria de donar error. Copia comentat l�error produ�t. */

INSERT INTO EMPRESA.DEPART (DEPT_NO,DNOMBRE,LOC) 
VALUES (20,'INVESTIGACI�N','MADRID');

-- Error SQL: ORA-00942: la tabla o vista no existe

/*  8. Connexi� de l'usuari SYSTEM: 
        a) assignar a l�usuari EMPLEAT el perm�s per inserir dades de la taula EMPRESA.DEPART
        b) consulta sobre la vista dba_tab_privs els privilegis sobre objectes que t� l�usuari EMPLEAT. 
        Copia comentat el resultat de la consulta. */

GRANT INSERT ON EMPRESA.depart TO empleat;

SELECT * FROM dba_tab_privs
WHERE grantee = 'EMPLEAT';

-- EMPLEAT	EMPRESA	DEPART	EMPRESA	INSERT	NO	NO	NO	TABLE	NO

/*  9. Connexi� de l'usuari EMPLEAT: realitzar de nou la inserci� de dades sobre la taula DEPART de l�usuari EMPRESA. 
    Ara no t�hauria de donar error. */

-- 1 fila insertadas.

/*  10. Connexi� de l'usuari SYSTEM: 
        a) assignar a l�usuari EMPLEAT el perm�s per modificar dades a la columna LOC de la taula EMPRESA.DEPART
        b) consulta sobre la vista DBA_COL_PRIVS  els privilegis sobre columnes que t� l�usuari EMPLEAT. 
        Copia comentat el resultat de la consulta. */

GRANT UPDATE (loc) ON EMPRESA.depart TO empleat;

SELECT * FROM dba_col_privs
WHERE grantee = 'EMPLEAT';

-- EMPLEAT	EMPRESA	DEPART	LOC	EMPRESA	UPDATE	NO	NO	NO

/*  11. Connexi� de l'usuari SYSTEM: 
        a) assignar a l�usuari EMPLEAT el perm�s per fer tot a la taula EMPRESA.DEPART
        b) consulta sobre la vista dba_tab_privs els privilegis sobre objectes que t� l�usuari EMPLEAT. 
        Copia comentat el resultat de la consulta. */

GRANT ALL ON EMPRESA.depart TO EMPLEAT;

SELECT * FROM dba_tab_privs
WHERE grantee = 'EMPLEAT';

/*  EMPLEAT	EMPRESA	DEPART	EMPRESA	INSERT	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	ALTER	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	DELETE	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	INDEX	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	SELECT	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	UPDATE	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	REFERENCES	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	READ	    NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	ON COMMIT REFRESH	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	QUERY REWRITE	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	DEBUG	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	FLASHBACK	NO	NO	NO	TABLE	NO */


/*  12. Connexi� de l'usuari SYSTEM: 
        a) retira a l�usuari EMPLEAT el perm�s per fer consultes a la taula EMPRESA.DEPART
        b) consulta sobre la vista dba_tab_privs els privilegis sobre objectes que t� l�usuari EMPLEAT. 
        Copia comentat el resultat de la consulta. */

REVOKE SELECT ON EMPRESA.depart FROM EMPLEAT;

SELECT * FROM dba_tab_privs
WHERE grantee = 'EMPLEAT';

/*  EMPLEAT	EMPRESA	DEPART	EMPRESA	INSERT	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	ALTER	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	DELETE	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	INDEX	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	UPDATE	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	REFERENCES	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	READ	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	ON COMMIT REFRESH	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	QUERY REWRITE	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	DEBUG	NO	NO	NO	TABLE	NO
    EMPLEAT	EMPRESA	DEPART	EMPRESA	FLASHBACK	NO	NO	NO	TABLE	NO */

/*  13. Connexi� de l'usuari SYSTEM: 
        a) retira a l�usuari EMPLEAT tots els permisos sobre la taula EMPRESA.DEPART
        b) consulta sobre la vista dba_tab_privs els privilegis sobre objectes que t� l�usuari EMPLEAT. 
        Copia comentat el resultat de la consulta. */

REVOKE ALL PRIVILEGES ON EMPRESA.depart FROM empleat;

SELECT * FROM dba_tab_privs
WHERE grantee = 'EMPLEAT';

-- La taula es buida