/*  1. Connexi� de l'usuari SYSTEM: crear un rol anomenat ADMINISTRADOR */

CREATE ROLE ADMINISTRADOR;

/*  2. Connexi� de l'usuari SYSTEM: afegir al rol ADMINISTRADOR els privilegis de 
    sistema  CREATE SESSION, CREATE TABLE i CREATE VIEW. */

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW TO ADMINISTRADOR;

/*  3. Connexi� de l'usuari SYSTEM: consultar els privilegis del rol ADMINISTRADOR a 
    les vistes dba_sys_privs, dba_tab_privs, dba_col_privs, role_sys_privs i 
    role_tab_privs. Per les consultes anteriors que mostrin algun resultat, 
    inclou-lo entre comentaris.  */

SELECT * FROM dba_sys_privs WHERE grantee = 'ADMINISTRADOR';
--  ADMINISTRADOR	CREATE TABLE	NO	YES	NO
--  ADMINISTRADOR	CREATE VIEW	NO	YES	NO
--  ADMINISTRADOR	CREATE SESSION	NO	YES	NO

SELECT * FROM dba_tab_privs WHERE grantee = 'ADMINISTRADOR';

SELECT * FROM dba_col_privs WHERE grantee = 'ADMINISTRADOR';

SELECT * FROM role_sys_privs WHERE role = 'ADMINISTRADOR';
--  ADMINISTRADOR	CREATE TABLE	NO	YES	NO
--  ADMINISTRADOR	CREATE SESSION	NO	YES	NO
--  ADMINISTRADOR	CREATE VIEW	NO	YES	NO

SELECT * FROM role_tab_privs WHERE role = 'ADMINISTRADOR';

/*  4. Connexi� de l'usuari SYSTEM: retirar el privilegi CREATE VIEW. del rol ADMINISTRADOR */

REVOKE CREATE VIEW FROM ADMINISTRADOR;

/*  5. Connexi� de l'usuari SYSTEM: 
        a) assignar a l'usuari EMPRESA el rol ADMINISTRADOR.
        b) consulta a la vista dba_role_privs els rols assignats a l'usuari EMPRESA.  */

GRANT ADMINISTRADOR TO EMPRESA;

SELECT * FROM dba_role_privs WHERE grantee = 'EMPRESA';

-- EMPRESA	ADMINISTRADOR	NO	NO	YES	YES	NO

/*  6. Connexi� de l'usuari SYSTEM: crear un rol anomenat COMERCIAL */

CREATE ROLE COMERCIAL;

/*  7. Connexi� de l'usuari SYSTEM: afegir al rol COMERCIAL els privilegis d'objecte
    que permetin inserir dades i modificar el valor de la columna LOC a la taula DEPART 
    de l'usuari EMPRESA. */

GRANT INSERT, UPDATE ON EMPRESA.depart TO COMERCIAL;

/*  8. Connexi� de l'usuari SYSTEM: 
        a) assignar a l'usuari EMPLEAT el rol COMERCIAL.
        b) consulta a la vista dba_role_privs els rols assignats a l'usuari EMPLEAT. */

GRANT COMERCIAL TO EMPLEAT;

SELECT * FROM dba_role_privs WHERE grantee = 'EMPLEAT';

-- EMPLEAT	COMERCIAL	NO	NO	YES	YES	NO

/*  9. Connexi� de l'usuari SYSTEM: consultar els privilegis del rol COMERCIAL a les 
    vistes dba_sys_privs, dba_tab_privs, dba_col_privs, role_sys_privs i role_tab_privs. 
    Per les consultes anteriors que mostrin algun resultat, inclou-lo entre comentaris. */

SELECT * FROM dba_sys_privs WHERE grantee = 'COMERCIAL';

SELECT * FROM dba_tab_privs WHERE grantee = 'COMERCIAL';

-- COMERCIAL	EMPRESA	DEPART	EMPRESA	INSERT	NO	NO	YES	TABLE	NO
-- COMERCIAL	EMPRESA	DEPART	EMPRESA	UPDATE	NO	NO	YES	TABLE	NO

SELECT * FROM dba_col_privs WHERE grantee = 'COMERCIAL';

SELECT * FROM role_sys_privs WHERE role = 'COMERCIAL';

SELECT * FROM role_tab_privs WHERE role = 'COMERCIAL';

-- COMERCIAL	EMPRESA	DEPART		INSERT	NO	YES	NO
-- COMERCIAL	EMPRESA	DEPART		UPDATE	NO	YES	NO

/*  10. Connexi� de l'usuari SYSTEM: 
        a) afegir el rol COMERCIAL al rol ADMINISTRADOR.
        b) consultar els rols que inclou el rol ADMINISTRADOR a la vista role_role_privs. */

GRANT COMERCIAL TO ADMINISTRADOR;

SELECT * FROM role_role_privs WHERE role = 'ADMINISTRADOR';

-- ADMINISTRADOR	COMERCIAL	NO	YES	NO

/*  11. Connexi� de l'usuari SYSTEM: retirar a l'usuari EMPLEAT el rol COMERCIAL. */

REVOKE COMERCIAL FROM EMPLEAT;

/*  12. Connexi� de l'usuari SYSTEM: eliminar el rol COMERCIAL. */

DROP ROLE COMERCIAL;

/*  13. Connexi� de l'usuari SYSTEM: crear el perfil anomenat USUARI_NORMAL amb les seg�ents 
    limitacions:
        � No pot tenir m�s de 1 sessions simult�nies connectades a la base de dades. 
        � El n�mero m�xim d'errors consecutius en les contrasenyes abans de bloquejar el 
        compte �s 3.
        � El temps de connexi� m�xim �s el valor per defecte.
        � El temps d�inactivitat m�xim que es permet a un usuari �s 2 minuts.
        � El n�mero m�xim de dies que t� validesa una contrasenya �s 90.  */

CREATE PROFILE USUARI_NORMAL LIMIT
    SESSIONS_PER_USER 1
    FAILED_LOGIN_ATTEMPTS 3
    IDLE_TIME 2
    PASSWORD_LIFE_TIME 90;

/*  14. Connexi� de l'usuari SYSTEM: assignar a l'usuari EMPLEAT el perfil USUARI_NORMAL. */

ALTER USER EMPLEAT PROFILE USUARI_NORMAL;

/*  15. Connexi� de l'usuari SYSTEM: modifica el perfil USUARI_NORMAL per tal que el n�mero 
    m�xim de sessions simult�nies connectades a la base de dades sigui il�limitat 
    i el temps de connexi� m�xim sigui 120 minuts. */

ALTER PROFILE USUARI_NORMAL LIMIT
    SESSIONS_PER_USER UNLIMITED
    CONNECT_TIME 120;

/*  16. Connexi� de l'usuari SYSTEM: eliminar el perfil USUARI_NORMAL */

DROP PROFILE USUARI_NORMAL CASCADE;