
package pkg05b_exapren;
import java.util.Scanner;

public class exercici1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        String lletresDNI = "TRWAGMYFPDXBNJZSQVHLCKE";
        System.out.println("Introdueix el teu NUMERO de DNI:");
        int numDNI = input.nextInt();
        System.out.println("Introdueix la lletra del teu DNI:");
        char inputLletraDNI= input.next().charAt(0);
        
        int punterLletraDNI = numDNI % 23;
        boolean esCorrecte = inputLletraDNI == lletresDNI.charAt(punterLletraDNI);
        System.out.println("La teva lletra és " + lletresDNI.charAt(punterLletraDNI));
        System.out.printf("Coincideix amb la lletra introduida? %s%n", esCorrecte);
    }
}