
package pkg05b_exapren;
import java.util.Scanner;

public class exercici3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Es demana que el usuari introdueixi el any vigent
        System.out.println("Introdueix l'any en el que estem:");
        int any = input.nextInt();
        
        // Es calculen els moduls que son necessaris per la comprobació
        int div4 = any % 4;
        int div100 = any % 100;
        int div400 = any % 400;
        
        // Si l'any introduit es divisible per 4 i no ho es per 100 (tot i que 
        // pot ser-ho per 400) es un any de traspas
        boolean esTraspas = div4 == 0 && (div100 > 0 || div400 == 0);
        System.out.printf("Es un any de traspàs? %s%n", esTraspas);
        
    }
}