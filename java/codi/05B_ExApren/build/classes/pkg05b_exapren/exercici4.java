
package pkg05b_exapren;
import java.util.Scanner;

public class exercici4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix el teu nom:");
        String nom = input.nextLine();
        System.out.println("Introdueix la teva edat:");
        int edat = input.nextInt();
        System.out.println("De quina franja horaria vols entrar(1,2 o 3)?");
        int franja = input.nextInt();
        System.out.println("Pots venir dimarts o dijous? (true/false)");
        boolean dimartsDijous = input.nextBoolean();
        
        boolean teDescompte = edat <= 25 && franja == 1 && dimartsDijous == true;
        System.out.printf("Hola %s, en el teu cas la matrúcula amb descompte es %s%n", nom.toUpperCase(), teDescompte);
    }
}
