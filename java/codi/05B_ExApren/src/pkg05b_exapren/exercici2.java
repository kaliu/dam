
package pkg05b_exapren;
import java.util.Scanner;

public class exercici2 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        double costat1 = input.nextDouble();
        double costat2 = input.nextDouble();
        double costat3 = input.nextDouble();
        
        boolean esPossible = (costat1 + costat2) > costat3 || (costat1 + costat3) > costat2 || (costat3 + costat2) > costat1;
        System.out.println(esPossible);
        
        boolean esEquilater = costat1 == costat2 && costat2 == costat3;
        System.out.println(esEquilater);
        
    }
}
