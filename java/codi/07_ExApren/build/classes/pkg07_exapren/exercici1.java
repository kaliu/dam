
package pkg07_exapren;

import java.io.PrintStream;

public class exercici1 {

    public static void main(String[] args) {
        final String DNI = "51285153Y";
        final char INICIAL = 'R';
        final boolean CARNETCONDUIR = false;
        final int NUMSABATA = 47;
        final int DIESANY = 365;
        
        // velocitatLlum en km/s
        final int VELOCITATLLUM = 300000;
        
        // parsec en anys llum
        final double PARSEC = 3.2616;
        
        final double ALCADA = 1.76;
        final double ROBATORI = 10.3;
        
        System.out.printf("El meu DNI és %s.%n"
                + "La meva inicial es %s.%n"
                + "Tinc carnet de conduir? %s.%n"
                + "Quin es el meu número de sabata? %d.%n"
                + "Quants dies te un any? %d dies.%n"
                + "A quina velocitat va la llum? %d km/s.%n"
                + "Quan es 1 parsec en anys llum? Aproximadament %.2f anys llum.%n"
                + "Quant medeixes? %.2f metres.%n"
                + "Han robat %.2f millons d'€ del banc d'aqui al costat!%n",
                DNI, INICIAL, CARNETCONDUIR, NUMSABATA, DIESANY, VELOCITATLLUM,
                PARSEC, ALCADA, ROBATORI);
    }
    
}
