
package pkg07_exapren;
import java.util.Scanner;

public class exercici2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        final int constant = 2;
        
        System.out.println("Introdueix un número (pot contenir decimals):");
        double valorIntroduit = input.nextDouble();
        
        valorIntroduit += (constant * 3);
        System.out.printf("El resultat de sumar el valor introduit 2 * 3 és %.2f%n", valorIntroduit);
        valorIntroduit -= (constant / 2);
        System.out.printf("El resultat de restar el resultat anterior a 2 / 2 és %.2f%n", valorIntroduit);
        valorIntroduit *= (constant + 4);
        System.out.printf("El resultat de multiplicar el resultat anterior a 2 + 4 és %.2f%n", valorIntroduit);
        valorIntroduit /= constant;
        System.out.printf("El resultat de dividir el resultat anterior entre 2 és %.2f%n", valorIntroduit);
    }
}