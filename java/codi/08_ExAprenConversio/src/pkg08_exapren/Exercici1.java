package pkg08_exapren;

import java.util.Scanner;

/**
 * EXERCICI 1
 * Fer un programa que demani la temperatura i pressió amb valors decimals 
 * i que s'emmagatzemin en variables de tipus int, realitzant les conversions 
 * explícites que es necessitin. 
 * 
 * S'han de mostrar per pantalla les dades de temperatura i pressió introduïdes 
 * convertides al tipus int.
 *
 */

public class Exercici1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix la temperatura d'avui(amb decimals):");
        double temp = input.nextDouble();
        System.out.println("Introdueix la pressió d'avui(amb valors decimals):");
        double press = input.nextDouble();
        
        System.out.printf("La temperatura i la pressió d'avui (sense decimals) és %dºC i %d bar(s)%n", (int) temp, (int) press );    
        
    }
    
}
