package pkg08_exapren;

/**
 * EXERCICI 
 * ----------
 * 
 * 
 * 
 */
public class Exercici3 {

	public static void main(String[] args) {
		
		byte valorByte1 = 10;
		byte valorByte2 = 2;
		
		char valorChar1 = 'a';
		char valorChar2 = 'b';
		
		int valorInt1 = 100;
		int valorInt2 = 200;
		
		long valorLong1 = 1000;
		long valorLong2 = 2000;		

		float valorFloat1 = 1000.1234f;
		float valorFloat2 = 2000.5678f;
		
		double valorDouble1 = 100000.1234;
		double valorDouble2 = 200000.5678;		

		//1. Guarda a una variable el resultat de valorByte1 / valorByte2
		//sense fer cap conversió explícita (casting)
                int act1 = valorByte1 / valorByte2;
		
		//2. Guarda a una variable el resultat de valorChar1 + valorChar2
		//sense fer cap conversió explícita (casting)
		int act2 = valorChar1 + valorChar2;
		
		//3. Guarda a una variable de tipus "int" el resultat de
		//valorLong1 * valorLong2
		int act3 = (int) valorLong1 * (int) valorLong2;
		
		//4. Guarda a una variable el resultat de
		//(valorByte1 * valorLong2)/valorDouble1 sense fer
		//cap conversió explícita (casting)
		
                double act4 = (valorByte1 * valorLong2)/valorDouble1;
		
		//5. Guarda a una variable de tipus "float" el resultat de
		//(valorByte1 * valorLong2)/valorDouble1 
		
                float act5 = (float) ((valorByte1 * valorLong2)/valorDouble1);
		
		//6. Indica el tipus de la variable result6
		//RAONA LA RESPOSTA
		//result6 = valorChar1*valorFloat1 + (valorByte1 * valorLong2)/valorDouble1;
		double act6 = valorChar1*valorFloat1 + (valorByte1 * valorLong2)/valorDouble1;
		
                // La resposta es double perque li podem assignar de forma implicita tots els tipus
                // demanats a la operació

	}
}
