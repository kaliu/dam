
package pkg08_exapren;



/**
 * Exercici 4
 * ==============
 * Escriu un programa que demani un valor del preu d'un llibre (número amb decimals) 
 * i mostrar el preu final resultat d'aplicar l'IVA (una constant de valor 20).

 * 
 * Recordeu que per afegir un percentatge d'un valor  al propi valor hem d'aplicar
 * la següent fòrmula:
 *          valorFinal = valor * ( 1 + percentatge / 100)
 * 
 * Exemple
 * Introduir el preu inicial
 * 100
 * El preu final és 120.
 * 
 */
import java.util.Scanner;
public class Exercici4 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final int iva = 20;
        
        System.out.println("Introdueix el preu sense IVA:");
        double preuSenseIva = input.nextDouble();
        
        double preuFinal = (int) preuSenseIva * (1 + (iva / 100));
        
        System.out.printf("El preu final sense decimals és %d", preuFinal);
    }
    
}
