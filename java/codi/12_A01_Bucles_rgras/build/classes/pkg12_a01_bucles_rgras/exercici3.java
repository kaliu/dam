
package pkg12_a01_bucles_rgras;
import java.util.Scanner;

public class exercici3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Benvingut a la calculadora");
        System.out.println("Introdueïx un nombre:");
        int num1 = input.nextInt();
        System.out.println("Introdueïx un altre nombre:");
        int num2 = input.nextInt();
        int result = 0;
        while (true) {
            System.out.printf("%nSelecciona la operació desitjada a fer%n"
                            + "amb els nombres %d i %d. (+ - * /).%n"
                            + "També pots introduïr una E per sortir.%n"
                            , num1, num2);
            char signe = input.next().charAt(0);
            switch (signe){
                    case '+':
                       result = num1 + num2;
                       break;
                    case '-':
                       result = num1 - num2;
                       break;
                    case '*':
                       result = num1 * num2;
                       break;
                    case '/':
                       result = num1 / num2;
                       break;
                    case 'E':
                       return;
                    default:
                        System.out.println("Error.");
            }
            System.out.printf("El resultat de %d %s %d és %d.%n", num1, signe, num2, result);
        }
        
    }
}
