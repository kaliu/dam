
package pkg12_a01_bucles_rgras;
import java.util.Scanner;

public class exercici4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueïx un nombre. Aquest programa"
                         + "retornarà els seus multiples.");
        int num = input.nextInt();
        
        for(int i = 1; i <= num; i++) {
            if (num % i == 0 && i != num) {
                System.out.printf("%d,", i);
            } 
            else if (i == num){
                System.out.printf("%d. %n", i);
            }
        }
    }
}
