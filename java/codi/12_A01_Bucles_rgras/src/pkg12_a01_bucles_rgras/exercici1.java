
package pkg12_a01_bucles_rgras;
import java.util.Scanner;

public class exercici1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Introdueïx un nombre enter positiu:");
        int num = input.nextInt();
        int result = 0;
        
        if (num > 0) {
            for (int i = 0; i <= num; i++) {
                result += i; 
            }
        } else {
            System.out.println("Error, número menor que 0.");
        }
        
        if (result > 0) {
            System.out.printf("El resultat de sumar tots%nels numeros desde 0 "
                    + "fins el %nnumero introduit: %d%n", result);
        }
    }
    
}
