package pkg12_a01_bucles_rgras;
import java.util.Scanner;

public class exercici2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Es múltilpe de 3? : El programa");
 
        while(true) {
            System.out.println("Introdueïx un nombre: (0 si vols sortir)");
            int num = input.nextInt();
            if (num == 0) {
                System.out.println("Sortint...");
                return;
            } else if (num % 3 == 0) {
                System.out.println("Correcte, es un nombre múltiple de 3");
            } else {
                System.out.println("No es múltiple.");
            }
        }
    }
}
