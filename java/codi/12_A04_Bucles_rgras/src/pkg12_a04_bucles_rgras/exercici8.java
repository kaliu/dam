
package pkg12_a04_bucles_rgras;
import java.util.Scanner;

public class exercici8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        String contra = "BONDIA!";
        int intents = 0;
    
        boolean incorrecta = true;
        while (incorrecta) {
            System.out.println("Introdueix la contrasenya");
            String inCont = input.nextLine();
            if (inCont.equals(contra)) {
                System.out.println("Correcte! Intents necessitats: " + intents);
                incorrecta = false;
            } else {
                intents++;
                System.out.println("Incorrecte! Intents fallits: " + intents);
            }
        }
    
    }
}