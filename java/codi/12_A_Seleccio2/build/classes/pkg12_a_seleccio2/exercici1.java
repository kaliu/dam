package pkg12_a_seleccio2;
import java.util.Scanner;

public class exercici1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Introdueïx la temperatura a la que es troba el aigua:");
        double temp = input.nextDouble();
        String estat = null;
        
        if (temp <= 0) {
            estat = "Sólid.";
        }
        else if (temp > 0 && temp <= 100) {
            estat = "Liquid.";
        }
        else if (temp > 100) {
            estat = "Gasós.";
        }
        
        System.out.printf("L'estat del aigua a %.2fºC és %s%n", temp, estat);
        
    }
    
}
