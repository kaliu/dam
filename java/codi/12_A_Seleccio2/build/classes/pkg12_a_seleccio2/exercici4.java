package pkg12_a_seleccio2;
import java.util.Scanner;

public class exercici4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String resposta;
        
        System.out.printf("Programa de caixer automàtic:%n"
                        + "A. Extreure%n"
                        + "B. Ingressar%n"
                        + "C. Últims Moviments%n"
                        + "D. Sortir del menú%n"
                        + "Premi l'opció dessitjada:%n");
        char opcio = input.nextLine().charAt(0);
        
        switch (opcio){
                case 'A':
                    resposta = "Vosté ha seleccionat Extreure diners";
                    break;
                case 'B':
                    resposta = "Vosté ha seleccionat Ingressar diners";
                    break;
                case 'C':    
                    resposta = "Vosté ha seleccionat veure els últims moviments";
                    break;
                case 'D':
                    resposta = "Vosté ha seleccionat sortir del menú.";
                    break;                    
                default:
                    resposta = "Opció invalida";
                    break;
        }
        System.out.println(resposta);
    }
}
