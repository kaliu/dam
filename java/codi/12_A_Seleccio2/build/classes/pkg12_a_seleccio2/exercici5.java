
package pkg12_a_seleccio2;
import java.util.Calendar;

public class exercici5 {
    public static void main(String[] args) {
        Calendar dataAvui = Calendar.getInstance();
        int dia = dataAvui.get(Calendar.DAY_OF_MONTH);
        int mes = dataAvui.get(Calendar.MONTH)+1;
        int any = dataAvui.get(Calendar.YEAR);    
        int diaDema = dia + 1;
        int mesDema = mes; 
        int anyDema = any;
        
        int diesMes = dataAvui.getActualMaximum(Calendar.DAY_OF_MONTH);
        
        if (dia == diesMes) {
            mesDema += 1;
            diaDema = 1;
        }
        if (mes == 12) {
            mesDema = 1;
            anyDema += 1;
        }    
        System.out.printf("La data d'avui és %d/%d/%d.%n", dia, mes, any);
        System.out.printf("Demà serà %d/%d/%d.%n", diaDema, mesDema, anyDema);

    }
}