package pkg12_a_seleccio2;
import java.util.Scanner;
import java.util.Calendar;


public class exercici6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String result;
        
        System.out.println("Introdueïx la teva data de naixement segons el següent format d'exemple: 01 de Gener");
        String dataNaix = input.nextLine();
        int diaNaix = Integer.parseInt(dataNaix.substring(0, 2));
        String mesNaix = dataNaix.substring(6, dataNaix.length());
        
        String signe = "";
        String signeUni = "";
        
        switch (mesNaix) {
            case "Gener":
                if (diaNaix>20) {
                    signe = "Aquari";
                    signeUni = "(♒)";
                } else {
                    signe = "Capricorn";
                    signeUni = "(♑)";
                }
                break;
            case "Febrer":
                if (diaNaix<20) {
                    signe = "Aquari";
                    signeUni = "(♒)";
                } else {
                    signe = "Pisces";
                    signeUni = "(♓)";
                }
                break;
            case "Marc":
                if (diaNaix>20) {
                    signe = "Aries";
                    signeUni = "(♈)";
                } else {
                    signe = "Pisces";
                    signeUni = "(♓)";
                }
                break;
            case "Abril":
                if (diaNaix>19) {
                    signe = "Taure";
                    signeUni = "(♉)";
                }
                else {
                    signe = "Aries";
                    signeUni = "(♈)";
                }
                break;
            case "Maig":
                if (diaNaix>20) {
                    signe = "Gèminis";
                    signeUni = "(♊)";
                } else {
                    signe = "Taure";
                    signeUni = "(♉)";
                }
                break;
            case "Juny":
                if (diaNaix>21) {
                    signe = "Càncer";
                    signeUni = "(♋)";
                } else {
                    signe = "Gèminis";
                    signeUni = "(♊)";
                }
                break;
            case "Juliol":
                if (diaNaix>21) {
                    signe = "Lleó";
                    signeUni = "(♌)";
                } else {
                    signe = "Càncer";
                    signeUni = "(♋)";
                } 
                break;
            case "Agost":
                if (diaNaix>21) {
                    signe = "Verge";
                    signeUni = "(♍)";
                } else {
                    signe = "Lleó";
                    signeUni = "(♌)";
                }
                break;
            case "Septembre":
                if (diaNaix>22) {
                    signe = "Balança";
                    signeUni = "(♎)";
                } else {
                    signe = "Verge";
                    signeUni = "(♍)";
                }
                break;
            case "Octubre":
                if (diaNaix>22) {
                    signe = "Ecorpió";
                    signeUni = "(♏)";
                } else {
                    signe = "Balança";
                    signeUni = "(♎)";
                }
                break;
            case "Novembre":
                if (diaNaix>21) {
                    signe = "Sagitari";
                    signeUni = "(♐)";
                } else {
                    signe = "Escorpió";
                    signeUni = "(♏)";
                }
                break;
            case "Desembre":
                if (diaNaix>21) {
                    signe = "Capricorn";
                    signeUni = "(♑)";
                } else {
                    signe = "Sagitari";
                    signeUni = "(♐)";
                }
        }
        
        System.out.printf("El teu signe del zódiac és %s%s.%n", signe, signeUni);
        
    }
}

// System.out.println(diaNaix + mesNaix);