
package pkg12_a_seleccio2;
import java.util.Scanner;

public class exercici3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Introdueïx la data d'avui amb el fomat DD:MM:AAAA");
        String rawData = input.nextLine();
 
        if (rawData.length() != 10) {
            System.out.println("Error: falten caràcters per completar el format");
            return;
        }

        int diaProc = Integer.parseInt(rawData.substring(0, 2));
        int mesProc = Integer.parseInt(rawData.substring(3, 5));
        int anyProc = Integer.parseInt(rawData.substring(6, 10));
        
        String mesLletra = null;
        switch (rawData.substring(3, 5)){
            case "01": mesLletra = "Gener";
                     break;
            case "02": mesLletra = "Febrer";
                     break;
            case "03": mesLletra = "Marc";
                     break;
            case "04": mesLletra = "Abril";
                     break;
            case "05": mesLletra = "Maig";
                     break;
            case "06": mesLletra = "Juny";
                     break;
            case "07": mesLletra = "Juliol";
                     break;
            case "08": mesLletra = "Agost";
                     break;
            case "09": mesLletra = "Septembre";
                     break;
            case "10": mesLletra = "Octubre";
                     break;
            case "11": mesLletra = "Nobrembre";
                     break;
            case "12": mesLletra = "Desembre";
                     break;                     
        }
        
        boolean dosPunts = false;
        boolean noMes12Mes = false;
        boolean diesValidsXMes = true;
        boolean esTraspas = (anyProc % 4) == 0 && (anyProc % 100) > 0 || (anyProc % 400) == 0;
        
        if (rawData.charAt(2) == ':' && rawData.charAt(5) == ':') {
            dosPunts = true;
        } else {
            System.out.println("Error: El format no es correcte.");
            return;
        }
        if (mesProc <= 12 && mesProc > 0) {
            noMes12Mes = true;
        } else {
            System.out.println("Error: l'any no pot tenir més de 12 mesos o ser menor que 0.");
            return;
        }
        if (esTraspas != true) {
            if (mesProc == 02 && diaProc == 29) {
                System.out.println("Error: aquest any no es de traspás per lo que "
                                   + "febrer no pot tenir 29 dies");
                return;
            }
        }
        if (diaProc > 31 || diaProc <= 0) {
                System.out.println("Error: el mes, sigui el any que sigui, sols "
                                   + "te un màxim de 31 dies i tampoc poden ser zero.");
                return;
        }        
        if (dosPunts == true && noMes12Mes == true && diesValidsXMes == true) {
            System.out.printf("Data introduïda: %d de %s de %d.%n", diaProc, mesLletra, anyProc);
        }
        else {
            System.out.println("ERROR FATAL: El programa no ha pogut completarse per raons "
                                + "desconegudes.");
        }
    }
}
