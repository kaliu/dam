
package pkg12_r04_rgras;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) { 
        Scanner input = new Scanner(System.in);
        
        int counter = 1;
        System.out.println("Entra un nombre entre 1 i 10:");
        int num = input.nextInt();
        
        while(counter <= num){
            counter++;
            System.out.printf("%d%d%d", counter, counter--, counter);
        }
    }
    
}
