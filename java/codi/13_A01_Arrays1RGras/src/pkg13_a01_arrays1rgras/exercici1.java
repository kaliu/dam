package pkg13_a01_arrays1rgras;

import java.util.Scanner;

public class exercici1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] diesSetmana = {"Dilluns", "Dimarts", "Dimecres", "Dijous",
        "Divendres", "Dissabte", "Diumenge"};
        String[] diesSetmanaIng = {"Monday", "Tuesday", "Wednesday", "Thursday",
        "Friday", "Saturday", "SUnday"};
        String[] diesSetmanaCopy = new String[7];
        
        while(true){
            System.out.printf("%n1. Mostra el contingut del array %n"
                            + "2. Reseteja el contingut del array %n"
                            + "3. Escriu novament el contingut del Array en anglès %n" 
                            + "4. Copia el array en un segon array %n"  
                            + "5. Mostra el contingut del segon array %n" 
                            + "6. Sortir %n");
            
            System.out.print("Selecciona una opció: ");
            int selec = input.nextInt();
            
            switch(selec){
                case 1:
                    System.out.printf("%nEl contingut de l'array es el seguent:");
                    if("".equals(diesSetmana[1])){
                        System.out.println(" L'Array esta buit.");
                    } else {
                        for(int i = 0; i < 7; i++){
                        System.out.printf("%n" + diesSetmana[i] + " ");
                        }
                    }
                    System.out.println("");
                    break;
                case 2:
                    System.out.println("El valor del Array ha sigut resetejat.");
                    for(int i = 0; i < 7; i++){
                        diesSetmana[i] = "";
                    }
                    break;
                case 3:
                    for(int i = 0; i < 7; i++){
                        diesSetmana[i] = diesSetmanaIng[i];
                    }
                    break;
                case 4:
                    for(int i = 0; i < 7; i++){
                        diesSetmanaCopy[i] = diesSetmana[i];
                    }
                    break;
                case 5:
                    System.out.printf("%nEl contingut del segon Array es el seguent:");
                    if("".equals(diesSetmanaCopy[1])){
                        System.out.println(" El segon Array esta buit.");
                    } else {
                        for(int i = 0; i < 7; i++){
                            System.out.printf("%n" + diesSetmanaCopy[i] + " ");
                        }
                    }
                    System.out.println("");
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opció invalida");
            }
        }
    }
    
}
