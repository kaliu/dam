package pkg13_a01_arrays1rgras;
import java.util.Scanner;

public class exercici2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double[] temp = {20.5, 22, 18.9, 26, 29.3, 23};
        
        System.out.printf("%nDe quina de les següents "
                        + "poblacions vols saber la temperatura?%n"
                        + "1. Barcelona%n"
                        + "2. Tarragona%n"
                        + "3. Lleida%n"
                        + "4. Girona%n"
                        + "5. Figueres%n"
                        + "6. Montseny%n");
        int selec = input.nextInt();
        
        if(selec <= 6 && selec > 0){
            System.out.printf("%nLa temperatura és: %.2fºC%n", temp[selec-1]);
        } else {
            System.out.printf("%nOpció invalida%n");
        }
    }
}
