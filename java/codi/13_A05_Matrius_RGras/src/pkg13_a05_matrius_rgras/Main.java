package pkg13_a05_matrius_rgras;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        String[][] nomProd = {{"Poma", "Pera", "Plàtan"},
                             {"Pruna", "Taronja", "Cirera"},
                             {"Mora","Mandarina","Maduixa"}};
        double[][] preu = {{1.1, 0.8, 1.5},  
                          {1.8, 1, 1.2}, 
                          {1.8, 1.3, 1.2}};
        int[][] estoc = {{10, 10, 10}, {10, 10, 10}, {10, 10, 10}};
        
        
        while(true){
            System.out.printf("%nMenú de la màquina expenedora:%n"
                            + "1. Mostrar items%n"
                            + "2. Comprar fruita%n"
                            + "3. Mostrar estoc%n"
                            + "4. Emplenar fruita%n"
                            + "5. Apagar maquina%n");
            
            int respuesta = input.nextInt();
            
            switch(respuesta){
                case 1:
                    for(int i = 0; i < nomProd.length; i++){
                        for(int j = 0; j < nomProd[i].length; j++){
                            System.out.printf("%14s, Codi: %d %d, %.2f€", nomProd[i][j], i+1, j+1, preu[i][j]);
                        }
                    System.out.println("");
                    }
                    break;
                    
                case 2:
                    System.out.println("Tecleja el codi de la columna i, després, la filera de la fruita"
                                     + "que vols comprar");
                    int columna = input.nextInt();
                    int fila = input.nextInt();
                    
                    if(estoc[columna-1][fila-1] > 0){
                        System.out.printf("Dispensant %s. El preu a pagar és %.2f€.", nomProd[columna-1][fila-1], preu[columna-1][fila-1]);
                        estoc[columna-1][fila-1] -= 1;
                    } else {
                        System.out.println("%nError, no queda estoc d'aquesta fruita.");
                    }
                    break;
                
                case 3:
                    for(int i = 0; i < nomProd.length; i++){
                        for(int j = 0; j < nomProd[i].length; j++){
                            System.out.printf("%14s, Estoc: %d", nomProd[i][j], estoc[i][j]);
                        }
                    System.out.println("");
                    }
                    break;
                case 4:
                    System.out.printf("Aquesta opció es exclusiva del personal%n"
                                    + "de manteniment i es requereix una contrasenya%n"
                                    + "per continuar:%n");
                    String contra = input.next();
                    
                    if("esbromapoma".equals(contra)){
                        System.out.println("Contrasenya correcta. Introdueix la"
                                + " columna i filera de la fruita que vols emplenar:");
                        
                    } else {
                        System.out.println("Contrasenya incorrecta.");
                    }
                    
                    break;
                case 5:
                    System.exit(0);
                    break;
            }
        }
    }
    
}
