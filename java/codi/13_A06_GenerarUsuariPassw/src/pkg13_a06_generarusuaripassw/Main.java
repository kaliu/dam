package pkg13_a06_generarusuaripassw;
import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int supUser = 2;
        Scanner input = new Scanner(System.in);
        int longPasswd = 8;
        
        String[] user = {};
        String[] pass = {};
        String[] nomComplet = {};
        
        while(true){
            String tmppass = "";
            
            
            System.out.printf("%nBenvingut al sistema de registre.%n"
                            + "Introdueix el teu Nom: ");
            String nom = input.nextLine();
            System.out.printf("Introdueix el teu primer cognom: ");
            String cognom1 = input.nextLine();
            System.out.printf("Introdueix el teu segon cognom: ");
            String cognom2 = input.nextLine();
            
            user = Arrays.copyOf(user, user.length + 1);
            String miniUser = (nom.charAt(0) + cognom1).toLowerCase();
            for(int j = 0; j < user.length; j++){
                if(miniUser.equals(user[j])){
                    miniUser = miniUser + supUser;
                    supUser++;
                }
            }
            
            user[user.length - 1] = miniUser;
            
            nomComplet = Arrays.copyOf(nomComplet, nomComplet.length + 1);
            nomComplet[nomComplet.length - 1] = (nom + " " + cognom1 + " " + cognom2);
            
            pass = Arrays.copyOf(pass, pass.length + 1);

            for(int i = 0; i < longPasswd; i++){
                
                int selec = (int) (Math.random() * ((2 - 1) + 1)) + 1;
                switch(selec){
                    case 1:
                        int letraRand = (int) (Math.random() * ((122 - 97) + 1)) + 97;
                        tmppass = tmppass + (char) letraRand;
                        break;
                    case 2:
                        int numRand = (int) (Math.random() * ((1 - 9) + 1)) + 9;
                        tmppass = tmppass + numRand;
                    break;
                }
            }
            pass[pass.length - 1] = tmppass;

            
            for(int w = 0; w < user.length; w++){
                System.out.printf("%nFicha d'usuari: %n"
                                + "Nom Complet: %s %n"
                                + "Usuari: %s %n"
                                + "Password: %s %n",
                                user[w], nomComplet[w], pass[w]);
            }
        }
        
    }
    
}
