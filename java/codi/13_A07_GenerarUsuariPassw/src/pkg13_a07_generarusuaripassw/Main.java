package pkg13_a07_generarusuaripassw;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Main {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        boolean userAuth = false, adminMenu = false, userMenu = false;
        int usrID = 0;
        
        // Configuració
        int longPasswd = 8; // Longitud de la contrasenya generada
         // Número per el que comença a comptar al repetir usuari
        String admPass = "P@ssw0rd!"; // Contrasenya del administrador
        // -----------
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); // Format data
        
        // ARRAYS
        String[] nom = {""};
        String[] cognom = {""};
        String[] user = {""};
        String[] pass = {""};
        Date[] lastLogin = {null};
        // ------
        
        
        while(true){
            // MENU PRINCIPAL
            System.out.printf("      Accés al sistema%n"
                            + "----------------------------%n"
                            + "1. Entrar com administrador%n"
                            + "2. Entrar com usuari%n"
                            + "3. Sortir%n");
            int selPrin = input.nextInt();
            switch(selPrin){
                case 1:
                    System.out.println("Introdueïx la contrasenya:");
                    String admPassIn = input.next();
                    if(admPass.equals(admPassIn)){
                        System.out.println("Contrasenya correcta!");
                        adminMenu = true;
                    } else {
                        System.out.println("Contrasenya incorrecta. Tornant al menú");
                    }
                    break;
                case 2:
                        userAuth = true;
                    break;
                case 3:
                    System.exit(0);
                    break;
            }
            // --------------
            
            // MENU DEL ADMIN
            while(adminMenu){
                System.out.printf("%nBenvingut administrador. Seleccioni una de les%n"
                                + "següents opcions: %n"
                                + "1. Mostrar tots els noms, cognoms i login dels usuaris%n"
                                + "2. Saber quants usuaris s'han donat d'alta en algun moment%n"
                                + "3. Saber quan va ser l'ultim accés del usuari%n"
                                + "4. Donar de baixa un usuari%n"
                                + "5. Donar d'alta un usuari%n"
                                + "6. Tanca la sessió%n");
                
                int selAdm = input.nextInt();
                switch(selAdm){
                    case 1:
                        for(int b = 0; b < user.length; b++){
                            if(!"".equals(user[b])){
                            System.out.printf("%nUsuari: %s. Nom i cognom: %s %s. Contrasenya: %s%n", 
                                                user[b], nom[b], cognom[b], pass[b]);
                            }
                        }
                        break;
                    case 2:
                        int usuarisReg = user.length; 
                        System.out.printf("Nombre d'usuaris registrats: %d", usuarisReg);
                        break;
                    case 3:
                        System.out.println("Introdueixi el usuari per comprobar la data del seu ultim inici de sesió:");
                        String userRqLogin = input.next();
                        for(int r = 0; r < user.length; r++){
                            if(userRqLogin.equals(user[r])){
                                System.out.println(formatter.format(r));
                            }
                        }
                        break;
                    case 4:
                        System.out.println("Introdueixi el usuari que vol eliminar");
                        String userRqDel = input.next();
                        for(int o = 0; o < user.length; o++){
                            if(userRqDel.equals(user[o])){
                                user[o] = "";
                                pass[o] = "";
                                nom[o] = "";
                                cognom[o] = "";
                                lastLogin[o] = null;
                            }
                        }
                        break;
                    case 5:
                        String tmppass = "";
                        
                        // DEMANA NOM I COGNOM
                        System.out.printf("%nBenvingut al sistema de registre.%n"
                                        + "Introdueix el teu Nom: ");
                        String nomStr = input.next();
                        System.out.printf("Introdueix el teu primer cognom: ");
                        String cognomStr= input.next();
                        // --------------------
                        
                        // GENERACIÓ D'USER
                        user = Arrays.copyOf(user, user.length + 1);
                        String miniUser = (nomStr.charAt(0) + cognomStr).toLowerCase();
                        for(int k = 0; k < user.length; k++){
                            if(miniUser.equals(user[k])){
                                miniUser = miniUser + supUser;
                                supUser++;
                                
                            }
                            
                        }
                        user[user.length - 1] = miniUser;
                        // ---------------
                        
                        lastLogin = Arrays.copyOf(lastLogin, lastLogin.length + 1);
                        
                        // DEFINICIÓ DE NOM I COGNOM
                        nom = Arrays.copyOf(nom, nom.length + 1);
                        nom[nom.length - 1] = nomStr;
                        
                        cognom = Arrays.copyOf(cognom, cognom.length + 1);
                        cognom[cognom.length - 1] = cognomStr;
                        // -------------------------
                        
                        // GENERACIÓ DE CONTRASENYA
                        pass = Arrays.copyOf(pass, pass.length + 1);
                        for(int e = 0; e < longPasswd; e++){
                            int selec = (int) (Math.random() * ((2 - 1) + 1)) + 1;
                            switch(selec){
                                case 1:
                                    int letraRand = (int) (Math.random() * ((122 - 97) + 1)) + 97;
                                    tmppass = tmppass + (char) letraRand;
                                    break;
                                case 2:
                                    int numRand = (int) (Math.random() * ((1 - 9) + 1)) + 9;
                                    tmppass = tmppass + numRand;
                                break;
                            }
                        }
                        pass[pass.length - 1] = tmppass;
                        // -----------------------
                        break;
                    case 6:
                        adminMenu = false;
                        break;
                }
            }
            // --------------
            
            // AUTENTIFICACIÓ DEL USUARI
            while(userAuth){
                System.out.println("Introdueix el teu usuari: ");
                String userIn = input.next();
                System.out.printf("%nIntrodueix la contransenya per l'usuari %s: ", userIn);
                String passIn = input.next();
                
                for(int n = 0; n < user.length; n++){
                    if(userIn.equals(user[n])){
                        if(passIn.equals(pass[n])){
                            System.out.printf("Contrasenya correcta. Benvingut %s %n", user[n]);
                            lastLogin[n] = new Date(System.currentTimeMillis());
                            userMenu = true;
                            userAuth = false;
                            usrID = n;
                        } else {
                            System.out.println("Contrasenya incorrecta.");
                        }
                    }
                }             
            }
            // ------------------------
            
            // MENÚ DEL USUARI
            while(userMenu){
                if(usrID == 0){
                    System.out.println("ERROR: Has entrat en el menú de usuari sense haberte autenticat.");
                    System.exit(1);
                }
                System.out.printf("Benvingut al menú d'usuari %s!"
                                + "%n1. Editar el meu nom i cognom"
                                + "%n2. Generar de nou la contrasenya"
                                + "%n3. Eliminar el meu usuari"
                                + "%n4. Tancar la sessió%n", 
                                user[usrID]);
                int selecc = input.nextInt();
                switch(selecc){
                    case 1:
                        boolean preguntaNom = true;
                        while(preguntaNom){
                            System.out.printf("El teu nom i cognoms actuals són %s %s%n"
                                            + "Segur que vols editarlos? (si/no) %n", nom[usrID], cognom[usrID]);
                            String resposta = input.next();
                            if(resposta.equals("si")){
                                System.out.println("Indrodueix el teu nom:");
                                nom[usrID] = input.next();
                                System.out.println("Indrodueix el teu cognom:");
                                cognom[usrID] = input.next();
                                System.out.printf("Canvi de nom satisfactori. Nom i cognom%n "
                                                 + "establert en %s %s. Tornant al menú del usuari", nom[usrID], cognom[usrID]);
                            } else if (resposta.equals("no")) {
                                System.out.println("Sembla que algú s'ha fet caqueta. Hehe%n"
                                                 + "Tornant al menú del usuari");
                                preguntaNom = false;
                            } else {
                                System.out.printf("T'has equivocat. No es tan complicat, de debó.%n"
                                                 + "Respon 'si' o 'no'.%n");
                            }
                        }
                        break;
                    case 2:
                        // COPIA DE L'ANTERIOR GENERADOR DE CONTRSENYES PERÓ APUNTANT
                        // AL USUARI ACTUAL (C.D.E.L.G.C.P.A.U.A.)
                        String tmppass = "";
                        for(int e = 0; e < longPasswd; e++){
                            int selec = (int) (Math.random() * ((2 - 1) + 1)) + 1;
                            switch(selec){
                                case 1:
                                    int letraRand = (int) (Math.random() * ((122 - 97) + 1)) + 97;
                                    tmppass = tmppass + (char) letraRand;
                                    break;
                                case 2:
                                    int numRand = (int) (Math.random() * ((1 - 9) + 1)) + 9;
                                    tmppass = tmppass + numRand;
                                break;
                            }
                        }
                        pass[usrID] = tmppass;
                        System.out.printf("La teva nova contrasenya es: %s", tmppass);
                        break;
                    case 3:
                        System.out.println("Has seleccionat l'opció per donar-te de baixa. Estas segur/a? (si/no)");
                        String resposta = input.next();
                            if(resposta.equals("si")){
                                System.out.printf("D'acord, adéu. De totes maneres demà t'anàvem a fotre fora%n"
                                                + "A continuació el teu usuari s'esborrarà i tornaràs al menú principal.%n"
                                                + "Introdueix la teva contrasenya:%n");
                                String delUserPassIn = input.next();
                                if(delUserPassIn.equals(pass[usrID])){
                                    user[usrID] = "";
                                    pass[usrID] = "";
                                    nom[usrID] = "";
                                    cognom[usrID] = "";
                                    lastLogin[usrID] = null;
                                    userMenu = false;
                                }
                            } else if (resposta.equals("no")) {
                               System.out.println("No, si ja m'esperava que et tiraries enrere.");
                            }
                        break;
                    case 4:
                        userMenu = false;
                        break;
                }
            }
            
            
        }
    }
    
}