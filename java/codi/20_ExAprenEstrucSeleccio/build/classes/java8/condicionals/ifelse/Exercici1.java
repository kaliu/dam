package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 1
 * 
 * Programa que demani un caràcter, a continuació un altre i 
 * comprovi si són iguals.   
 */
public class Exercici1 {
    public static void main(String[] args){
        Scanner lector=new Scanner(System.in);
        char c1,c2;
        
        System.out.println("Introduir dos caràcters:");
        c1=lector.next().toLowerCase().charAt(0);
        c2=lector.next().toLowerCase().charAt(0);
        
        if(c1==c2){
            System.out.println("Els dos caràcters són iguals.");
        }else{
             System.out.println("Els dos caràcters són diferents.");
        }
    }
 
}