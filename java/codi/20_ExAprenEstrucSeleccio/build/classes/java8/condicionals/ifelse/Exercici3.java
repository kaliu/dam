package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 3
 *
 * Programa que demani un caràcter, a continuació un altre i comprovi si els dos
 * són lletres minúscules
 */
public class Exercici3 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        char c1, c2;

        System.out.println("Introduir dos caràcters:");
        c1 = lector.next().charAt(0);
        c2 = lector.next().charAt(0);

        if ((c1 >= 'a' && c1 <= 'z') && (c2 >= 'a' && c2 <= 'z')) {
            System.out.println("Els dos caràcters són lletres minúscules.");
        } else {
            System.out.println("Els dos caràcters no són lletres minúscules.");
        }
    }

}
