package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 4
 * 
 * Programa que demani un caràcter i comprovi si és un dígit 
 * numèric (xifra entre 0 i 9).          
 */
public class Exercici4 {
    
     public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        char c1;

        System.out.println("Introduir un caràcter:");
        c1 = lector.next().charAt(0);

        if ((c1 >=48 && c1 <= '9') ) {
            System.out.println("El caràcter és un dígit.");
        } else {
            System.out.println("El caràcter no és un dígit.");
        }
    }

 
}