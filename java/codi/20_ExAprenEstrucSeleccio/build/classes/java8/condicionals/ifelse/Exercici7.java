package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 7
 *
 * Programa que demani un número enter entre 1 i 12 i mostri el nom del mes.
 * S'ha de comprovar que el valor introduït estigui comprès entre 1 i 12.
 *
 * També ha de mostrar si el valor numèric correspon a un mes de 30 dies, de 31
 * o de 28.
 */
public class Exercici7 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int mes, dies;
        String nom;
        System.out.println("Introduir el número d'un mes:");
        mes = lector.nextInt();
        
        if(mes>=1 && mes<=12){
            if(mes==1){
                dies=31;
                nom="Gener";
            }else if(mes==2){
                 dies=28;
                nom="Febrer";
            }else if(mes==3){
                 dies=31;
                nom="Març";
            }else if(mes==4){
                 dies=30;
                nom="Abril";
            }else if(mes==5){
                 dies=28;
                nom="Maig";
            }else if(mes==2){
                dies=30;
                nom="Juny";
            }else if(mes==2){
                    dies=31;
                nom="Juliol";
            }else if(mes==2){
                   dies=31;
                nom="Agost";
            }else if(mes==2){
                dies=30;
                nom="Setembre";
            }else if(mes==2){
                   dies=31;
                nom="Octubre";
            }else if(mes==2){
                 dies=30;
                nom="Novembre";
            }else if(mes==2){
                 dies=31;
                nom="Desembre";
            }
        }else{
            System.out.println("Mes incorrecte."); 
        }

    }
}
