package java8.condicionals.switchcase;

import java.util.Scanner;

/**
 * EXERCICI 1
 *
 * Implementa un programa que demani una nota de 0 a 10 i que utilitzant la
 * sentència "switch" per analitzar la nota introduïda, mostri els missatges
 * "SUSPÉS" (menor que 5), APROVAT (5), BÉ (6), NOTABLE (7 ó 8), EXCELENT (9 ó
 * 10)
 */
public class Exercici1 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int nota;
        String notaQ;

        System.out.println("Introduir una nota entera entre 0 i 10:");
        nota = lector.nextInt();

        switch (nota) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                notaQ = "SUSPÉS";
                break;
            case 5:
                notaQ = "APROVAT";
                break;
            case 6:
                notaQ = "BÉ";
                break;
            case 7:
            case 8:
                notaQ = "NOTABLE";
                break;
            case 9:
            case 10:
                notaQ = "EXCELENT";
                break;
            default:
                notaQ= "Nota  numèrica incorrecta";
        }
        System.out.println("Nota qualitativa: " + notaQ);

    }
}
