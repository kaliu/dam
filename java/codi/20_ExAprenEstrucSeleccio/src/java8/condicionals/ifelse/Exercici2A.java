package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 2
 *
 * Programa que demani tres números enters i mostri el major dels tres.
 */
public class Exercici2A {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int a = 1, b = 2, c = 3, major;
        System.out.println("Introduir 3 nombres enters:");
        a = lector.nextInt();
        b = lector.nextInt();
        c = lector.nextInt();

        if (a == b && b == c) {
            System.out.println("Els tres nombres són iguals");
        } else {
            if (a >= b && a >= c) {
                major = a;
            } else if (b >= c) {
                major = b;
            } else {
                major = c;
            }
            System.out.println("El número més gran és: " + major);
        }
    }
}
