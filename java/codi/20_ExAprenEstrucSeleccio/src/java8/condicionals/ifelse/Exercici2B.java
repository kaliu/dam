package java8.condicionals.ifelse;

import java.util.Scanner;


public class Exercici2B {

       
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int a = 1, b = 2, c = 3, major;
        System.out.println("Introduir 3 nombres enters:");
        a = lector.nextInt();
        b = lector.nextInt();
        c = lector.nextInt();
        
        if (a >= b) {
            if (a >= c) {
                major = a;
            } else {
                major = c;
            }
        } else {
            if (b >= c) {
                major = b;
            } else {
                major = c;
            }
        }
        System.out.println("El número més gran és: " + major);

    }
}

