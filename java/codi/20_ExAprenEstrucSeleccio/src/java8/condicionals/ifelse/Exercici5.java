package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 5
 * 
 * Programa que demani dos números enters i mostri el resultat 
 * de la divisió del primer número pel segon. 
 * S'ha de comprovar abans que el divisor (el segon) no pot ser zero.            
 */
public class Exercici5 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int a,b;
        System.out.println("Introduir 2 nombres enters:");
        a = lector.nextInt();
        b = lector.nextInt();
        
        if(b!=0){
            System.out.println("Divisió entera= "+(a/b));
        }else{
            System.out.println("No es pot dividir per 0");
        }
        
    }
 
}