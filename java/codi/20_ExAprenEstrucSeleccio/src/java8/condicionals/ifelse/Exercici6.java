package java8.condicionals.ifelse;

import java.util.Scanner;

/**
 * EXERCICI 6
 *
 * Programa que demani tres números enters corresponents a hora, minuts i segons
 * respectivament. S'ha de comprovar que el valor de l'hora, minuts i segons
 * introduïts són correctes.
 */
public class Exercici6 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int hora, minut, segon;
        System.out.println("Introduir l'hora, minuts i segons d'un moment del dia: :");
        hora = lector.nextInt();
        minut = lector.nextInt();
        segon = lector.nextInt();

        if ((hora >= 0 && hora < 24) && (minut >= 0 && minut < 60)
                && (segon >= 0 && segon < 60)) {
            System.out.println("Hora correcta");
        } else {
            System.out.println("Hora incorrecta");
        }

    }
}
