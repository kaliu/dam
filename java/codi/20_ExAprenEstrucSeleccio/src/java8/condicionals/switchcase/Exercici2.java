package java8.condicionals.switchcase;

import java.util.Scanner;

/**
 * EXERCICI 3
 *
 * Programa que demani un número enter entre 1 i 12 i i que utilitzant la
 * sentència "switch" mostri el nom del mes. S'ha de comprovar que el valor
 * introduït estigui comprès entre 1 i 12.
 *
 * Utilitzant un altra sentència "switch" també ha d'indicar si el valor numèric
 * correspon a un mes de 30 dies, de 31 o de 28.
 */
public class Exercici2 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int mes, dies = -1;
        String nom = "";
        System.out.println("Introduir el número d'un mes:");
        mes = lector.nextInt();

        switch (mes) {
            case 1:
                dies = 31;
                nom = "Gener";
                break;
            case 2:
                dies = 28;
                nom = "Febrer";
                break;
            case 3:
                dies = 31;
                nom = "Març";
                break;
            case 4:
                dies = 30;
                nom = "Abril";
                break;
            case 5:
                dies = 28;
                nom = "Maig";
                break;
            case 6:
                dies = 30;
                nom = "Juny";
                break;
            case 7:
                dies = 31;
                nom = "Juliol";
                break;
            case 8:
                dies = 31;
                nom = "Agost";
                break;
            case 9:
                dies = 30;
                nom = "Setembre";
                break;
            case 10:
                dies = 31;
                nom = "Octubre";
                break;
            case 11:
                dies = 30;
                nom = "Novembre";
                break;
            case 12:
                dies = 31;
                nom = "Desembre";
                break;
            default:
                System.out.println("Mes incorrecte.");

        }
        if (mes >= 1 && mes >= 12) {
            System.out.println(nom + " amb " + dies);
        }

    }

}
