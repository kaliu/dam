package java8.condicionals.switchcase;

import java.util.Scanner;

/**
 * EXERCICI 3
 *
 * Implementa un programa que mostri un menu amb tres opcions diferents, i que
 * depenent de l'opció seleccionada mostri un missatge semblant a "opció 1",
 * "opció 2", etc.
 *
 * Si s'introdueix una opció no válida, s'ha de mostrar un missatge d'error del
 * tipus "opció incorrecta".
 *
 */
public class Exercici3 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        String opcio = "";
        String seleccio = "";
        
        System.out.println("Introduir una opció:"
                + "\n1. Opció 1"
                + "\n2. Opció 2"
                + "\n3. Opció 3"
                + "\n4. Sortir 4");        
        opcio = lector.next();
        
        switch (opcio) {
            case "1":
                seleccio = "Opció 1";
                break;
            case "2":
                seleccio = "Opció 2";
                break;
            case "3":
                seleccio = "Opció 3";
                break;
            case "4":
                System.exit(0);
                break;  
            default:
                seleccio="Opció incorrecta";
        }
        System.out.println(seleccio);


    }
}
