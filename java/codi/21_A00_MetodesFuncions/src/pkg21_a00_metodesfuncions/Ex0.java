package pkg21_a00_metodesfuncions;
import java.util.Scanner;

/* Ex0
Crear un mètode anomenat entradaEnterPositiu,  el qual demani introduir un 
enter positiu el valida i el  torna. */


public class Ex0 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix un nombre enter positiu");
        int num = input.nextInt();
        boolean enterPositiu = entradaEnterPositiu(num);
        if(enterPositiu){
            System.out.println("Correcte, el numero es positiu");
        } else {
            System.out.println("Error, el nombre no es positiu");
        }
        
    }
    
    public static boolean entradaEnterPositiu(int x){
        if( x > 0 ){
            return true;
        } else {
            return false;
        }
        
    }
    
}
