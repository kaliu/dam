package pkg21_a00_metodesfuncions;
import java.util.Scanner;

/* Ex1
Crear un mètode anomenat factorial, al qual se li passi per paràmetre un 
nombre enter positiu i torni el valor del factorial del nombre. */

public class Ex1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix un nombre enter positiu");
        int num = input.nextInt();
        
        System.out.println("El factorial de aquest nombre es: " + factorial(num));
    }
    
    public static int factorial(int x){
        int resultado = 1;
        for(int w = 1; w <= x; w++){
            resultado *= w;
        }
        return resultado;
    }
}
