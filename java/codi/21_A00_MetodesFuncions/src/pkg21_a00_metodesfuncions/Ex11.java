
package pkg21_a00_metodesfuncions;
import java.util.Scanner;
/*Crear un mètode que li arribi per paràmetre un nombre enter positiu i torni 
un array amb els seus divisors. */

public class Ex11 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();
        int [] divisors = retornaDivisors(num);
        for(int i = 0; i < divisors.length; i++){  
            if( divisors[i] != 0){
                System.out.printf("%d ", divisors[i]);
            }
        }
        System.out.println("");
    }
    
    public static int[] retornaDivisors(int numero){
        int numdiv = 0;
        
        for(int i = 1; i <= numero; i++){
            if(numero % i == 0){
                numdiv++;
            }
        }
        int[] divisors = new int[numdiv];
        
        int w = 0;
        for(int i = 1; i <= numero; i++){
            if(numero % i == 0){
                divisors[w] = i;
                w++;
            }
        }
        
        return divisors;
    }
}
