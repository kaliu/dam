package pkg21_a00_metodesfuncions;
import java.util.Scanner;
/* Crear un mètode que li arribi per paràmetre un array ple de 
nombres i un valor. El mètode ha de tornar la posició on es troba 
el valor dins de l'array o -1 si no és a l'array.
Crear un altre mètode que li arribi per paràmetre un array ple de nombres i 
un valor, el mètode ha de tornar el nombre de vegades que es troba repetit el
valor rebut dins de l'array. */


public class Ex12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] nums = {0, 2, 1312, 4, 3, 2, 3, 1, 2, 3, 1312};
        int valor = 3;
        
        System.out.printf("El valor %d es troba primer a la posició %d", valor, trobaElValor(nums, valor));
        System.out.printf(" i es repeteix %d vegades a l'array%n", repetitValor(nums, valor));
    }
    
    static int trobaElValor(int[] arrayNums, int valor) {
        int result = -1;
        for(int i = 0; i < arrayNums.length; i++){
            if(arrayNums[i] == valor){
                result = i;
                break;
            }
        }
        
        
        return result;
    }
    
    static int repetitValor(int[] arrayNums, int valor){
        int result = 0;
        for(int i = 0; i < arrayNums.length; i++){
            if(arrayNums[i] == valor){
                result++;
            }
        }
        
        
        return result;
    }
}
