
package pkg21_a00_metodesfuncions;
/* Codificar un mètode que imprimeixi els valors d'un array de 
 * dues dimensions de nombres reals, per files. */


public class Ex14 {
    public static void main(String[] args) {
        int[][] array = {{5, 3, 2, 21, 23},{12, 32, 93, 13, 23, 103}};
        
        printMatriu(array);
    }
    
    static void printMatriu(int[][] matriu){
        for(int i = 0; i < matriu.length; i++){
            for(int j = 0; j < matriu[i].length; j++){
                System.out.printf("%d ", matriu[i][j]);
            }
            System.out.println();
        }
    }
}
