package pkg21_a00_metodesfuncions;
import java.util.Scanner;

/* Ex2
Crear un mètode anomenat areaTriangle, al qual se li passi per paràmetre 
la base i l'altura i torni l'àrea del triangle */


public class Ex2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix la base del triangle");
        double base = input.nextDouble();
        System.out.println("Introdueix la altura");
        double altura = input.nextDouble();       
        
        System.out.println("L'area del triangle es: " + areaTriangle(altura, base));
    }
    
    public static double areaTriangle(double altura, double base){
        double area = (base * altura) / 2;
        return area;
    }
}
