package pkg21_a00_metodesfuncions;
import java.util.Scanner;

/* Ex3
Crear un mètode anomenat esPrimer, al qual se li passi per paràmetre un nombre 
enter positiu i torni un boolean amb valor true si el nombre rebut és primer i 
false en cas contrari. */


public class Ex3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix un nombre enter");
        int num = input.nextInt();
     
        if(esPrimer(num)){
            System.out.println("El nombre introduit es primer");
        } else {
            System.out.println("El nombre introduit no es primer");
        }
        
    }
    
    public static boolean esPrimer(int num){
        int vegades = 0;
        for(int i = 1; i <= num; i++){
            if((num % i) == 0){
                vegades++;
            }
        }
        if(vegades <= 2){
            return true;
        } else {
            return false;
        }
    }
}
