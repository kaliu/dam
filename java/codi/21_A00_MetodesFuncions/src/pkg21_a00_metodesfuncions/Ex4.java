package pkg21_a00_metodesfuncions;

/* Ex4
Crear un mètode anomenat sumaArray al qual se li passi per paràmetre un array 
de nombres reals ple i torni el valor de la suma de totes les seves dades. */

public class Ex4 {

    public static void main(String[] args) {
        int[] nums = {1, 7, 4};
        
        System.out.println("El resultat de la suma es: " + sumaArray(nums));
        
    }
    
    public static int sumaArray(int[] arrayNums){
        int total = 0;
        for(int i = 0; i < arrayNums.length; i++){
            total += arrayNums[i];
        }
        return total;
    }
    
}
