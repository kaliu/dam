package pkg21_a00_metodesfuncions;

/* Ex5  
Crear un mètode anomenat imprimirArray, al qual se li passi per paràmetre un 
array d'enters i l'imprimeixi per pantalla. */


public class Ex5 {

    public static void main(String[] args) {
        int[] num = {2, 1, 8};
        
        printArray(num);
        
    }
    
    public static void printArray(int[] arrayNums){
        for(int i = 0; i < arrayNums.length; i++){
            System.out.printf("%d ",arrayNums[i]);
        }
        System.out.println();
    }
    
}
