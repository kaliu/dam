package pkg21_a00_metodesfuncions;

/* Ex6
Crear un mètode anomenat ordenarArray, al qual se li passi per paràmetre un array 
d'enters i l'ordeni seguint l'algorisme de la bombolla. */


public class Ex6 {

    public static void main(String[] args) {
        int[] num = {3, 53, 52, 1};
        
        imprimirArray(num);
        System.out.println(metodeBombolla(num));
        imprimirArray(num);
        
    }
    
    public static void imprimirArray(int[] array){
        for(int i=0; i < array.length; i++){  
            System.out.printf("%d ", array[i]);  
        }
        System.out.println("");
    }
    
    public static int metodeBombolla(int[] numArray) {  
        int maxArr = numArray.length;  
        int numCanvis = 0;
        int temp = 0;
        for(int i = 0; i < maxArr; i++){ // Per cada nombre en la array
            for(int j = 1; j < (maxArr-i); j++){  
                if(numArray[j-1] > numArray[j]){  // Si el nombre anterior es major que el seguent
                    temp = numArray[j-1];  
                    numArray[j-1] = numArray[j];  
                    numArray[j] = temp; // Els valors intercambien les seves posicions
                    numCanvis++;
                }             
            }  
        }
        return numCanvis;
    }
}
