
package pkg21_a00_metodesfuncions;
import java.util.Scanner;
/* Ex7
 Crear un mètode que li arribi per paràmetre una matriu d'enters 
 de dues dimensions i torni un array d'enters amb les dades de la 
 matriu però afegides a l'array per files. */


public class Ex7 {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] num = {{1,2,3},
                       {4,5,6}};
        
        System.out.println(printaArray(num));
        
    }
    
    public static int printaArray(int[][] array2D){
        
        
        return array2D[1][2];
    }
}
