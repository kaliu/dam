package pkg21_a00_metodesfuncions;

public class Ex8 {
    public static void main(String[] args) {
        int[] num = {3, 53, 52, 1};
        System.out.printf("El número mes gran de la Array és %d", mesGran(num));
    }
    
    public static int mesGran(int[] numArray){
        int maxArr = numArray.length;
        int temp = 0;
        for(int i = 0; i < maxArr; i++){ // Per cada nombre en la array
            for(int j = 1; j < (maxArr-i); j++){  
                if(numArray[j-1] > numArray[j]){  // Si el nombre anterior es major que el seguent
                    temp = numArray[j-1];  
                    numArray[j-1] = numArray[j];  
                    numArray[j] = temp; // Els valors intercambien les seves posicions
                }             
            }  
        }
        
        return numArray[maxArr - 1];
    }
}