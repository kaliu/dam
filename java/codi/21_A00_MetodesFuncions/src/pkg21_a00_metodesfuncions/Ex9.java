
package pkg21_a00_metodesfuncions;

/*  Crear un altre mètode que li arribi per paràmetre un array de nombres reals, 
que pot estar no ple, junt amb  la seva ocupació i torni el valor de la mitjana 
aritmètica dels seus valors.*/
public class Ex9 {
    public static void main(String[] args) {
        int[] nums = {19, 29, 12, 10, 3, 2};
        System.out.println(mitjaAritmetica(nums, 6));
        
    }

    public static int mitjaAritmetica(int[] arrayNums, int ocupacio){
        int suma = 0;
        for(int i = 0; i < ocupacio; i++){
            suma = suma + arrayNums[i];
        }
        
        System.out.println(suma);
        int mitjana = suma / ocupacio;
        return mitjana;
    }
    
}
