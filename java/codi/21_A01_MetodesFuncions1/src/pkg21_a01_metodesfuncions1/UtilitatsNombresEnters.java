package pkg21_a01_metodesfuncions1;

/**
 *
 * @author Roger
 */
public class UtilitatsNombresEnters {
    
    /**
     * Comprova si num es positiu i enter o no
     * @param num Número a comprobar
     * @return true si es enter i positiu, false si no
     */
    public boolean entradaEnterPositiu(double num)
    {
        return (int)num == num && num > 0;
    }
}
