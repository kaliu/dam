package java8.matrius;


/**
 * EXERCICI 1:
 * -----------
 * 
 * Carregar dues matrius de mida 3x3 amb valors aleatoris
 * entre 0 i 9 (tots dos inclosos):
 * Mostrar totes dues, sumar-les i mostrar la seva suma.
 * 
 * La suma de dues matrius s'obté 
 * - Creant una nova matriu de les mateixes dimensions. 
 * - Cada element de la nova matriu és la suma dels elements que
 * es troben a la mateixa posició (fila,columna)
 * a les dues matriues que es sumem.
 * 
 * EXEMPLE
Primera matriu:
  6  4  8
  8  7  2
  5  5  5
Segunda matriu:
  5  2  3
  3  5  6
  0  5  6
Suma:
 11  6 11
 11 12  8
  5 10 11
 * 
 *  
 *  
 */
public class Exercici1 {

	
}
