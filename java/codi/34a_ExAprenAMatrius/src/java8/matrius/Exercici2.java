package java8.matrius;


/**
 * EXERCICI 2:
 * -----------
 * La diagonal d'una matriu està formada pels elements que tenen el
 * valor de fila igual al valor de columna.
 * 
 * Per exemple, per la matriu:
 * 
 *  1  2  3  4
 *  5  6  7  8
 *  9 10 11 12
 * 13 14 15 16
 * 
 * Els elements de la diagonal son:
 *  1         
 *     6      
 *       11  
 *          16
 * 
 * El triangle inferior d'una matriu està format pels elements
 * que estan sota els elements de la diagonal.
 * 
 * Per exemple, per la matriu:
 * 
 *  1  2  3  4
 *  5  6  7  8
 *  9 10 11 12
 * 13 14 15 16
 * 
 * el triangle inferior està format pels elements:
 *            
 *  5         
 *  9 10    
 * 13 14 15  
 * 
 * ENUNCIAT
 * --------
 * Crear un programa que a partir d'una matriu de mida 4x4  
 * amb valors enters aleatoris:
 * - mostri la matriu
 * - mostri el triangle inferior d'una matriu
 * - mostri el valor de la suma dels elements de la matriu que 
 * pertanyen al triangle inferior de la matriu
 * 
 * EXEMPLE
Mostrar matriu:
  3  2  3  9
  1  1  5  8
  8  9  8  5
  7  4  2  6
Triangle inferior de la matriu
  1
  8  9
  7  4  2
Suma del triangle inferior de la matriu:31
 * 
 */
public class Exercici2 {

	
}
