package java8.matrius;



/**
 * EXERCICI 3:
 * -----------
 * Carregar una matriu de mida 3x3  amb valors demanats a l'usuari.
 * 
 * El vostre programa ha de dir si és simètrica o no.
 * 
 * Una matriu és simètrica quan per qualsevol valor i,j
 * s'acompleix matriu[i][j] = matriu[j][i]
 * 
 * EXEMPLE
matriz[0][0]:
1
matriz[0][1]:
2
matriz[0][2]:
3
matriz[1][0]:
2
matriz[1][1]:
1
matriz[1][2]:
2
matriz[2][0]:
3
matriz[2][1]:
2
matriz[2][2]:
1
Mostrar matriu:
  1  2  3
  2  1  2
  3  2  1
Es simètrica
* 
 */
public class Exercici3 {

    

}
