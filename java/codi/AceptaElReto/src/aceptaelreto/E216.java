package aceptaelreto;
import java.util.Scanner;

public class E216 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        int vegades = input.nextInt();
    
        for(int i = 0; i < vegades; i++){
            int gotes = input.nextInt();
            
            int hores = gotes / 3600;
            int minuts = (gotes % 3600) / 60;
            int segons = gotes % 60;
            
            System.out.printf("%02d:%02d:%02d", hores, minuts, segons);
        }
    }
}
