
package aceptaelreto;
import java.util.Scanner;

public class E300 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        int veces = input.nextInt();
        
        for(int k = 0; k < veces; k++){
            String palabra = input.next();
            if(palabra.contains("a") && palabra.contains("e") && palabra.contains("i") && palabra.contains("o") && palabra.contains("u")){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }

    }
}
