
package chickens01;

public class Chickens01 {
    public static void main(String[] args) {
        int chickenCount = 7;
        int eggsPerChicken = 2;
        
        // Dia Inicial
        // Printa les variables chickenCount i eggsPerChiken
        System.out.println("Si Paco comença amb "+chickenCount+
        " gallines i cada gallina posa cada dia "+eggsPerChicken+" ous.");
        
        // Dilluns
        // Els ous son igual a les gallines per el nombre de ous que posa cada 
        // gallina
        int eggs = chickenCount * eggsPerChicken;
        System.out.println("Dilluns, Paco té " + chickenCount + " gallines");
        System.out.println("I té " + eggs + " ous.");
        
        // Dimarts
        // Els ous son igual als ous del dia anterior mes els ous que posen les
        // gallines aquest dia
        chickenCount = chickenCount + 1;
        eggs = eggs + (chickenCount * eggsPerChicken);
        
        System.out.println("Dimarts, Paco té " + chickenCount + " gallines");
        System.out.println("I té " + eggs + " ous.");
        
        
        // Dimecres
        chickenCount = chickenCount / 2;
        eggs = eggs + (chickenCount * eggsPerChicken);
        
        System.out.println("Dimecres, Paco té " + chickenCount + " gallines");
        System.out.println("I té " + eggs + " ous.");
    }   
}
