
package chickens02;

public class Chickens02 {
    public static void main(String[] args) {
        double mondayEggs = 100d;
        double tuesdayEggs = 121d;
        double wendsdayEggs = 117d;
        
        double dailyAverage = (mondayEggs + tuesdayEggs + wendsdayEggs) / 3;
        int monthlyAverage = (int) (dailyAverage * 30);
        double monthlyProfit = monthlyAverage * 0.18;
                
        
        System.out.println("Daily Average:   " +dailyAverage);
        System.out.println("Monthly Average: " +monthlyAverage);
        System.out.println("Monthly Profit:  $" +monthlyProfit);
    }
    
}
