package estructuresrepeticio;

import java.util.Scanner;



public class While {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int i = 1;  //Declarem la variable de control i la inicialitzem
        long suma = 0;
        int num;

        System.out.println("Introduir fins a quin nombre es farà la suma:");
        num = lector.nextInt();

        //Inici del bloc while
        while (num >= i) { //S'avalua la condició
            suma = suma + i;
            
            System.out.print("+" + i);
            i++;  //S'actualitza la variable de control
        } //Fí del bloc while

        //Mostrem el resultat
        System.out.println("=" + suma);
    }

}
