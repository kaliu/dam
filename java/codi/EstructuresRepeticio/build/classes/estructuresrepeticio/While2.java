package estructuresrepeticio;

import java.util.Scanner;

public class While2 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int num;

        System.out.println("COMPROVAR SI UN NOMBRE ENTER ÉS MÚLTIPLE DE 3:"
                + "\n Introduir els nombres o 0 per finalitzar.");

        while ((num = lector.nextInt()) != 0) {
            if (num % 3 == 0) {
                System.out.println("És múltiple de 3.");
            } else {
                System.out.println("No és múltiple de 3.");
            }

        }

    }

}
