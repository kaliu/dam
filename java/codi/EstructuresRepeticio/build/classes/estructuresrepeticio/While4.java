package estructuresrepeticio;

import java.util.Scanner;
/**
 * L'aplicació ha de permetre introduir nombres enters i comprovar si són 
 * múltiples de 3 o no, mostrant un missage indicant-ho.
 * Per finalitzar-la s'ha d'introduir el valor 0.
 * 
 */
public class While4 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        boolean noZero = true;
        int num;

        System.out.println("COMPROVAR SI UN NOMBRE ENTER ÉS MÚLTIPLE DE 3:"
                + "\n Introduir els nombres o 0 per finalitzar.");

        while (noZero) {
            num = lector.nextInt();
            if (num != 0) {
                if (num % 3 == 0) {
                    System.out.println("És múltiple de 3.");
                } else {
                    System.out.println("No és múltiple de 3.");
                }
            } else {
                noZero = false;
            }
       }
    }
}
