package estructuresrepeticio;

import java.util.Scanner;

/**
 * El programa ha de mostrar un menú per seleccionar l'idioma o sortir de 
 * l'aplicació. 
 * Si l'opció introduïda correspon a un dels idiomes, es mostrarà
 * la salutació corresponent a l'idioma seleccionat.
 * Si l'opció introduïda no existeix, es mostrarà un missatge indicant que no
 * és vàlida i es tornarà a mostrar el menú.
 * Si l'opció és sortir, no es mostrarà res i finalitzarà l'aplicació.
 * 
 * 
 * @author j
 */

public class While3 {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        String opcio;
        boolean continuar = true;
        String salutacio;

        while (continuar) {
            System.out.println("Seleccionar Idioma:"
                    + "\n1. Català"
                    + "\n2. Castellà"
                    + "\n3. Anglès"
                    + "\n4. Sortir ");
            opcio = lector.next();
            
            salutacio = "";
            switch (opcio) {
                case "1":
                    salutacio = "Bon dia!";
                    break;
                case "2":
                    salutacio = "Buenos días!";
                    break;
                case "3":
                    salutacio = "Good morning!";
                    break;
                case "4":
                    continuar=false;
                     break;
                default:
                    salutacio= "Opció no vàlida.";
            }
            System.out.println(salutacio);
        }

    }

}
