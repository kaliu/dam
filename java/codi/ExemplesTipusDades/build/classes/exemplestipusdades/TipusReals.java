package exemplestipusdades;

public class TipusReals {

    public static void main(String[] args) {

        //TIPUS REALS
        System.out.println(2.38F+4); //float + int --> float
        System.out.println(0.765f*8.4); //float*double --> double
        System.out.println(-4.5674); // double
        System.out.println(3.48625E3); //double
        System.out.println(3.48625E3F); // float

    }

}
