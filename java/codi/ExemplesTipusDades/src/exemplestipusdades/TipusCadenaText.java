package exemplestipusdades;

public class TipusCadenaText {

    public static void main(String[] args) {
        System.out.println("Bon dia");  // tipus String
        System.out.println("Primer\tSegon\tTercer"); // \t caràcter especial: tabulador
        System.out.println("Línia1\nLínia2");  // \n caràcter especial: crea nova línia i fa el salt de línia
        System.out.println("cadena1 " + "cadena2 " + "cadena3"); // '+' concatena cadenes.

    }

}
