package exemplestipusdades;

public class TipusEnters {

    public static void main(String[] args) {
    
        //TIPUS ENTER
        System.out.println(17); //tipus int
        System.out.println(999999999); //tipus int 
        System.out.println(-45674); //tipus int 
        System.out.println(76878674L); //tipus long
        System.out.println(-76878674L); //tipus long
        System.out.println(3333l); //tipus long

//Representació en Decimal, Octal i Hexadecimal
        System.out.println(064); //en octal, tipus int
        System.out.println(0x34); //en hexadecimal, tipus int
        System.out.println(52); //en decimal, tipus int
        System.out.println(0x34L); //en hexadecimal, tipus long
        System.out.println(064l); //en octal, tipus long
    }

}
