package exemples_for;

public class For1 {
/*
* Sumar els 10 primers nombres.
*/
    public static void main(String[] args) {
        int suma = 0;
        
        //Inici del bloc for
        for (int i = 1; i <= 10; i++) {
            suma += i;
            System.out.print("+" + i);
        }
     
        
        //Mostrem el resultat
        System.out.println("=" + suma);
    }

}
            