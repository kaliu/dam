package exemples_for;

public class For4 {

    public static void main(String[] args) {
        /*
* Sumar els nombres parells entre 1 i 10.
         */

        int suma = 0;
        int compt = 0;

        for (int i = 1; i <= 10; i += 2) {
            suma += i;
            System.out.print("+" + i);
            compt++;
        }
        
        //Mostrem el resultat
        System.out.println("=" + suma);

        System.out.println("compt=" + compt);
    }
}



