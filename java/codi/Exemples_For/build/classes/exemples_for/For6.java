package exemples_for;
/*
Es construeix una cadena nova amb només els caràcters 
que no són dígits.
*/
public class For6 {

    public static void main(String[] args) {
        String cad = "Avui és 3 de desembre del 2018";
        String nova="";
        char c;
        System.out.println("Longitud de la cadena=" + cad.length());

        for (int i = 0; i < cad.length(); i++) {
            c = cad.charAt(i);
            if (c < '0' || c >'9') {
                nova+=c;
            }
        }
        System.out.println("nova= "+nova);

    }

}
