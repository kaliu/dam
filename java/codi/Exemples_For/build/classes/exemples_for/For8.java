package exemples_for;
/*
    Obtenir la cadena inversa
*/

public class For8 {

    public static void main(String[] args) {
        String cad = "Avui és 3 de desembre del 2018";
        String nova="";
    
        System.out.println("Longitud de la cadena=" + cad.length());

        for (int i = 0; i < cad.length(); i++) { 
                nova=cad.charAt(i)+nova;           
        }
        System.out.println("nova= "+nova);
    }
}