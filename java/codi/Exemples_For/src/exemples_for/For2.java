package exemples_for;
/*
* Sumar els 10 primers nombres.
*/
public class For2 {

    public static void main(String[] args) {
        int suma = 0;

        //Inici del bloc for
        for (int i = 10; i>0; i--) {
            suma += i;
            System.out.print("+" + i);
        }

        //Mostrem el resultat
        System.out.println("=" + suma);
    }

}

