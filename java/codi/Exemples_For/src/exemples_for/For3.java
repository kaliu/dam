package exemples_for;

public class For3 {

    /*
* Sumar els nombres parells entre 1 i 10.
     */
    public static void main(String[] args) {
        int suma = 0;
        int compt = 0;

        for (int i = 2; i <= 10; i += 2) {
            suma += i;
            System.out.print("+" + i);
            compt++;
        }
        //Mostrem el resultat
        System.out.println("=" + suma);

        System.out.println("compt=" + compt);
    }
}
