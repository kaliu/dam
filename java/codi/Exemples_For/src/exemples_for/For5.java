package exemples_for;
/*
Imprimir només els caràcters que són dígits numèrics.
*/
public class For5 {

    public static void main(String[] args) {
        String cad = "Avui és ";
        char c;
        System.out.println("Longitud de la cadena=" + cad.length());

        for (int i = 0; i < cad.length(); i++) {
            c = cad.charAt(i);
            if (c >= '0' && c <= '9') {
                System.out.print(c);
            }
        }
        System.out.println("");

    }

}
