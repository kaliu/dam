package exemples_for;

/*
    Obtenir la cadena inversa
*/
public class For7 {

    public static void main(String[] args) {
        String cad = "Avui és 3 de desembre del 2018";
        String nova = "";

        System.out.println("Longitud de la cadena=" + cad.length());

        for (int i = cad.length() - 1; i >= 0; i--) {
            nova += cad.charAt(i);
        }

        System.out.println("nova= " + nova);
    }
}
