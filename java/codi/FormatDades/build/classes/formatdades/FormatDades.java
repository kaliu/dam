package formatdades;

public class FormatDades {

    public static void main(String[] args) {
        int num = 325;
        long dada = 12234435;
        float varA = 1.2348324f;
        double varB = 2369.45;
        double varC = 34;
        String cad = "Avui és un bon dia";
        String cad2;

        System.out.println("FORMAT DADES ENTERES");
        System.out.format("num=%d%n", num);
        System.out.format("dada=%d%n", dada);

        //Fixem la quantitat mínima d'espais completant amb espais en blanc 
        System.out.format("num=%10d%n", num);
        System.out.format("dada=%10d%n", dada);
        System.out.printf("dada=%10d%n", dada);

        //Fixem la quantitat mínima d'espais completant amb 0 
        System.out.format("num=%08d%n", num);
        System.out.format("dada=%08d%n", dada);

        //Donem format a més d'una dada en la mateixa cadena
        System.out.format("num=%08d  dada=%7d%n", num, dada);

        System.out.println("\nFORMAT DADES REALS");
        //Dades reals sense format
        System.out.format("varA=%f varB=%f varC=%f%n", varA, varB, varC);

        //Fixem la quantitat màxima de xifres decimals
        System.out.format("varA=%.2f varB=%.2f varC=%.3f%n", varA, varB, varC);

        //Fixem la quantitat mínima d'espais que ocuparà el valor
        System.out.format("varA=%6.2f varB=%6.2f varC=%6.3f%n", varA, varB, varC);
        System.out.printf("varA=%6.2f varB=%6.2f varC=%6.3f%n", varA, varB, varC);

        System.out.println("FORMAT CADENES DE TEXT");
        System.out.format("cad=%s%n", cad);
        System.out.format("cad=%35s%n", cad);
        
//Si la quantitat d'espais mínima és negativa es generen per l'esquerra.
        System.out.format("cad=%-35s%n", cad);
        System.out.printf("cad=%35s%n", cad);

        System.out.println("MÈTODE format de la classe String");
        cad2 = String.format("dada=%5d varB=%6.2f%n cad=%35s", dada, varB, cad);
        System.out.println(cad2);
    }

}
