package migestormatrius;
import java.util.Random;

public class MiGestorMatrius {
    
    public static void main(String[] args) {

        double[][] matriu = new double[5][5];  
        System.out.printf("%nMatriu generada:%n");
        generarMatriu(matriu);
        mostrarMatriu(matriu);
        System.out.printf("%nSuma total de la matriu:%n");
        System.out.println(sumaMatriu(matriu));
        
        int numSostre = 20;
        
        System.out.printf("%nÉs algun número major que %d? %n", numSostre);
        System.out.println(sostreMatriu(matriu, numSostre));
        
        System.out.printf("%nNúmeros en diagonal a la matriu: %n");
        double[] diagonal = diagonalMatriu(matriu);
        for(int i = 0; i < diagonal.length; i++){
            System.out.printf("%.1f ", diagonal[i]);
        }
            System.out.println("");
    }
    
    static void generarMatriu(double[][] matriu){
        Random rd = new Random();
        for(int i = 0; i < matriu.length; i++){
            for(int j = 0; j < matriu[i].length; j++){
               matriu[i][j] = rd.nextDouble() + rd.nextInt(10);
            }
        }
    }
    
    static void mostrarMatriu(double[][] matriu){
        for(int i = 0; i < matriu.length; i++){
            for(int j = 0; j < matriu[i].length; j++){
                System.out.printf("%.1f ", matriu[i][j]);
            }
            System.out.println("");
        }
    }
    
    static double sumaMatriu(double[][] matriu){
        double result = 0;
        for(int i = 0; i < matriu.length; i++){
            for(int j = 0; j < matriu[i].length; j++){
                result += matriu[i][j];
            }
        }
        return result;
    }
    
    static boolean sostreMatriu(double[][] matriu, int num){
        boolean esMajor = true;
        
        for(int i = 0; i < matriu.length; i++){
            for(int j = 0; j < matriu[i].length; j++){
                if(num < matriu[i][j]){
                    esMajor = false;
                }
            }
        }
        
        return esMajor;
    }
    
    static double[] diagonalMatriu(double[][] matriu){
        double[] result = new double[matriu.length];
        for( int k = 0 ; k < matriu.length ; k++ ) {
                result[k] = matriu[k][k];
        }
        return result;
    }
    
}
