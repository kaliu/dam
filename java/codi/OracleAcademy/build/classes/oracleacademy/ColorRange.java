
package oracleacademy;
import java.util.Scanner;

public class ColorRange {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Por favor, introduzca la longitud de onda");
        double longOnda = input.nextDouble();
        String result = "ninguno, no se encuentra en el espectro visible.";
        
        if (longOnda >= 360 && longOnda < 450) {
            result = "Violeta.";
        }
        if (longOnda >= 450 && longOnda < 495) {
            result = "Azul.";
        }
        if (longOnda >= 495 && longOnda < 570) {
            result = "Verde.";
        }
        if (longOnda >= 570 && longOnda < 590) {
            result = "Amarillo.";
        }
        if (longOnda >= 590 && longOnda < 620) {
            result = "Naranja.";
        }
        if (longOnda < 750 && longOnda > 620) {
            result = "Rojo.";
        }
        
        System.out.printf("El color és: %s%n",result);
            
    }
    
}
