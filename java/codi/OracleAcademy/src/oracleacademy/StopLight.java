
package oracleacademy;
import java.util.Scanner;

public class StopLight {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.printf("Enter a color code:%n");
        int currentColor = input.nextInt();
        String nextColor = "Invalid color.";
        
        if (currentColor == 1)  {
            nextColor = "Next Traffic Light is green";
        }
        if (currentColor == 2) {
            nextColor = "Next Traffic Light is yellow";
        }
        if (currentColor == 3) {
            nextColor = "Next Traffic Light is red";                         
        }

        System.out.println(nextColor);
    }
}
