package p1_uf2_rogergras;

import java.util.Scanner;

/**
 *
 * @author Roger
 */
public class FuncionsApp {

    /**
     * Aquest mètode serveix per que es mostri la informació de tots els usuaris
     *
     * @param user Array de noms d'usuari
     * @param nom Array de noms
     * @param cognom Array de cognoms
     * @param pass Array de contrasenyes
     */
    public void mostrarUsuaris(String[] user, String[] nom, String[] cognom, String[] pass) {
        for (int b = 0; b < user.length; b++) {
            if (!"".equals(user[b])) {
                System.out.printf("%nUsuari: %s. %n"
                        + "Nom i cognom: %s %s. %n"
                        + "Contrasenya: %s. %n"
                        + "--------------------------- %n",
                        user[b], nom[b], cognom[b], pass[b]);
            }
        }
    }

    /**
     * Acoseguir la id del usuari que vols eliminar
     *
     * @param user Array de usuaris
     * @return ID del usuari corresponent
     */
    public int deleteUser(String[] user) {
        Scanner input = new Scanner(System.in);
        int userID = 0;
        String userRqDel = input.next();
        for (int o = 0; o < user.length; o++) {
            if (userRqDel.equals(user[o])) {
                userID = o;
            }
        }
        return userID;
    }

    /**
     * Genera un usuari amb login i contrasenya donats un nom i cognom
     *
     * @param nom El nom del l'usuari a crear
     * @param cognom El cognom de l'usuari a crear
     * @param supUser El número per on comença a contar
     * @param user El array d'usuaris
     * @return Resultat (user i password)
     */
    public String[] addUser(String nom, String cognom, int supUser, String[] user) {
        String resultat[] = new String[4];
        String miniUser = (nom.charAt(0) + cognom).toLowerCase();
        for (int k = 0; k < user.length; k++) {
            if (miniUser.equals(user[k])) {
                miniUser = miniUser + supUser;
                supUser++;
            }
        }
        resultat[0] = miniUser;
        resultat[1] = genPass(8);

        return resultat;
    }

    /**
     * Genera una contrasenya de mida x amb caràcters alfanumèrics
     *
     * @param longPass Lo llarga que es vol la contrasenya
     * @return Password
     */
    public String genPass(int longPass) {
        String tmppass = "";
        for (int e = 0; e < longPass; e++) {
            int selec = (int) (Math.random() * ((2 - 1) + 1)) + 1;
            switch (selec) {
                case 1:
                    int letraRand = (int) (Math.random() * ((122 - 97) + 1)) + 97;
                    tmppass = tmppass + (char) letraRand;
                    break;
                case 2:
                    int numRand = (int) (Math.random() * ((1 - 9) + 1)) + 9;
                    tmppass = tmppass + numRand;
                    break;
            }
        }
        return tmppass;
    }

    /** 
     * Donada la id del usuari, l'array de noms i de cognoms permet canviar el nom i cognom de
     * l'usuari
     * @param loginUsrID L'ID del usuari logejat
     * @param nom L'array de noms
     * @param cognom L'array de cognoms
     * @return Resultat (nom i cognoms nous)
     */
    public String[] editUser(int loginUsrID, String[] nom, String[] cognom) {
        Scanner input = new Scanner(System.in);
        String[] result = new String[2];
        System.out.printf("El teu nom i cognoms actuals són %s %s%n"
                + "Segur que vols editarlos? (si/no) %n", nom[loginUsrID], cognom[loginUsrID]);
        String resposta = input.next();
        if (resposta.equals("si")) {
            System.out.printf("%nBenvingut al sistema de registre.%n"
                    + "Introdueix el teu Nom: ");

            String nomStr = input.next();
            result[0] = nomStr;
            System.out.printf("Introdueix el teu primer cognom: ");
            String cognomStr = input.next();
            result[1] = cognomStr;

            System.out.printf("Canvi de nom satisfactori. Nom i cognom "
                    + "establert en %s %s.%n Tornant al menú del usuari", result[0], result[1]);
        } else if (resposta.equals("no")) {
            System.out.println("Sembla que algú s'ha fet caqueta. Hehe%n"
                    + "Tornant al menú del usuari");
        } else {
            System.out.printf("T'has equivocat. No es tan complicat, de debó.%n"
                    + "Respon 'si' o 'no'.%n");
        }
        return result;
    }
}
