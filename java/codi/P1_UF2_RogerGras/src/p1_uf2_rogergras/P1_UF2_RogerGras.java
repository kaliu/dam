package p1_uf2_rogergras;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author Roger
 */
public class P1_UF2_RogerGras {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        FuncionsApp usrFunc = new FuncionsApp();

        boolean userAuth = false, adminMenu = false, userMenu = false;
        int loginUsrID = 0;

        // Configuració
        int supUser = 1; // Número per el que comença a comptar al repetir usuari
        String admPass = "password"; // Contrasenya del administrador
        // -----------

        // ARRAYS
        String[] nom = {""};
        String[] cognom = {""};
        String[] user = {""};
        String[] pass = {""};
        Date[] lastLogin = {null};
        // ------

        while (true) {
            // MENU PRINCIPAL
            System.out.printf("      Accés al sistema%n"
                    + "----------------------------%n"
                    + "1. Entrar com administrador%n"
                    + "2. Entrar com usuari%n"
                    + "3. Sortir%n");
            int selPrin = input.nextInt();
            switch (selPrin) {
                case 1:
                    System.out.println("Introdueïx la contrasenya:");
                    String admPassIn = input.next();
                    if (admPass.equals(admPassIn)) {
                        System.out.println("Contrasenya correcta!");
                        adminMenu = true;
                    } else {
                        System.out.println("Contrasenya incorrecta. Tornant al menú");
                    }
                    break;
                case 2:
                    userAuth = true;
                    break;
                case 3:
                    System.exit(0);
                    break;
            }
            // --------------

            // MENU DEL ADMIN
            while (adminMenu) {
                System.out.printf("%nBenvingut administrador. Seleccioni una de les%n"
                        + "següents opcions: %n"
                        + "1. Mostrar tots els noms, cognoms i login dels usuaris%n"
                        + "2. Saber quants usuaris té el sistema%n"
                        + "3. Saber quan va ser l'ultim accés del usuari%n"
                        + "4. Donar de baixa un usuari%n"
                        + "5. Donar d'alta un usuari%n"
                        + "6. Tanca la sessió%n");

                int selAdm = input.nextInt();
                switch (selAdm) {
                    case 1:
                        usrFunc.mostrarUsuaris(user, nom, cognom, pass);
                        break;
                    case 2:
                        int contador = 0;
                        for (int i = 0; i < user.length; i++) {
                            if (user[i] != "") {
                                contador++;
                            }
                        }
                        System.out.printf("Nombre d'usuaris registrats: %d", contador);
                        break;
                    case 3:
                        System.out.println("Introdueixi el usuari per comprobar la data del seu ultim inici de sessió:");
                        String userRqLogin = input.next();

                        for (int r = 0; r < user.length; r++) {
                            if (userRqLogin.equals(user[r])) {
                                System.out.println(lastLogin[r]);
                            }
                        }
                        break;
                    case 4:
                        System.out.println("Introdueixi el usuari que vol eliminar");
                        int delUserID = usrFunc.deleteUser(user);
                        user[delUserID] = "";
                        pass[delUserID] = "";
                        nom[delUserID] = "";
                        cognom[delUserID] = "";
                        lastLogin[delUserID] = null;
                        break;
                    case 5:
                        System.out.printf("%nBenvingut al sistema de registre.%n"
                                + "Introdueix el teu Nom: ");
                        String nomStr = input.next();
                        System.out.printf("Introdueix el teu primer cognom: ");
                        String cognomStr = input.next();

                        String[] userPass = usrFunc.addUser(nomStr, cognomStr, supUser, user);

                        user = Arrays.copyOf(user, user.length + 1);
                        user[user.length - 1] = userPass[0];

                        nom = Arrays.copyOf(nom, nom.length + 1);
                        nom[nom.length - 1] = nomStr;

                        cognom = Arrays.copyOf(cognom, cognom.length + 1);
                        cognom[cognom.length - 1] = cognomStr;

                        pass = Arrays.copyOf(pass, pass.length + 1);
                        pass[pass.length - 1] = userPass[1];

                        lastLogin = Arrays.copyOf(lastLogin, lastLogin.length + 1);
                        break;
                    case 6:
                        adminMenu = false;
                        break;
                }
            }
            // --------------

            // AUTENTIFICACIÓ DEL USUARI
            while (userAuth) {
                System.out.println("Introdueix el teu usuari: ");
                String userIn = input.next();
                
                for (int n = 0; n < user.length; n++) {
                    if (userIn.equals(user[n])) {
                        System.out.printf("%nIntrodueix la contransenya per l'usuari %s: %n", userIn);
                        String passIn = input.next();
                        
                        if (passIn.equals(pass[n])) {
                            System.out.printf("Contrasenya correcta. Benvingut %s %n", user[n]);
                            lastLogin[n] = new Date(System.currentTimeMillis());
                            userMenu = true;
                            userAuth = false;
                            loginUsrID = n;
                        } else {
                            System.out.println("Contrasenya incorrecta o usuari inexistent.");
                        }
                    } 
                }
            }
            // ------------------------

            // MENÚ DEL USUARI
            while (userMenu) {
                if (loginUsrID == 0) {
                    System.out.println("ERROR: Has entrat en el menú de usuari sense haberte autenticat.");
                    System.exit(1);
                }
                System.out.printf("%nBenvingut al menú d'usuari %s!"
                        + "%n1. Editar el meu nom i cognom"
                        + "%n2. Generar de nou la contrasenya"
                        + "%n3. Eliminar el meu usuari"
                        + "%n4. Tancar la sessió%n",
                        user[loginUsrID]);
                int selecc = input.nextInt();
                switch (selecc) {
                    case 1:
                        String[] nomCognom = usrFunc.editUser(loginUsrID, nom, cognom);
                        if(nomCognom[0] != ""){
                            nom[loginUsrID] = nomCognom[0];
                            cognom[loginUsrID] = nomCognom[1];
                        }
                        break;
                    case 2:
                        pass[loginUsrID] = usrFunc.genPass(8);
                        System.out.printf("La teva nova contrasenya es: %s", pass[loginUsrID]);
                        break;
                    case 3:
                        System.out.println("Has seleccionat l'opció per donar-te de baixa. Estas segur/a? (si/no)");
                        String selec = input.next();
                        if (selec.equals("si")) {
                            System.out.printf("D'acord, adéu. De totes maneres demà t'anàvem a acomiadar%n"
                                    + "A continuació el teu usuari s'esborrarà i tornaràs al menú principal.%n"
                                    + "Introdueix la teva contrasenya:%n");
                            String delUserPassIn = input.next();
                            if (delUserPassIn.equals(pass[loginUsrID])) {
                                user[loginUsrID] = "";
                                pass[loginUsrID] = "";
                                nom[loginUsrID] = "";
                                cognom[loginUsrID] = "";
                                lastLogin[loginUsrID] = null;
                                userMenu = false;
                            }
                        } else if (selec.equals("no")) {
                            System.out.println("No, si ja m'esperava que et tiraries enrere.");
                        }
                        break;
                    case 4:
                        userMenu = false;
                        break;
                }
            }

        }
    }
}
