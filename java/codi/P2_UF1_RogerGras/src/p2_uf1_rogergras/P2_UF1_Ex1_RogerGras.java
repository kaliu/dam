
package p2_uf1_rogergras;
import java.util.Scanner;

public class P2_UF1_Ex1_RogerGras {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        // Definició de variables i constants
        final double PADDPREU = 5, PISCPREU = 4.5, JACPREU = 3, ESMPREU = 7.5;
        double cost = 0, preuPerTicket = 0;
        boolean seguir = true;
        int numTicket = 0, ticketsPad = 0, ticketsPisc = 0, ticketsJac = 0, ticketsEsm = 0;
        
        // Bucle principal
        while(seguir){
            // Menú i selecció d'opció
            System.out.printf("%nMenú del programa:%n"
                            + "\t 0. Sortir del programa%n"
                            + "\t 1. Paddle - %.1f€%n"
                            + "\t 2. Piscina - %.1f€%n"
                            + "\t 3. Jacuzzi - %.1f€%n"
                            + "\t 4. Esmorzar - %.1f€%n"
                            + "Introdueix la opció desitjada:%n",
                            PADDPREU, PISCPREU, JACPREU, ESMPREU);
            int selecc = input.nextInt();
            
            // Si la selecció es 0 no preguntar per el nombre de tickets desitjats
            if (selecc != 0){ 
            System.out.printf("%nIntrodueix cuants tickets vols:%n");
            numTicket = input.nextInt();
            }
            
            // En funció de la opció seleciconada fer les operacions pertinents
            // En el cas del 0 trencar el bucle
            switch(selecc){
                case 0:
                    seguir = false;
                    break;
                case 1:
                    cost = PADDPREU * numTicket;
                    preuPerTicket = PADDPREU;
                    ticketsPad += numTicket;
                    break;
                case 2:
                    cost = PISCPREU * numTicket;
                    preuPerTicket = PISCPREU;
                    ticketsPisc += numTicket;
                    break;
                case 3:
                    cost = JACPREU * numTicket;
                    preuPerTicket = JACPREU;
                    ticketsJac += numTicket;
                    break;
                case 4:
                    cost = ESMPREU * numTicket;
                    preuPerTicket = ESMPREU;
                    ticketsEsm += numTicket;
                    break;
            }
            
            // Si el bucle s'ha trencat aquesta linea no es mostra ja que no
            // s'ha calculat ningun preu
            if(seguir){
                System.out.printf("El cost será (%d * %.1f€) %.2f€%n", numTicket, preuPerTicket, cost);
            }
        }
        
        // De nou, si no es calcula res simplement printa un "Fins aviat"
        // Si el programa s'ha utilitzat printa el nombre de tickets per cada
        // activitat i acaba també
        if(cost != 0){
            System.out.printf("Tickets comprats:%n"
                            + "Paddle: \t %d%n"
                            + "Piscina: \t %d%n"
                            + "Jacuzzi: \t %d%n"
                            + "Esmorzar: \t %d%n"
                            + "Fins aviat!%n",
                            ticketsPad, ticketsPisc, ticketsJac, ticketsEsm);
        }else {
            System.out.println("Fins aviat!%n");
        }
    }
    
}
