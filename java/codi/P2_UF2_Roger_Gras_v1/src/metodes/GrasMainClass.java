package metodes;

import java.util.Random;

/**
 *
 * @author roger
 */
public class GrasMainClass {

    /**
     * Desordena els valors dins de una array bidimensional de Strings
     *
     * @param tauler de dos dimensions per a desordenar
     * @return Matrix de dos dimensions desordenat
     */
    public String[][] desordenarTauler(String[][] tauler) {
        Random rand = new Random();
        String aux = "";

        for (int i = 0; i < 50; i++) {
            int rand1 = rand.nextInt(4 - 0);
            int rand2 = rand.nextInt(2 - 0);

            int rand3 = rand.nextInt(4 - 0);
            int rand4 = rand.nextInt(2 - 0);
            aux = tauler[rand1][rand2];
            tauler[rand1][rand2] = tauler[rand3][rand4];
            tauler[rand3][rand4] = aux;
        }
        return tauler;
    }

    /**
     * Mostra la matriu
     *
     * @param tauler matriu a mostrar
     */
    public void mostrarTauler(String[][] tauler) {
        System.out.printf("    0 1 2%n"
                + "  |------%n");
        for (int i = 0; i < tauler.length; i++) {
            System.out.print(i + " | ");
            for (int j = 0; j < tauler[i].length; j++) {
                System.out.print(tauler[i][j] + " ");
            }
            System.out.println(" ");

        }
    }

    /**
     * Comproba si el tauler esta buit (el jugador ha guanyat)
     *
     * @param tauler
     * @return true si es buit(victoria), false si encara queden números
     */
    public boolean comprobarVictoria(String[][] tauler) {
        for (int i = 0; i < tauler.length; i++) {
            for (int j = 0; j < tauler[i].length; j++) {
                if (!tauler[i][j].equals(" ")) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Comprova la validesa de un nombre
     * @param valorin el valor a comprobar
     * @param max el maxim nombre al que pot arribar el valor
     * @return true si es valid, false si no
     */
    public boolean compVal(int valorin, int max) {
        if (valorin > max || valorin < 0) {
            return false;
        }
        return true;
    }
    
    /**
     * Troba el valor més alt a una matriu
     * @param tauler la matriu
     * @return el valor mes alt
     */
    public String valorMesAlt(String[][] tauler){
        String valor = "0";
        for (int i = 0; i < tauler.length; i++) {
            for (int j = 0; j < tauler[i].length; j++) {
                if (tauler[i][j].compareTo(valor) > 0){
                    valor = tauler[i][j];
                }
            }
        }
        
        return valor;
    }
    
    
    /**
     * Esborra del tauler els nombres desitjats
     * @param tauler la matriu
     * @param valor nombre a esborrar
     * @return tauler modificat
     */
    public String[][] esborrarCorrecte (String[][] tauler, String valor){
        for (int i = 0; i < tauler.length; i++) {
            for (int j = 0; j < tauler[i].length; j++) {
                if(tauler[i][j].equals(valor)){
                    tauler[i][j] = " ";
                }
            }
        }
        
        return tauler;
    }

}
