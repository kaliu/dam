package p2_uf2_roger_gras_v1;

import metodes.GrasMainClass;
import java.util.Scanner;

/**
 *
 * @author roger
 */
public class P2_UF2_Roger_Gras_v1 {

    public static void main(String[] args) {
        GrasMainClass m = new GrasMainClass();
        Scanner in = new Scanner(System.in);
        
        // Incialització del programa
        String[][] tauler = {
            {"1", "2", "3"},
            {"4", "5", "6"},
            {"1", "2", "3"},
            {"4", "5", "6"}
            };

        long iniciPartida = System.currentTimeMillis() / 1000;
        tauler = m.desordenarTauler(tauler);
        String valorMesAlt;
        
        // Bucle principal
        while(!m.comprobarVictoria(tauler)){
            m.mostrarTauler(tauler);
            
            // Obtenció de les coordenades per part del usuari
            System.out.println("Introdueïx la primera posició (22 per exemple) del número mes gran");
            int posicio1 = in.nextInt();
            int posicio12 = posicio1 % 10;
            int posicio11 = (posicio1 / 10) % 10;
            int[] posicio1val = {posicio12, posicio11}; 
            System.out.println("Introdueïx la segona posició del número mes gran");
            int posicio2 = in.nextInt();
            int posicio22 = posicio2 % 10;
            int posicio21 = (posicio2 / 10) % 10;
            
            // Comprobació de la validesa dels valors introduits (en cas de no enter java petarà igualment)
            if((m.compVal(posicio11, 2) && m.compVal(posicio12, 3)) && (m.compVal(posicio21, 2) && m.compVal(posicio22, 3))){
                
                valorMesAlt = m.valorMesAlt(tauler);
                if(tauler[posicio12][posicio11].equals(valorMesAlt) && tauler[posicio22][posicio21].equals(valorMesAlt)){
                    // Si es correcte es crida a esborrar del tauler aquests valors
                    tauler = m.esborrarCorrecte(tauler, valorMesAlt);
                } else {
                    System.out.println("ERROR: Els valors seleccionats no son correctes o no és el valor mes alt.");
                }
                
            } else {
                System.out.println("ERROR: Un dels valors es invalid");
            }
            
            
        }
        
        // Final del programa
        long finalPartida = System.currentTimeMillis() / 1000;
        System.out.printf("Has aconseguit completar el joc en %d segons. Felicitats!! %n", finalPartida - iniciPartida);
    }
}
