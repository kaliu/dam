
package prova1_uf1_rogergras;
import java.util.Scanner;

public class Prova1_UF1_RogerGras {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
       
        System.out.println("Introduïr el nombre de connectors que voleu comprar:");
       
        // Canvia el valor de connectors amb el nombre de connectors a comprar
        int connectors = input.nextInt();
        
        
        final int PACKPCC = 100;
        double preuPCC = 10.50;
        
        final int PACKELEC = 150;
        double preuElec = 12.20;

        int resPacksPCC = (connectors % PACKPCC == 0)? connectors / PACKPCC: connectors / PACKPCC + 1;
        int resPacksElec = (connectors % PACKELEC == 0)? connectors / PACKELEC: connectors / PACKELEC + 1;
        double resPreuPCC = preuPCC * resPacksPCC;
        double resPreuElec = preuElec * resPacksElec;
        
        System.out.printf("Proveïdor PCComponents\t %d Pack \t %.2f€%n"
                        + "Proveïdor Electronics\t %d Pack \t %.2f€%n"
                , resPacksPCC, resPreuPCC, resPacksElec, resPreuElec);
    }

    
}
