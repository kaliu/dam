
package provesbucles;

public class ProvesBucles {
    public static void main(String[] args) {
        
        int i = 0;
        while (i < 5) {
            System.out.println("*");
            i++;
        }
        
        
//        int i = 1; 
//        int suma=0; //Inici del bloc while
//        while (i<10) { //S'avalua la condició
//            suma+=i;
//            System.out.print("+"+i );
//            i++; } 
//            System.out.println("=" +suma);


//        int i = 97;   
//        while (i<123) {
//            System.out.print((char)i +" ");
//            i++;
//        }
//        System.out.println();
//        
//        
//        int i2 = 0;
//        while (i2<=19) {
//            System.out.print("* ");
//            i2++;
//            if (i2 == 10) {
//                System.out.println();
//            }
//        }
//        
//        
//        System.out.println();
//        int i3 = 0;
//        while (i3 < 1000) {
//            i3++;
//            if ((i3 % 7) == 0) {
//                System.out.printf("%d ",i3);
//            }
//        }
//        
//        System.out.println();
//        int i4 = 1, a = 8;
//        boolean continuar = true;
//        while (continuar) {
//            System.out.printf("%d ",i4);
//            i4++;
//            if (i4 == a) {
//                continuar = false;
//            }
//        }  
//        
//        int i5 = 0;
//        while (i5 < 100) {
//            System.out.printf("%d + 100 = %d", i5, (i5 + 100) );
//      }
    }
}
