
package stringsprovas;
import java.util.Scanner;

public class exercici3 {
    public static void main(String[] args) {     
    Scanner input = new Scanner(System.in);
    System.out.println("Introdueix un caràcter");
    char lletra = input.next().charAt(0); 
    
    boolean esAbc = (int) lletra >= 65 && lletra <= 90 || lletra >= 97 && lletra <= 122;
    System.out.printf("Està el caràcter introduït entre A/a i Z/z? %s%n", esAbc);
    }
}