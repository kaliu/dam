
package tipusdades2;


public class Exercici2 {
    public static void main(String[] args) {
        
        //Exercici 2
        int x = 4;
        int y = 6;
        double m = 3.2;
        double n = 5.3;
        
        System.out.println("El valor de X és: " + x);
        System.out.println("El valor de Y és: " + y);
        System.out.println("El valor de M és: " + m);
        System.out.println("El valor de N és: " + n);
         
        System.out.println("\nLa suma entre " + x + " i " + y + " és: " + (x+y));
        System.out.println("La resta entre " + x + " i " + y + " és: " + (x-y));
        System.out.println("El producte entre " + x + " i " + y + " és: " + (x*y));
        System.out.println("La divisió entre " + x + " i " + y + " és: " + (x/y));
        System.out.println("El reste de la divisió entre " + x + " i " + y + " és: " + (x%y));
        
        System.out.println("\nLa suma entre " + m + " i " + n + " és: " + (m+n));
        System.out.println("La resta entre " + m + " i " + n + " és: " + (m-n));
        System.out.println("El producte entre " + m + " i " + n + " és: " + (m*n));
        System.out.println("La divisió entre " + m + " i " + n + " és: " + (m/n));
        System.out.println("El reste de la divisió entre " + m + " i " + n + " és: " + (m%n));
        
        System.out.println("\nLa suma entre " + x + " i " + n + " és: " + (x+n));
        System.out.println("La divisió entre " + y + " i " + m + " és: " + (y/m));
        System.out.println("El reste de la divisió entre " + y + " i " + m + " és: " + (y%m));
        
        System.out.println("\nEl doble de " + x + " és: " + (x*2));
        System.out.println("El doble de " + x + " és: " + (y*2));
        System.out.println("El doble de " + n + " és: " + (n*2));
        System.out.println("El doble de " + m + " és: " + (m*2));

        System.out.println("\nLa suma de totes les variables és: " + (x+y+m+n));
        System.out.println("El producte de totes les variables és: " + (x*y*m*n));
    }
}
