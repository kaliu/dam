package tipusdades2;

public class Exercici5 {
    public static void main(String[] args) {
    
           String usuari1Nom = "Antoni";
           String usuari2Nom = "Pau";
           String usuari3Nom = "Laura";
            
           String usuari1Cognom1 =  "Lopez";
           String usuari2Cognom1 =  "García";
           String usuari3Cognom1 =  "Gonzalez";
           
           String usuari1Cognom2 = "Rodrigez";
           String usuari2Cognom2 = "Arenòs";
           String usuari3Cognom2 = "Bayés";
           
           
           int usuari1Edat = 17;
           int usuari2Edat = 5;
           int usuari3Edat = 101;
           
           double usuari1Altura = 1.67;
           double usuari2Altura = 1.4;
           double usuari3Altura = 1.552;
           
           String usuari1Ciutat = "Montcada i Reixac";
           String usuari2Ciutat = "Barcelona";
           String usuari3Ciutat = "Cerdanyola";
        
        
           System.out.printf("%-12s;%-15s;%-15s;%3s;%4.2f;%-20s%n",        
           usuari1Nom,usuari1Cognom1,usuari1Cognom2,usuari1Edat,usuari1Altura,usuari1Ciutat);
           System.out.printf("%-12s;%-15s;%-15s;%3s;%4.2f;%-20s%n",         
           usuari2Nom,usuari2Cognom1,usuari2Cognom2,usuari2Edat,usuari2Altura,usuari2Ciutat);
           System.out.printf("%-12s;%-15s;%-15s;%3s;%4.2f;%-20s%n",
           usuari3Nom,usuari3Cognom1,usuari3Cognom2,usuari3Edat,usuari3Altura,usuari3Ciutat);
        
        }
}
