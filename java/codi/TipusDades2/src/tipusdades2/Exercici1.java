package tipusdades2;

public class Exercici1 {

    public static void main(String[] args) {

        // Exercici 1
        int n = 5;
        double a = 2.3;
        char c = 'V';
        
        System.out.println("La variable n es: " + n);
        System.out.println("La variable a es: " + a);
        System.out.println("La variable c es: " + c);

        System.out.println("La suma de N i A es: " + (n+a) );
        System.out.println("La resta de N i A es: " + (n-a) );
        System.out.println("El valor numeric corresponent a la variable C es: " + (int) c);
        
    }
    
}
