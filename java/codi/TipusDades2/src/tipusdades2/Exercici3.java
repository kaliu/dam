
package tipusdades2;

public class Exercici3 {
    public static void main(String[] args) {
    
        // Exercici 3
        
        char inicialCognom = 'G';
        int edat = 19;
        double alcada = 1.7625;
        String ciutat = "Canovelles";
        boolean futbol = false;
        
        System.out.println("La inicial del meu cognom és: " + inicialCognom);
        System.out.println("La meva edat és: " + edat);
        System.out.println("La meva altura és: " + alcada);
        System.out.println("La meva ciutat és: " + ciutat);
        System.out.println("M'agrada el futbol? " + futbol);

        System.out.printf("%nLa inicial del meu nom és: %s%n", inicialCognom);
        System.out.printf("La meva edat és: %d%n", edat);
        System.out.printf("La meva altura és: %f%n", alcada);
        System.out.printf("La meva ciutat és: %s%n", ciutat);
        System.out.printf("M'agrada el futbol? %s%n", futbol);

        
    }
}
