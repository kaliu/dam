
package tipusdades2;

public class Exercici4 {
    public static void main(String[] args) {
    
        // Exercici 4
        double preuOrdinador = 500.40;
        int numOrdinadors = 30;
        double totalPreu = preuOrdinador * numOrdinadors;
    
        System.out.printf("Hi han %d ordinadors, el seu preu és %.2f€ i en total costen %.2f€ %n", numOrdinadors,preuOrdinador, totalPreu);
    }
}
