/*
* Demanar per teclat dues dates i mostrar el nombre de dies que hi ha de diferència.
* Suposant tots els mesos de 30 dies.
*/
package com.mycompany.uf1_practica_1_b;

import java.util.Scanner;

/**
 *
 * @author esther
 */
public class Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int dia1, mes1, any1;
        int dia2, mes2, any2;
        
        Scanner data = new Scanner(System.in);
        
        System.out.println("DATA 1");
        System.out.println("Introdueix el dia");
        dia1 = data.nextInt();
        System.out.println("Introdueix el mes");
        mes1 = data.nextInt();
        System.out.println("Introdueix l'any");
        any1 = data.nextInt();
        
        System.out.println("\nDATA 2");
        System.out.println("Introdueix el dia");
        dia2 = data.nextInt();
        System.out.println("Introdueix el mes");
        mes2 = data.nextInt();
        System.out.println("Introdueix l'any");
        any2 = data.nextInt();
        
        System.out.println("\n\n La diferència entre aquestes dues dates és de:");
        System.out.println(((any2-any1)*12*30 + (mes2-mes1)*30 + dia2-dia1 ) + " dies\n\n"); 
        // opció més avançada que printa en singular dia si la diferència és de només 1 dia.
        //System.out.println(((any2-any1)*12*30 + (mes2-mes1)*30 + dia2-dia1 ) == 1 ?  ((any2-any1)*12*30 + (mes2-mes1)*30 + dia2-dia1 )+ " dia": ((any2-any1)*12*30 + (mes2-mes1)*30 + dia2-dia1 )+" dies\n\n");
        
        
    }
    
}
