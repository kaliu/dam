/*
/*
 * Demana per teclat el nom, edat, salari base i mostra :
Si és menor de 16 no té edat per a treballar
Entre 19 i 50 anys el salari porta un increment del 5 per cent
Entre 51 i 60 anys el salari porta un increment del 10 per cent
Si és major de 60 el salari porta un increment del 20 per cent
 */
package com.mycompany.uf1_practica_1_b;

import java.util.Scanner;

/**
 *
 * @author esther
 */
public class Ex2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner lector = new Scanner(System.in);
        String nom;
        int edat;
        double salari;
        
        System.out.println("CALCUL DEL SALARI CORRESPONENT  ");
        System.out.println(" Introdueix el teu nom");
        nom = lector.nextLine();
        System.out.println(" Introdueix la teva edat");
        edat = lector.nextInt();
        System.out.println(" Introdueix el salari base que cobres actualment");
        salari = lector.nextDouble();
                
        
        System.out.printf ("\n\nDades del/la treballador/a:\n\t%s, %d anys i salari base anual de %.2f €\n", nom,edat,salari);
        System.out.print(edat<16?"\tNo tens edat per treballar":"");
        System.out.printf(edat>=16 && edat<=40 ?"\tLa teva edat està compresa entre 16 i 40 anys i et correspon un salari de %.2f € ":"", salari*1.05);
        System.out.printf(edat>=41 && edat<=60 ?"\tLa teva edat està compresa entre 41 i 60 anys i et correspon un salari de %.2f € ":"", salari*1.10);
        System.out.printf(edat>60 ?"\tEts major de 60 anys i et correspon un salari de %.2f € ":"",salari*1.20);
    }
    
}
