/*
/*
 * Fent servir operadors abreujats I UNA SOLA VARIABLE fes un programa que calculi la quantitat de pomes que queden al cistell en cada moment.
Ha de mostrar un missatge indicant cada operació realitzada i el seu resultat.
Inicialment, al matí hi havia 100 pomes i el Pere va llençar 3 que estàven en mal estat.
A migdia, van venir els 4 fills del Pere i cada un es va menjar 2.
A la tarda, en Pere va afegir al cistell el doble de les pomes que hi quedàven
Al vespre, i de manera encara desconeguda van desaparèixer 78 pomes.

 */
package com.mycompany.uf1_practica_1_b;

/**
 *
 * @author esther
 */
public class Ex3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int quantitatDePomes = 100;
        
        System.out.println("Inicialment hi havia al cistell " + quantitatDePomes + " pomes");
        quantitatDePomes-=3;
        System.out.println("El Pere va llençar 3 que estaven en mal estat i al cistell quedaven  " + quantitatDePomes + " pomes");
        quantitatDePomes-=8;
        System.out.println("A migdia, van venir els 4 fills del Pere i cada un es va menjar 2 i al cistell quedaven " + quantitatDePomes + " pomes");
        quantitatDePomes*=2;
        System.out.println("A la tarda, en Pere va afegir al cistell el doble de les pomes que hi quedàven i al cistell quedaven " + quantitatDePomes + " pomes");
        quantitatDePomes-=78;
        System.out.println("Al vespre, i de manera encara desconeguda van desaparèixer 78 pomes i finalment al cistell queden " + quantitatDePomes + " pomes\n\n");
        




        
        
    }
    
}
