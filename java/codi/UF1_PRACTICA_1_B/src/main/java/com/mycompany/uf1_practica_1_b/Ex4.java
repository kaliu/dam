/*
/*
 * Les aules de l’institut tenen un nombre constant i fixe de 30 PC. 
Mostra per pantalla la quantitat màxima d’alumnes que poden estar matriculats d’informàtica tenint 
en compte que en torn de matí tenim 6 aules disponibles i en torn de tarda 5 aules disponibles ja que una la fan servir per Batxillerat.
Si el preu de manteniment del software de cada PC és de 1€ mensual mostra el cost anual d’aquest manteniment.

 */
package com.mycompany.uf1_practica_1_b;

/**
 *
 * @author esther
 */
public class Ex4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        final int PC = 30;
        int aulesDisponibles = 6;
        int alummesMatriculatsInformatica;
        float preuMantenimentPC = 1f;
        float costMantenimentAnual;
        
        
        //Torn de Matí 6 aules + torn de tarda 5 aules
        alummesMatriculatsInformatica = PC * ( aulesDisponibles + aulesDisponibles - 1);
        
        System.out.println ("La quantitat màxima d’alumnes que poden estar matriculats d’informàtica és de "  + alummesMatriculatsInformatica + " alumnes/as");
        
        // El preu anual és 12 vegades el nombre de PC tenint en compte que tenim 6 aules i cada pc te un cost de 1€
        costMantenimentAnual = PC * aulesDisponibles * preuMantenimentPC * 12;
        System.out.printf ("Si el preu de manteniment del software de cada PC és de 1€ mensual el cost anual d’aquest manteniment és de %.2f €\n\n", costMantenimentAnual); 

        
    }
    
}
