package validarentrades;

import java.util.Scanner;

public class ValidarEntradaNombreReal {

    public static void main(String[] args) {
        double num = 0;
        Scanner lector = new Scanner(System.in);
        boolean continuar = true;

        while (continuar) {
            System.out.println("Introduir un nombre real positiu:");

            while (!lector.hasNextDouble()) {
                lector.nextLine();
                System.out.println("Valor no numèric, tornar a introduir-lo.");
            }

            num = lector.nextDouble();

            if (num > 0) {
                continuar = false;
            } else {
                System.out.println("Valor no positiu.");
                lector.nextLine();
            }
        }

        System.out.println("Nombre introduït: " + num);

    }

}
