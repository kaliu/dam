package variablesformatsortida;

public class VariablesFormatSortida {

    public static void main(String[] args) {
        
       // Descomptes
       final double IVA = 0.21;
       double descomptePantalo = 0.15;
       double descompteSabates = 0.05;
       
       // Preus dels productes inicialment
       double preuSamarreta = 100;
       double preuPantalo = 100;
       double preuSabates = 100;
       
       // Variables de càlculs
       double preuDescomptat;
       double preuDescomptat2;
       double preuDescomptatTotal;
       double preuFinal;
       double preuFinal2;
       
       
       preuDescomptat = preuSamarreta * IVA;
       preuFinal = preuSamarreta - preuDescomptat;
       System.out.printf("El preu de la sammarreta és %.2f€, el preu sense IVA "
               + "és %.2f€ i m'he estalviat %.2f€%n"
               ,preuSamarreta, preuFinal, preuDescomptat);
        
       preuDescomptat = preuPantalo * descomptePantalo;
       preuFinal = preuPantalo - preuDescomptat;
       
       preuDescomptat2 = preuSabates * descompteSabates;
       preuFinal2 = preuSabates - preuDescomptat2;
               
       System.out.printf("També m'he comprat un pantalo de %.2f€ per %.2f€ i"
               + "unes sabates de %.2f€ per %.2f€%n",
               preuPantalo, preuFinal, preuSabates, preuFinal2);
       
       System.out.printf("En total m'he estalviat %.2f€%n", preuDescomptat + 
               preuDescomptat2 + (preuSamarreta * IVA));
    }
    
}
