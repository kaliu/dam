package variablesformatsortida;

import java.util.Scanner;

public class VariablesFormatSortida {

    public static void main(String[] args) {
        
        Scanner inputDescomptePantalo = new Scanner(System.in);
        Scanner inputDescompteSabates = new Scanner(System.in);
        Scanner inputPreuSamarreta = new Scanner(System.in);
        Scanner inputPreuSabates = new Scanner(System.in);
        Scanner inputPreuPantalo = new Scanner(System.in);
        
       // Descomptes
       final double IVA = 0.21;
       double descomptePantalo;
       double descompteSabates;
       
       // Preus dels productes inicialment
       double preuSamarreta;
       double preuPantalo;
       double preuSabates;
       
       // Variables de càlculs
       double preuDescomptat;
       double preuDescomptat2;
       double preuDescomptatTotal;
       double preuFinal;
       double preuFinal2;
       
       System.out.println("Introdueix el preu de la samarreta:");
       preuSamarreta = inputPreuSamarreta.nextDouble();
       
       
       preuDescomptat = preuSamarreta * IVA;
       preuFinal = preuSamarreta - preuDescomptat;
       System.out.printf("El preu de la sammarreta és %.2f€, el preu sense IVA "
               + "és %.2f€ i m'he estalviat %.2f€%n"
               ,preuSamarreta, preuFinal, preuDescomptat);
       
       System.out.println("Introdueix el preu de els pantalons:");
       preuPantalo = inputPreuPantalo.nextDouble();
       System.out.println("Introdueix el descompte dels pantalons en un decimal del 0 al 1:");
       descomptePantalo = inputDescomptePantalo.nextDouble();
       
       System.out.println("Introdueix el preu de les sabates:");
       preuSabates = inputPreuSabates.nextDouble();
       System.out.println("Introdueix el descompte de les sabates en un decimal del 0 al 1:");
       descompteSabates = inputDescompteSabates.nextDouble();
       
       preuDescomptat = preuPantalo * descomptePantalo;
       preuFinal = preuPantalo - preuDescomptat;
       
       preuDescomptat2 = preuSabates * descompteSabates;
       preuFinal2 = preuSabates - preuDescomptat2;
               
       System.out.printf("També m'he comprat un pantalo de %.2f€ per %.2f€ i"
               + "unes sabates de %.2f€ per %.2f€%n",
               preuPantalo, preuFinal, preuSabates, preuFinal2);
       
       System.out.printf("En total m'he estalviat %.2f€%n", preuDescomptat + 
               preuDescomptat2 + (preuSamarreta * IVA));
    }
    
}
