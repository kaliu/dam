
package autoavaluació1;
import java.util.Scanner;

public class exercici1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
       
        System.out.println("Escriu el teu nom:");
        String nom = input.nextLine();
        System.out.println("Introdueix el nom del módul:");
        String modul = input.nextLine();
        System.out.println("Introdueix la nota de la UF1:");
        double uf1 = input.nextDouble();        
        System.out.println("Introdueix la nota de la UF2:");
        double uf2 = input.nextDouble();
        System.out.println("Introdueix la nota de la UF3:");
        double uf3 = input.nextDouble();   
        
        boolean aprobat = uf1 >= 5 && uf2 >= 5 && uf3 >= 5;
        double mitjana = (uf1 + uf2 + uf3) / 3;
        
        System.out.printf("%nBenvolgut/da %s, segons les dades que ens consten: %n"
                + "Nota UF1: %.2f       Nota UF2: %.2f      Nota UF3: %.2f%n"
                + "La nota del mòdul %s és de %.2f%n"
                + "La superació de cada una de les UF i per tant del mòdul %s és %s%n", 
                nom, uf1, uf2, uf3, modul, mitjana, modul, aprobat);
    }
     
}
