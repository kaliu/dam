
package autoavaluació1;

public class exercici4 {
    public static void main(String[] args) {
        
        double preuBoli = 1.3;
        final double iva = 0.21;
        
        System.out.printf("El preu del bolígraf amb IVA és %.2f%n", preuBoli);
        System.out.printf("El preu del bolígraf sense IVA és %.2f%n", (preuBoli - (preuBoli * iva)));
        System.out.printf("El pack de 10 bolígrafs amb IVA és %.2f%n", (preuBoli * 10));        
    }
}
