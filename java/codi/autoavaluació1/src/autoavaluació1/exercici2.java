
package autoavaluació1;
import java.util.Scanner;


public class exercici2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix el radi (en metres) del terra que vols enrajolar:");
        double radiEnrajolar = input.nextDouble();
        final double preuRajola = 10;
        double areaTerra = 3.1416 * (radiEnrajolar * radiEnrajolar);
        
        System.out.printf("El cost d'enrajolar el terra será de %.2f€ %n", areaTerra * preuRajola);
        
    }
}
