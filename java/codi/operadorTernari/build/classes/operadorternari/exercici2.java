package operadorternari;
import java.util.Scanner;

public class exercici2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Intodueix un numero:");
        int a1 = input.nextInt();
        System.out.println("Introdueix un altre numero:");
        int a2 = input.nextInt();
        
        String mesGran = (a1>a2)?"a1":"a2";
        
        System.out.printf("El numero mes gran es %s", mesGran);
    }
    
}
