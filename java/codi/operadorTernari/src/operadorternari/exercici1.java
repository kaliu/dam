
package operadorternari;
import java.util.Scanner;

public class exercici1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Introdueix la teva edat:");
        int edat= input.nextInt();
        String major = (edat>=18)?"Ets":"No ets";
        System.out.printf("%s majot de edat %n", major);
    }
}