
package operadorternari;
import java.util.Scanner;

public class exercici4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Introdueix un número:");
        int num = input.nextInt();
        
        String esParell = (num % 2 == 0)?"es parell,":"no es parell,";
        String esMes100 = (num > 100)?"és més gran que 100":"no es més gran que 100";
        String entre120;
        entre120 = (num >= 120 && num <= 127)?"i está comprés entre el 120 i el 127.":
                "no esta comprés entre el 120 i el 127";
        
        
        System.out.printf("El numero %d %s %s %s%n", num, esParell, esMes100, entre120);
    }
}
