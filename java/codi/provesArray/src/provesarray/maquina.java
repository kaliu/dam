package provesarray;
import java.util.Scanner;

public class maquina {
    public static void main(String[] args) {
        String[][] nom={{"Manzana", "Pera", "Mandarina"},{"Fresa", "Naranja", "Piña"}};
        float[][] preu = {{2.4F, 2.3F, 9.3F},{13.2F, 10.3F, 10.3F}};
        Scanner input = new Scanner(System.in);
       
        System.out.printf("Bienvenido a la máquina expendedora.%n"
                        + "Esta es nuestra selección de productos:%n");
        for(int i = 0; i < nom.length; i++){
            for(int j = 0; j < nom[i].length; j++){
                System.out.printf("%20s ", nom[i][j]);
            }
            System.out.println("");
        }
        System.out.println("Escribe a continuación la que deseas para que se"
                            + " te muestre el precio:");
        String respuesta = input.nextLine();
        
        for(int i = 0; i < nom.length; i++){
            for(int j = 0; j < nom[i].length; j++){
                if(respuesta.equals(nom[i][j])){
                    System.out.printf("La %s está ubicada en la fila %d, columna %d"
                                    + " y tiene un precio de %.2f€%n", respuesta, i+1, j+1, preu[i][j]);
                    System.exit(0);
                }
            }
        }
        System.out.println("Producto no encontrado");
    }
}
