package repte_bucle_rgras;
import java.util.Scanner;

public class Repte_bucle_rgras {

    public static void main(String[] args) {
        // Definir scanner y variables
        Scanner input = new Scanner(System.in);
        int num1, num2, signe, result = 0, encerts = 0, errors = 0;
        char signeSim = '.';
        int resposta;
        // Información inicial
        System.out.printf("Benvingut al joc de les operacions! A continuació %n"
                        + "es mostraran operacions aleatòries i les haureu de %n"
                        + "resoldre correctament. Els números van del 1 al 100 i %n"
                        + "les operacions són suma, resta, multiplicació i divisió. %n"
                        + "Pots respondre 0 en cualsevol moment per sortir. %n"
                        + "Bona sort!! %n");
        
        // Generacio y validacio de numeros y operadors
       
        while (true) {
            boolean invalidDiv = true;
            boolean validSameNum = true;
            // Generació aleatoria de numeros i operador
            num1 = (int) (Math.random() * 100 + 1);
            num2 = (int) (Math.random() * 100 + 1);
            while (validSameNum) {
                if (num1 != num2) {
                    validSameNum = false;
                } else {
                    num1 = (int) Math.floor(Math.random()*100+1);
                    num2 = (int) Math.floor(Math.random()*100+1);
                }
            }
            
            signe = (int) Math.floor(Math.random()*4+1);
            
            // Conversió del signe en num i resolucio de operació
            switch (signe) {
                case 1:
                    signeSim = '+';
                    result = num1 + num2;
                    break;
                case 2:
                    signeSim = '-';
                    result = num1 - num2;
                    break;
                case 3:
                    signeSim = '*';
                    result = num1 * num2;
                    break;
                case 4:
                    signeSim = '/';
                    break;
            }
            // Validació y recalcul si es necessari (divisions)
            if (signe == 4){
                while (invalidDiv) {
                    if (num1 % num2 == 0 && num1 / num2 != 0){
                        result = num1 / num2;
                        invalidDiv = false;
                    } else {
                        num1 = (int) Math.floor(Math.random()*100+1);
                        num2 = (int) Math.floor(Math.random()*100+1);
                    }
                }
            }
            // Printa la operacio i demana resposta            
            System.out.printf("%n%d %s %d és: ", num1, signeSim, num2);
            resposta = input.nextInt();
            // Comproba respota o sortida i suma puntuacio
            if (resposta == 0){
                System.out.printf("Sortint del programa. El nombre de encerts ha estat %d i el de erros %d.%n"
                                  , encerts, errors);
                System.exit(0);
            } else if (resposta == result) {
                encerts += 1;
                System.out.printf("Ben fet! El nombre de encerts es %d i el de erros %d.", encerts, errors);
            } else {
                errors += 1;
                System.out.printf("Error! El nombre de encerts es %d i el de errors %d.", encerts, errors);
            }
        }
    }
}
