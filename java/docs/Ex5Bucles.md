

## 1.

```java
import java.util.Scanner;
public class xxxx
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
        int num;
        int fact = 1;
        
        System.out.print("Enter any positive integer: ");
        num = console.nextInt();
       
        for(int i=1; i<=num; i++)
        {
            fact *= i;
        }
        
        System.out.println(fact);
    }
}
```

Si introdueixes 7 printarà 20. Multiplica tots els nombres fins que el resultat es major que el nombre introduït.



## 2.

```java
public class Test {
public static void main(String[] args)
    {
        int i = 0;
        for (System.out.println("HI"); i < 1; i++)
            System.out.println("HELLO GEEKS");
    }
}
```

Printarà:

**HI**

**HELLO GEEKS**



## 3.

```java
import java.util.Scanner;

public class xxxx

{
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
        int number;
        
        System.out.print("Enter the positive integer ");
        number = console.nextInt();
        boolean flag = true;
        
        for(int i = 2; i < number; i++)    {
        	if(number % i == 0){
                	flag = false;
                	break;
            }
       	}
      
        if(flag && number > 1) {
            System.out.println("Number is xxx");
        } else {
            System.out.println("Number is not xxx");
        }
     }  
}

```

Pregunta un enter positiu. Comprova que el número sigui prim o no(primer comprova si es divisible entre qualsevol nombre entre 2 i el número anterior al nombre i desprès si no deixa resta).

En cas de que sigui prim (i de que sigui major que 1) printa que es prim. Si no es compleix aquestes condicions es que no es prim.



## 4.

```javA
class Test {
public static void main(String[] args)
    {
        int x = 1, y = 2;
        do
            System.out.println("FRIENDS");
        while (x < y);
        System.out.println("ENEMY");
    }
}
```

Printarà:

FRIENDS

ENEMY (infinites vegades)



## 5.

```java
import java.util.Scanner;

public class xxxxx
{
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
        int num;
        
        System.out.print("Enter any positive integer: ");
        num = console.nextInt();
                
        System.out.println("xxxxxxxxxxxxxxxxx " + num);
        
        for(int i=1; i<=10; i++)
        {
            System.out.println(num +" x " + i + " = " + (num*i) );
        }
    }
}
```

Demanarà un numero(**num**). Printarà:

​	xxxxxxxxxxxxxxxxx **num**

I printarà 10 vegades

​	**num** x **i** = (**num** * **i**)

Exemple. si **num** és 2

​	xxxxxxxxxxxxxxxxx 2

​	2 x 1 = 2

​	2 x 2 = 4

​	etc.



## 6.

```java
import java.util.Scanner;

public class xxxxxxx
{
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
        
        int n1;
        int n2;
        int result = 1;
        
        System.out.print("Enter xxxxxxxxx ");
        n1 = console.nextInt();
        
        System.out.print("Enter xxxxxxxxx ");
        n2 = console.nextInt();

        for(int i = 1; i <= n2; i++)
        {
        result *= n1;
        }

        System.out.println("Result: "+ result);
    }
}
```

Multiplica **n1** por si mismo **n2** veces

## 7.

 ```java
import java.util.Scanner;

public class xxxxxxxxxxx
{
    public static void main(String[] args)
    {
        Scanner console = new Scanner(System.in);
     
        int number;
        int reverse = 0;
        
        System.out.print("Enter the number ");
        number = console.nextInt();
        
        int temp = number;
        int remainder = 0;
        
        while(temp>0)
        {
        remainder = temp % 10;
        reverse = reverse * 10 + remainder;
            temp /= 10;
        }

        System.out.println("xxxxxx" + number + " is " + reverse);
    }
}
 ```

Al introduir un nombre et retorna el nombre escrit al inrevés.

Per exemple, si introduïm 12 retorna 21.

## 8.

```java
public class xxxxxxxxxxxxx
{
   public static void main(String[] args)
   {
      final int MAXR = 4,
                MAXC = 5;

      for (int i = 1; i <= MAXR; i++)
      {
         for (int j = 1; j <= MAXC; j++)
         {
            System.out.print("*");
         }
         System.out.println();
      }
   }
}
```

El primer bucle imprimirà una línia buida i també executarà un bucle. Aquest segon bucle s'executai printa 5 vegades un *. Per lo tant el primer bucle es crea una linia, el segon printa 5 * i això passa 4 vegades.

## 9.

```java
class xxxxxxxxxxxxx {
    public static void main(String args[]){
         for(int i=1; i>=1; i++){
              System.out.println("The value of i is: "+i);
         }
    }
}
```

Imprimeix el valor de **i** de forma infinita. El valor de **i** serà simplement i + 1 cada vegada.



## 10.

```java
public class xxxxxxxxxxxx{
    public static void main(String[] args) {
        for (char ch = 'a' ; ch <= 'z' ; ch++)
            System.out.println(ch);
}
```

Printa totes les lletres de la a *a* la *z*.

