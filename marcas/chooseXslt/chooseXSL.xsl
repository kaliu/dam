<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/" >
        <html>
            <head>
                <meta charset="UTF-8"/>
                <style>
                    table, td, th {
                        border: 1px solid black;   
                    }
                    td {
                        color: white;
                        border: 1px solid black;   
                    }
                </style>
            </head>
            <body>
                <h1> Llibres per sobre de 10€ </h1>
                <table>
                    <tr>
                        <th>ISBN</th>
                        <th>Títol</th>
                        <th>Autor</th>
                    </tr>
                    <xsl:for-each select="llibreria/llibre">
                        <xsl:if test="preu &gt; 10">
                            <xsl:choose>
                                <xsl:when test="preu &gt; 25">
                                    <tr style="background-color: red;">
                                        <td><xsl:value-of select="isbn"/></td>
                                        <td><xsl:value-of select="titol"/></td>
                                        <td><xsl:value-of select="autor"/></td>
                                    </tr>
                                </xsl:when>
                                <xsl:when test="preu &lt; 25 or preu = 25">
                                    <tr style="background-color: green;">
                                        <td><xsl:value-of select="isbn"/></td>
                                        <td><xsl:value-of select="titol"/></td>
                                        <td><xsl:value-of select="autor"/></td>
                                    </tr>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>