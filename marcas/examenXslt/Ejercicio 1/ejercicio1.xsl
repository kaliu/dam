<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <xsl:for-each select="/Gimnasio/Monitor">
                <xsl:sort select="nomMonitor" order="descending"/>
                    <xsl:if test="sueldo &gt; '1600' or sueldo = '1600'">
                        <h2>
                            <xsl:value-of select="nomMonitor"/>
                        </h2>
                        <xsl:for-each select="especilidad">
                            <xsl:value-of select="."/> <br/>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
