<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:variable name="sueldoIni" select="/Gimnasio/sueldoInicial"/>
    <xsl:variable name="sueldoFin" select="/Gimnasio/sueldoFinal"/>

    <xsl:template match="/">
        <html>
            <body>
                <ul>
                    <xsl:for-each select="/Gimnasio/Monitor">
                        <xsl:for-each select="especilidad">
                            <xsl:if test=". = 'Zumba' and ../sueldo &gt; $sueldoIni and ../sueldo &lt; $sueldoFin">
                                <li><xsl:value-of select="../@CodMonito"/>&#160;<xsl:value-of select="../nomMonitor"/></li>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>