<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <style>
                th, td {
                    border: 1px solid black;
                    padding-left: 5px;
                    padding-right: 5px;
                }
            </style>
            <body>
                <table>
                    <tr>
                        <th>Nombre Monitor</th>
                        <th>Sueldo y moneda</th>
                        <th>Clases</th>
                    </tr>
                    <xsl:for-each select="Gimnasio/Monitor">
                            <tr>
                                <td><xsl:value-of select="nomMonitor"/></td>
                                <td><xsl:value-of select="sueldo"/>&#160;<xsl:value-of select="sueldo/@moneda"/></td>
                                <td>
                                    <xsl:for-each select="especilidad">
                                        <xsl:value-of select="."/> <br/>
                                    </xsl:for-each>
                                </td>
                                <td>
                                    <xsl:element name="a">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="nomMonitor/@ubicacion"/>
                                        </xsl:attribute>
                                        Ciudad
                                    </xsl:element>   
                                </td>
                            </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>