xquery version "3.0";

for $ball in doc("academia_ball.xml")/Balls/ball
let $profe := $ball/professor
where $ball/preu/@quota eq "mensual"
return $profe
