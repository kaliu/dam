<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template match="/">
        <html>
         <head>
        	<meta charset="UTF-8"/>
        </head>
         <body>
             <h1>La meva biblioteca</h1>
             <table>
                 <tr style="background-color:green">
                     <th>Títol</th>
                     <th>Autor</th>
                     <th>Preu </th>
                 </tr>
                 <xsl:for-each select="llibreria/llibre">
                    <xsl:sort select="preu" data-type="number" order="descending"/>
                    <xsl:if test="autor = 'Carlos Garre'">
                        <tr>
                            <td><xsl:value-of select="titol"/></td>
                            <td><xsl:value-of select="autor"/></td>
                            <td><xsl:value-of select="preu"/></td>
                        </tr>
                    </xsl:if>
                 </xsl:for-each>
             </table>
         </body>
        </html> 
    </xsl:template>
</xsl:stylesheet>