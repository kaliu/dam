<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
            </head>
            <body>
                <h1> La biblioteca d'en Roger </h1>
                <table>
                    <tr style="background-color:red">
                        <th>Títol</th>
                        <th>Autor</th>
                        <th>Preu</th>
                    </tr>
                    <xsl:for-each select="llibreria/llibre">
                        <xsl:if test="preu &gt; 10 and autor = ' Virginia Woolf '">
                                <tr>
                                    <td><xsl:value-of select="titol"/></td>
                                    <td><xsl:value-of select="autor"/></td>
                                    <td><xsl:value-of select="preu"/></td>
                                </tr>
                            </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html> 
    </xsl:template>
</xsl:stylesheet>