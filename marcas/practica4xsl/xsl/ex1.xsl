<?xml version="1.0" encoding="UTF-8"?>
<!-- Crea una lista numerada con los nombre de los héroes ordenados descendentemente. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/" >
        <html>
            <head>
                <meta charset="UTF-8"/>
            </head>
            <body>
                <h1> Lista numerada con los nombres de los héroes </h1>
                <ol>
                    <xsl:for-each select="/lista/ciudad">
                        <xsl:for-each select="grupoHeroes">
                            <xsl:for-each select="heroe">
                                <li><xsl:value-of select="."/></li>
                            </xsl:for-each>
                        </xsl:for-each>
                    </xsl:for-each>
                </ol>    
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>