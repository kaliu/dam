<?xml version="1.0" encoding="UTF-8"?>
<!-- Crea una lista numerada por grupos de héroes, dentro ella pon una lista no numerada con 
     los héroes y al lado la ciudad en la que actuan. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h1> Grupos de héroes </h1>
                <ol>
                    <xsl:for-each select="/lista/ciudad">
                        <xsl:for-each select="grupoHeroes">
                            <li><xsl:value-of select="nombreGrupo"/></li>
                                <ul>
                                    <xsl:for-each select="heroe">
                                        <li><xsl:value-of select="."/>&#160;<xsl:value-of select="../../nombreCiudad"/></li>
                                    </xsl:for-each>
                                </ul>
                        </xsl:for-each>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>