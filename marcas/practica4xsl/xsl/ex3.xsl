<?xml version="1.0" encoding="UTF-8"?>
<!-- Muestra los nombres y la edad de los héroes que actúen en Barcelona y que tengan más de 28 años. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h1> Héroes en Barcelona que tienen más de 28 años </h1>
                <xsl:for-each select="/lista/ciudad">
                    <xsl:for-each select="grupoHeroes">
                        <xsl:for-each select="heroe">
                            <xsl:if test="@edad &gt; 28 and ../../nombreCiudad = 'Barcelona'">
                                <xsl:value-of select="."/> <br/>    
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:for-each> 
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>