<?xml version="1.0" encoding="UTF-8"?>
<!-- Haz la misma lista ordenada que el anterior ejercicio por edad descendentemente. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h1> Héroes en Barcelona que tienen más de 28 años </h1>
                <xsl:for-each select="/lista/ciudad">
                    <xsl:for-each select="grupoHeroes">
                        <xsl:for-each select="heroe">
                            <xsl:sort select="@edad">
                                <xsl:if test="@edad &gt; 28 and ../../nombreCiudad = 'Barcelona'">
                                    <xsl:value-of select="."/> <br/>
                                </xsl:if>
                            </xsl:sort>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:for-each> 
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>