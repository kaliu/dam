<?xml version="1.0" encoding="UTF-8"?>
<!-- Crea  un  fitxer  XSL  (“p1.xsl”) i  general  l’HTML:
      on  es  mostrin  totes  les  dades  de  les  
      pel·lícules  (menys la foto). -->
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
            </head>
            <body>
                <h1>La meva biblioteca de pelicules</h1>
                <table>
                    <tr style="background-color:green">
                        <th>Títol</th>
                        <th>Nacionalitat</th>
                        <th>Productor</th>
                        <th>Director</th>
                        <th>Any</th>
                        <th>Duració</th>
                        <th>Gènere</th>
                        <th>Sinopsis</th>
                        <th>Format</th>
                    </tr>
                    <xsl:for-each select="pelicules/pelicula">
                        <tr>
                                <td><xsl:value-of select="titol"/></td>
                                <td><xsl:value-of select="nacionalitat"/></td>
                                <td><xsl:value-of select="productor"/></td>
                                <td><xsl:value-of select="director"/></td>
                                <td><xsl:value-of select="any"/></td>
                                <td><xsl:value-of select="duracio"/></td>
                                <td><xsl:value-of select="genere"/></td>
                                <td style="font-size: 10;"><xsl:value-of select="sinopsis"/></td>
                                <td><xsl:value-of select="format"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>

    </xsl:template>


</xsl:stylesheet>  