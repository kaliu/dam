<?xml version="1.0" encoding="UTF-8"?>
<!-- on es mostri el títol, director i fotografia 
    (la imatge) de totes les pel·lícules emmagatzemades. -->
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
            </head>
            <body>
                <h1>La meva biblioteca de pelicules</h1>
                <table>
                    <tr style="background-color:green">
                        <th>Títol</th>
                        <th>Director</th>
                        <th>Fotografía</th>
                    </tr>
                    <xsl:for-each select="pelicules/pelicula">
                        <tr>
                                <td><xsl:value-of select="titol"/></td>
                                <td><xsl:value-of select="director"/></td>
                                <td>            
                                    <xsl:element name="img">
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="foto"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="style">
                                            height: 100px;
                                            width: auto;
                                        </xsl:attribute>
                                    </xsl:element>
                                </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>

    </xsl:template>


</xsl:stylesheet>  