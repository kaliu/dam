<?xml version="1.0" encoding="UTF-8"?>
<!--  igual que l’anterior però realitzant plantilles y que al 
    fer clic en la fotografia, ens porti a la URL emmagatzemada. -->
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
            </head>
            <body>
                <h1>La meva biblioteca de pelicules</h1>
                <table>
                    <tr style="background-color:green">
                        <th>Títol</th>
                        <th>Director</th>
                        <th>Fotografía</th>
                    </tr>
                        <xsl:apply-templates />
                </table>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="pelicula">
        <tr>
            <td><xsl:value-of select="titol"/></td>
            <td><xsl:value-of select="director"/></td>
            <td>
                <xsl:element name="a">
                    <xsl:attribute name="href">
                        <xsl:value-of select="URL"/>
                    </xsl:attribute>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="foto"/>
                        </xsl:attribute>
                        <xsl:attribute name="style">
                            height: 100px;
                            width: auto;
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>   

            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>  