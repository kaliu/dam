<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h1>Media de notas</h1>
                <table>
                    <tr>
                        <th>Alumno</th>
                        <th>Media de Notas</th>
                    </tr>
                    <xsl:for-each select="notes/alumne">
                        <tr>
                            <td>
                                <xsl:value-of select="nom" />
                            </td>
                            <td>
                                <xsl:value-of select="(notaSocials + notaMates + notaEF) div 3" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>