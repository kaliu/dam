<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <style>
                    table, tr, th, td {
                        text-align: center;
                        border: black solid 1px
                    };
                </style>
            </head>
            <body>
                <h1>Media de notas</h1>
                <table>
                    <tr>
                        <th> Alumno </th>
                        <th> Media de Notas </th>
                        <th> Calificación </th>
                    </tr>
                    <xsl:for-each select="notes/alumne">
                        <xsl:variable name="mediaNotas" select="floor((notaSocials + notaMates + notaEF) div 3)"/>
                        <tr>
                            <td>
                                <xsl:value-of select="nom" />
                            </td>
                            <td>
                                <xsl:value-of select="$mediaNotas" />
                            </td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="$mediaNotas &gt; 4.9 and $mediaNotas &lt; 7">
                                        Suficiente
                                    </xsl:when>
                                    <xsl:when test="$mediaNotas &gt; 6.9 and $mediaNotas &lt; 9">
                                        Notable
                                    </xsl:when>
                                    <xsl:when test="$mediaNotas &gt; 8.9 and $mediaNotas &lt; 10">
                                        Excelente
                                    </xsl:when>
                                    <xsl:otherwise>
                                        Suspenso
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>