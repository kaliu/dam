<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <table border="1">
                <xsl:for-each select="/flota/sistema/planeta">
                    <ul>
                        <b>Planeta:
                        <xsl:value-of select="nomPlaneta"/> </b> <br/>
                        Lunas:
                        <xsl:for-each select="luna">
                            <li><xsl:value-of select="."/></li>
                        </xsl:for-each>
                    </ul>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>