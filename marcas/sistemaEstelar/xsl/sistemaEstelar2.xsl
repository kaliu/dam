<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <table border="1">
                <xsl:for-each select="/flota/sistema/planeta">
                    <xsl:if test="habitantes > 50000">
                        <ol>
                            <li><xsl:value-of select="nomPlaneta"/></li>
                        </ol>
                    </xsl:if>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>