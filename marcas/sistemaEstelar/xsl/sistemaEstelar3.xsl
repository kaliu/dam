<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <table border="1">
                <tr style="background-color:red">
                    <th> Nombre de la nave </th>
                    <th> Sistema a la que pertenece</th>
                </tr>
                    <xsl:for-each select="/flota/sistema/nave">
                    <xsl:if test="tripulacion/capitan/apellidosCapitan = 'Smith'">
                    <tr>
                            <td><xsl:value-of select="nomNave"/></td>
                            <td><xsl:value-of select="/flota/sistema/@cod"/></td>
                    </tr>
                    </xsl:if>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>