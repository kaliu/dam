<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <h5> Naves con Cañones Laser de nivel 6</h5>
            <xsl:for-each select="/flota/sistema/nave">
                <xsl:if test="arma/tipo/@nivel = 6 and arma/tipo = 'Cañon laser'">
                    <xsl:value-of select="tripulacion/capitan/nomCapitan"/>
                </xsl:if>
            </xsl:for-each>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>