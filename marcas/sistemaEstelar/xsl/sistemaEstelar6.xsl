<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <h5> Planetas con más de 20000 habitantes pero menos que 50000 </h5>
            <xsl:for-each select="/flota/sistema/planeta">
                <xsl:if test="(habitantes &gt; 20000) and (habitantes &lt; 50000)">
                    <xsl:value-of select="nomPlaneta"/> <br/>
                </xsl:if>
            </xsl:for-each>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>