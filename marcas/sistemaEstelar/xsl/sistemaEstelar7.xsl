<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <body>
            <h5> Naves con torpedos o misiles </h5>
            <xsl:for-each select="/flota/sistema/nave">
                <xsl:if test="arma/tipo = 'Torpedor' or arma/tipo = 'Misiler'">
                    <xsl:value-of select="nomNave"/> <br/>
                </xsl:if>
            </xsl:for-each>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>