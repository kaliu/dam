<?xml version="1.0" encoding="UTF-8"?>
<!-- Especificar una transformación que genere un documento XML a partir de la 
    entrada del enunciado en el que sólo se muestren las precipitaciones de 
    una ciudad determinada. -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="ciudad" select="'Sevilla'"/>

    <xsl:template match="/">
        <precipitaciones>
            <xsl:for-each select="/precipitaciones/registro">
                <xsl:if test="lugar = $ciudad">
                    <registro>
                        <lugar><xsl:value-of select="lugar"/></lugar>
                        <fecha><xsl:value-of select="fecha"/></fecha>
                        <litros-m2><xsl:value-of select="litros-m2"/></litros-m2>
                    </registro>
                </xsl:if>
            </xsl:for-each>
        </precipitaciones>
    </xsl:template>
</xsl:stylesheet>