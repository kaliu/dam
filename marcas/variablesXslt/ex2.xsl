<?xml version="1.0" encoding="UTF-8"?>
<!-- Especificar una transformación que genere un documento XML a partir de la 
    entrada del enunciado en el que sólo se muestren las precipitaciones que superen 
    una cierta cantidad de litros -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="minLitros" select="'20'"/>

    <xsl:template match="/">
        <precipitaciones>
            <xsl:for-each select="/precipitaciones/registro">
                <xsl:if test="litros-m2 &gt; $minLitros">
                    <registro>
                        <lugar><xsl:value-of select="lugar"/></lugar>
                        <fecha><xsl:value-of select="fecha"/></fecha>
                        <litros-m2><xsl:value-of select="litros-m2"/></litros-m2>
                    </registro>
                </xsl:if>
            </xsl:for-each>
        </precipitaciones>
    </xsl:template>
</xsl:stylesheet>