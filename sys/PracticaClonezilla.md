# Clonezilla

## Muntatge i creació de directori al disc dur secundari

![Anotación 2020-04-19 180421](PracticaClonezilla.imgs/Anotación 2020-04-19 180421.png)

## Opcions de clonació  a disc

Totes les opcions fan referencia a on guardar la imatge clonada.

- **local_dev**: Utilitzar un dispositiu local
- **ssh_server**: Utilitzar un servidor SSH
- **samba_server**: Utilitzar un servidor Samba
- **nfs_server**: Utilitzar un servidor NFS
- **webdav_server**: Utilitzar el protocol WebDAV
- **s3_server**: Utilitzar un servidor Amazon AWS S3
- **enter_shell**: Entrar a la linea de comandes i fer-ho manualment
- **ram_disk**: Utilitzar la RAM
- **skip**: No muntar res





































## Informe final

Després de la creació de la imatge la restaurarem en una nova maquina virtual

![image-20200419185530008](PracticaClonezilla.imgs/image-20200419185530008.png)

Al finalitzar podrem veure com la nova maquina virtual es calcada a la original i comparteixen el mateix sistema de fitxers.

![image-20200419191128016](PracticaClonezilla.imgs/image-20200419191128016.png)