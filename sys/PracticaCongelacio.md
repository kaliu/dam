# 

# Congelació de Ubuntu

### Pregunta 1

`sudo sed -i "s/aufs=tmpfs$/aufs=tmpfs apparmor=0/" /etc/lethe/09_lethe /etc/grub.d/09_lethe`

Aquesta comanda simplement afegeix el text "apparmor=0" a les d'entrades de lethe al GRUB per deshabilitar que s'activi AppArmor al iniciar el sistema.

### Desinstal·lació i descongelació de Lethe

Buscant a els fòrums de ajuda de Lethe he trobat un link a un article de 2009 per desinstal·lar el programari. El problema amb aquesta article es que la web ja no existeix. Utilitzant [la maquina del temps d'Internernet](archive.org) he pogut accedir a [la entrada al blog](https://web.archive.org/web/20100114181207/http://sliceoflinux.com/2009/08/13/desinstala-lethe-y-descongela-tu-equipo/).

Primer de tot cal iniciar un LiveCD i modificar la configuració perquè es mostri el GRUB. Des d'aquí iniciarem a una instancia de la maquina que no esta congelada per poder desinstal·lar i descongelar el sistema.

Quan estem al menú del GRUB iniciarem el sistema amb l'entrada que no diu Lethe 

![image-20200422002209525](PracticaCongelacio.imgs/image-20200422002209525.png)

Al iniciar executem `sudo apt purge lethe` i el propi programa de desinstal·lació s'encarregarà de tornar el GRUB a la seva forma inicial.

![image-20200422002707607](PracticaCongelacio.imgs/image-20200422002707607.png)

![image-20200422003633353](PracticaCongelacio.imgs/image-20200422003633353.png)









### Instalació de Ofris

Primer tenim que accedir al [repositori de Ofris](https://sourceforge.net/projects/dafturnofris-id/files/development/dafturn-ofris-en/) i descarregar l'arxiu mes recent. El descomprimim i executem l'script que te al interior

![image-20200422115345105](PracticaCongelacio.imgs/image-20200422115345105.png)

Teòricament seleccionaríem la 3a opció per congelar el equip completament i la 4a en cas de voler-ho descongelar. Actualment, en versions noves de Ubuntu, el script no funciona per varies raons. La mes important es que es va fer abans de que SystemD esdevingués el programa de inici del sistema per defecte.





































# Congelació a Windows

## DeepFreeze

### Instal·lació

Instalarem DeepFreeze al sistema operatiu

![image-20200422122640692](PracticaCongelacio.imgs/image-20200422122640692.png)

### Establecer indicador de Clonar

L'opció serveix per poder implementar una imatge mestra i sols tenir que instal·lar aquesta a tots els equips. Això es molt útil a sector educatius o empresarials on vols que tots els equips tinguin la mateixa imatge congelada.

### ThawSpace

Si, funciona correctament

![image-20200422123934754](PracticaCongelacio.imgs/image-20200422123934754.png)









## Toolwiz Time Freeze

### Instal·lació

![image-20200422124509442](PracticaCongelacio.imgs/image-20200422124509442.png)

### Opcions de Time Freeze

![image-20200422125726636](PracticaCongelacio.imgs/image-20200422125726636.png)

En aquesta finestra ens dona varies opcions. La primera consisteix simplement en congelar o descongelar el sistema. La segona en fer que el sistema es congeli automàticament al iniciar el sistema. La tercera opció consisteix en seleccionar carpetes especifiques que no es congelaran.





### Activació de Time Freeze

Activem la congelació del sistema

![image-20200422130104757](PracticaCongelacio.imgs/image-20200422130104757.png)

Crearem una carpeta anomenada test i reiniciarem

![image-20200422130153539](PracticaCongelacio.imgs/image-20200422130153539.png)

Al tornar a iniciar sessió la carpeta ja no està

![image-20200422130405291](PracticaCongelacio.imgs/image-20200422130405291.png)





























### Exclusió de carpetes

Creem una carpeta i l'afegim a l'exclusió de Time Freeze

![image-20200422130507696](PracticaCongelacio.imgs/image-20200422130507696.png)

Iniciem la congelació

![image-20200422130558103](PracticaCongelacio.imgs/image-20200422130558103.png)

I ara creem un arxiu a la carpeta. Reiniciem l'equip

![image-20200422130623511](PracticaCongelacio.imgs/image-20200422130623511.png)

I l'arxiu de prova continua al seu lloc

![image-20200422130854991](PracticaCongelacio.imgs/image-20200422130854991.png)

### Descongelació del sistema

Per descongelar simplement desmarquem l'opció de iniciar la congelació amb el sistema i desactivem la congelació al panell de la aplicació.