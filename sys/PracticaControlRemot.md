[TOC]

# Control remot per SSH

## Instalació i configuració al servidor SSH

Per instal·lar els paquets necessaris executarem la seguen comanda:

`sudo apt install openssh-server`

![image-20200422185650339](PracticaControlRemot.imgs/image-20200422185650339.png)

El instal·lador ho posarà tot a punt per poder començar a utilitzar-ho però si volem configurar el servidor(canviar el port per on funciona ssh, etc.) ho podrem fer al arxiu `/etc/ssh/sshd_config`.

Per saber si el servei esta funcionant executarem la seguent comanda:

`sudo systemctl status sshd`

Si tot ha anat bé ens ha de tornar això:

![image-20200422190102097](PracticaControlRemot.imgs/image-20200422190102097.png)

Ara sols fa falta connectar-se des de un client.

## Connexió a un servidor SSH des de un equip Windows anterior a Windows 10

Per totes les versions anteriors a Windows 10 tenim que instal·lar el programari anomenat PuTTY des de el [següent enllaç](https://putty.org/). Aquest programari lliure i gratuït ens permetrà connectar-nos a el servidor SSH de manera fàcil.

Al finalitzar la instal·lació e iniciar el programa podrem veure la següent finestra:

![image-20200422192955686](PracticaControlRemot.imgs/image-20200422192955686.png)

Ara simplement posarem la IP del nostre servidor, ens assegurarem de que esta en mode SSH i apretarem a *open*.

![image-20200422193111563](PracticaControlRemot.imgs/image-20200422193111563.png)

Li diem que si volem afegir la clau del servidor

![image-20200422193137034](PracticaControlRemot.imgs/image-20200422193137034.png)

Introduïm usuari i contrasenya

![image-20200422193225124](PracticaControlRemot.imgs/image-20200422193225124.png)

I ja podem controlar remotament el nostre sistema Ubuntu.





































## Connexió a un servidor SSH des de un equip Windows 10

Amb Windows 10, després de la actualització de 2015, el client ssh ve integrat en el sistema així que ens estalviem alguns passos. Tenim que iniciar PowerShell i executar la seguent comanda:

`ssh USUARI@IP `

On USUARI és el usuari en el que ens volem loguejar en el servidor i IP  es la direcció del servidor. Si tot ha sortit bé iniciarem sessió al servidor:

![image-20200422193738538](PracticaControlRemot.imgs/image-20200422193738538.png)

## Connexió a un servidor SSH des de un equip GNU/Linux

Els equips GNU/Linux tenen millor integració amb aquest servei i no resulta estrany que estigui instal·lat per defecte. En tot cas en les distribucions basades en Ubuntu s'instal·la amb la comanda

`sudo apt install openssh-client`

El procediment es similar a el de Windows, iniciem una terminal i executem

`ssh USUARI@IP `

On USUARI és el usuari en el que ens volem loguejar en el servidor i IP  es la direcció del servidor. Si tot ha sortit bé iniciarem sessió al servidor:

![image-20200422194541479](PracticaControlRemot.imgs/image-20200422194541479.png)

Acceptem la clau del servidor i ja estarem dins.









































# Control remot per TeamViewer

En cas de TeamViewer el procés es pràcticament el mateix per a tots els sistemes operatius així que en el meu cas utilitzaré una maquina Ubuntu per accedir remotament a un equip Windows 7.

## Instalació de TeamViewer a Ubuntu i Windows

### Ubuntu

Primer de tot accedirem a la [pàgina web de TeamViewer](https://www.teamviewer.com/es/descarga/linux/) i descarregarem la versió per a Ubuntu/Debian.

Quan finalitzi la descarrega obrirem una terminal i ens mourem fins la carpeta de Descarregues. Allà executarem la següent comanda per instal·lar el paquet:

`sudo apt install ./teamviewer_15.4.4445_amd64.deb` (substituir el nom del paquet per el descarregat)

![image-20200422195454927](PracticaControlRemot.imgs/image-20200422195454927.png)

Acceptem i continuem amb la instal·lació. Quan finalitzi iniciem el programa.































Acceptarem les condicions d'us

![image-20200422200108281](PracticaControlRemot.imgs/image-20200422200108281.png)

I tan senzill com això estarà instal·lat

![image-20200422200142076](PracticaControlRemot.imgs/image-20200422200142076.png)











### Windows

A Windows la instal·lació es un procés pràcticament anecdòtic. Accedim a la [pàgina de descarrega de TeamViewer](https://www.teamviewer.com/es/descarga-automatica-de-teamviewer/) i executem l'instal·lador.

![image-20200422200601100](PracticaControlRemot.imgs/image-20200422200601100.png)

Seleccionem la opció de *Default Installation* i que es per us Personal. El progrmari s'instalara com cualsevol altre. Al finalitzar l'executarem

![image-20200422200719794](PracticaControlRemot.imgs/image-20200422200719794.png)



## Connexió a TeamViewer 

La connexió es tan senzilla com posar la ID de un equip en l'altre i introduïr la contrasenya

![image-20200422200858370](PracticaControlRemot.imgs/image-20200422200858370.png)

I ja podrem controlar el nostre equip de forma remota

![image-20200422203837916](PracticaControlRemot.imgs/image-20200422203837916.png)

# Control remot per Remote Desktop de Windows

Remote Desktop es el programari i protocol (RDP) de Microsoft per al control remot dels equips. Tot i que se suposa que sols un equip Windows pot fer de servidor podem controlar-ho a través d'un equip Ubuntu com es el cas que mostraré.

## Configuració de Remote Desktop al servidor (Windows 7)

Per permetre que equips externs controlin el nostre equip tindrem que configurar Windows 7 de la seguent manera:

Premerem botó dret sobre *El Meu Equip* i entrarem a Propietats.

 ![image-20200422201625715](PracticaControlRemot.imgs/image-20200422201625715.png)

Des d'aquí entrarem a la Configuració avançada del sistema ![image-20200422201712449](PracticaControlRemot.imgs/image-20200422201712449.png)

I entrarem a la pestanya de Remot

![image-20200422201915036](PracticaControlRemot.imgs/image-20200422201915036.png)

A questa pestanya canviarem la configuració a la segona opció.

## Configuració del client Ubuntu

Instal·larem el client Vinagre a Ubuntu amb la següent comanda

`sudo apt install vinagre `

![image-20200422202153685](PracticaControlRemot.imgs/image-20200422202153685.png)

Al iniciar el programa premerem a connectar i ens sortirà una finestra. En aquesta finestra seleccionarem le protocol RDP com es mostra en la següent imatge:

![image-20200422202412606](PracticaControlRemot.imgs/image-20200422202412606.png)

Introduirem la direcció IP de la maquina Windows i acceptem el certificat

![image-20200422202539626](PracticaControlRemot.imgs/image-20200422202539626.png)

I introduïm les credencials del usuari amb el que ens volem connectar. Una particularitat es que te suport per connectar-se amb usuaris del domini on esta el sistema Windows.

![image-20200422202642466](PracticaControlRemot.imgs/image-20200422202642466.png)

I ja podem controlar remotament el equip

![image-20200422202738349](PracticaControlRemot.imgs/image-20200422202738349.png)

Una particularitat de RDP es que al sistema host la sessió es tanca completament. Es més que un servei de control remot, un servei per iniciar sessió en un altre equip remotament i això permet iniciar sessió com Administrador i no amb l'usuari que l'esta utilitzant si aquest no te suficients permisos.