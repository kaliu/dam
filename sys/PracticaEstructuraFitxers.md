Amb Ubuntu Server ja instal·lat començo a fer els exercicis.

## 1.

![image-20191202163415963](C:\Users\Roger\AppData\Roaming\Typora\typora-user-images\image-20191202163415963.png)

## 2.

### a)

##### Windows

Ruta relativa a ex1.c: 

​    exercicis\ex1.c*

Ruta relativa a punters.odt:

​     *..\documents\programacio\punters\punter.odt*

Ruta absoluta a ex1.c: 

​    *C:\Documents and settings\roger\Mis Documentos\progames\exercicis\ex1.c*

Ruta absoluta a punters.odt:

​    *C:\Documents and settings\roger\Mis Documentos\documents\programacio\             punters\punter.odt*

##### Linux

Ruta relativa a ex1.c: 

​    *exercicis/ex1.c*

Ruta relativa a punters.odt: 

​    ../documents/programacio/punters/punter.odt

Ruta absoluta a ex1.c:

​    /home/roger/programes/exercicis/ex1.c

Ruta absoluta a punters.odt:

​    /home/roger/documents/programacio/punters/punter.odt

### b)

Les rutes que contesto estaran en format Linux.

a) *apunts1.odt*

b) *../tema2/apunts2.odt*

c) *../../programacio/funcions/funcions.odt*

d) *../../temari.odt*

## 3.

a)

![image-20191204193447878](PracticaEstructuraFitxers.imgs/image-20191204193447878.png)

b)![image-20191204193616237](PracticaEstructuraFitxers.imgs/image-20191204193616237.png)

c)

![image-20191204193750871](PracticaEstructuraFitxers.imgs/image-20191204193750871.png)

d)

![image-20191204193823597](PracticaEstructuraFitxers.imgs/image-20191204193823597.png)

e)

![image-20191204193914946](PracticaEstructuraFitxers.imgs/image-20191204193914946.png)

f)

![image-20191204194054586](PracticaEstructuraFitxers.imgs/image-20191204194054586.png)

g) 

![image-20191204194544430](PracticaEstructuraFitxers.imgs/image-20191204194544430.png)

h) 

![image-20191204194734609](PracticaEstructuraFitxers.imgs/image-20191204194734609.png)

i)

![image-20191204194843021](PracticaEstructuraFitxers.imgs/image-20191204194843021.png)

j)

![image-20191204194933955](PracticaEstructuraFitxers.imgs/image-20191204194933955.png)

k)

<img src="PracticaEstructuraFitxers.imgs/image-20191204195017298.png" alt="image-20191204195017298"  />

l)

<img src="PracticaEstructuraFitxers.imgs/image-20191204195248002.png" alt="image-20191204195248002"  />

m)

![image-20191204195436433](PracticaEstructuraFitxers.imgs/image-20191204195436433.png)

## 4.

### a)

| Directori actual | Comanda             | Directori final (ruta absoluta)        |
| ---------------- | ------------------- | -------------------------------------- |
| /                | cd home/usuariA     | /home/usuariA                          |
| /home            | cd /usuariB/dirBB   | /home/usuariB/dirBB                    |
| /home/usuariA    | cd ../../boot/grub/ | /boot/grub                             |
| /boot/grub       | cd ../home/usuariB  | /boot/home/usuariB **(és incorrecte)** |
| /boot            | cd ../home/dirBB    | /home/dirBB **(és incorrecte)**        |
| /                | cd /boot/grub       | /boot/grub                             |

### b)

| Directori actual (abs) | Directori destí (abs) | Comanda                |
| ---------------------- | --------------------- | ---------------------- |
| /home/usuariB/dirBB    | /home/usuariB/        | cd ..                  |
| /home/usuariB/dirBB    | /home/usuariA         | cd ../../usuariA       |
| /home/usuariB/dirBB    | /boot/grub            | cd /boot/grub          |
| /home/usuariA          | /home/usuariB/dirBB   | cd ../usuariB/dirBB    |
| /home/usuariA          | /                     | cd /                   |
| /boot/grub             | /home/usuariB/dirBB   | cd /home/usuariB/dirBB |
