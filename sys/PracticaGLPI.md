# Instal·lació i configuració del GLPI

## Descarrega i descompressió

![Anotación 2020-04-19 164928](PracticaGLPI.imgs/Anotación 2020-04-19 164928.png)

## Catalogació d'inventari

![Anotación 2020-04-19 172512](PracticaGLPI.imgs/Anotación 2020-04-19 172512.png)













## Creació d'incidència

![Anotación 2020-04-19 172813](PracticaGLPI.imgs/Anotación 2020-04-19 172813.png)

## Gestió de contactes

![Anotación 2020-04-19 173040](PracticaGLPI.imgs/Anotación 2020-04-19 173040.png)

## Informe final

Al acabar la practica tenim un sistema per poder monitoritzar i tindre classificats tan l'inventari d'aparells electrònics, fer seguiment de les incidències i moltes mes funcions. Un programari molt interessant per aplicacions de *Help Desk* en el àmbit empresarial.

