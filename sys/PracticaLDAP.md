[TOC]



# Practica LDAP

## Part Obligatòria

### Configuració del nom de servidor

![Anotación 2020-04-01 124240](PracticaLDAP.imgs/Anotación 2020-04-01 124240.png)















### Estructura d'usuaris a LDAP

![Anotación 2020-04-01 185924](PracticaLDAP.imgs/Anotación 2020-04-01 185924.png)

[Arxiu LDIF](rgras.ldif) (també inclòs adjunta amb la practica)

### Importació d'un arxiu extern

![Anotación 2020-04-01 191025](PracticaLDAP.imgs/Anotación 2020-04-01 191025-1585763168553.png)

### Configuració de la restricció d'usuaris al servidor Apache2

![Anotación 2020-04-01 192907](PracticaLDAP.imgs/Anotación 2020-04-01 192907.png)

![Anotación 2020-04-01 192933](PracticaLDAP.imgs/Anotación 2020-04-01 192933.png)

## Part opcional

Simplement seleccionem la OU de sanitat en comptes de la de Informàtica:

![Anotación 2020-04-01 193634](PracticaLDAP.imgs/Anotación 2020-04-01 193634-1585763441467.png)