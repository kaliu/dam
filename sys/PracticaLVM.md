# LVM i GParted

## Introducció

### Part 1. Snapshot i creació de discs

Creem una snapshot per si ens equivoquem en algun pas:

![image-20200130200159442](PracticaLVM.imgs/image-20200130200159442.png)

I ara crearem dos discs durs més a la màquina virtual:

![Captura de pantalla de 2020-01-30 20-05-41](PracticaLVM.imgs/Captura%20de%20pantalla%20de%202020-01-30%2020-05-41.png)















### Part 2. GParted

Arrenquem la la iso de GPARTED que ens portarà a aquest menú:

![image-20200130201141130](PracticaLVM.imgs/image-20200130201141130.png)

Seleccionem la primera opció i presionem ENTER

Al iniciar el sistema ens pregunta quin tipus de teclat tenim. Seleccionem la primera opció

![image-20200130201451217](PracticaLVM.imgs/image-20200130201451217.png)

Ens preguntará la disposició de les tecles. Seleccionem qwerty

![image-20200130201738350](PracticaLVM.imgs/image-20200130201738350.png)

I seleccionem la dispocició espanyola:

 ![image-20200130201857723](PracticaLVM.imgs/image-20200130201857723.png)

Al configurar aixó acabarem a una terminal que ens preguntará si volem continuar a la interficie grafica o fer altres accions. Elegirem la opció 0 per entrar al sistema gràfic.

![image-20200130202142587](PracticaLVM.imgs/image-20200130202142587.png)







## GParted

### Exercici 1

###### Crear particions primàries

Per crear 3 particions primer necesitem crear una taula de particions. Ho farem de la següent forma:

![image-20200226200441655](PracticaLVM.imgs/image-20200226200441655.png)

![image-20200226200505301](PracticaLVM.imgs/image-20200226200505301.png)

Creem una taula de tipus msdos i li donem a *"Apply"*

Ara, crearem les particions. Premerem la primera icona desde l'esquerra, la que te forma de fulla. Això ens protarà a una finestra com aquesta:![image-20200226200843004](PracticaLVM.imgs/image-20200226200843004.png)

Li donarem 1/3 de la mida del disc i, per les dos primeres, el sistema de fitxers es **ext4**

![image-20200226201143273](PracticaLVM.imgs/image-20200226201143273.png)

![image-20200226201223471](PracticaLVM.imgs/image-20200226201223471.png)

I, la tercera, en format NTFS

![image-20200226201310314](PracticaLVM.imgs/image-20200226201310314.png)

I ens quedarà algo com el següent:

![image-20200226201352937](PracticaLVM.imgs/image-20200226201352937.png)

###### Aplicar canvis

Per aplicar els canvis apretarem la icona del "tick" a la part superior

![hehe](PracticaLVM.imgs/image-20200226201457591.png)

Li donem a "Apply"

![image-20200226201523294](PracticaLVM.imgs/image-20200226201523294.png)

I esperem a que el programa acabi de fer les operacions pertinents.

### Exercici 2

###### Redimensionar una partició

Per redimensionar una partició necessitem premer botó dret a la partició i seleccionar l'opció de Redimensionar

![image-20200226202127906](PracticaLVM.imgs/image-20200226202127906.png)

En el nostre cas la redimensionarem de 10GB a 1GB

![image-20200226202338973](PracticaLVM.imgs/image-20200226202338973.png)

Apliquem els canvis [com s'ha mostrat anteriorment](#Aplicar canvis).

###### Crear particions lógiques

Ara crearem una partició ampliada per poder afegir 3 particions logiques més. Això no seria posible si volguessim afegir un altra particio primaria per les [limitacions de MBR](https://www.howtogeek.com/193669/whats-the-difference-between-gpt-and-mbr-when-partitioning-a-drive/).

Per crear una partició ampliada el procés es molt semblant a [crear una partició primaria](#Crear particions primàries) peró desplegarem el menú "Crear com a:" i seleccionarem *"Partició ampliada"* i ocuparem la resta del disc:

![image-20200226203511871](PracticaLVM.imgs/image-20200226203511871.png)

Aquesta no te sistema de arxius perque sols serveix per contenir particions lógiques. Afegim la partició ampliada i [apliquem els canvis](#Aplicar canvis).

Ara es tan senzill com crear una partició com hauriem de fer normalment però dins de la partició ampliada. AIxò es fa senzillament apretant la icona de crear mentres tenim seleccionada la partició ampliada.

![image-20200226203951464](PracticaLVM.imgs/image-20200226203951464.png)

I exactament igual amb les 3 següents.

![image-20200226204317777](PracticaLVM.imgs/image-20200226204317777.png)

![image-20200226204351759](PracticaLVM.imgs/image-20200226204351759.png)

[Apliquem els canvis](#Aplicar canvis)

### Exercici 3

Crearem 3 particions primàries exactament iguals que [al primer exercici](#Exercici 1) però aquest cop al segon disc.

























### Exercici 4

Per fusionar les particions eliminant el contingut de la segona simplement esborrarem la segona partició i expandirem la primera amb l'espai lliure restant.

Això és fa simplement prement botó dret a la partició que volem esborrar

![image-20200304185009431](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304185009431.png)

I, després, redimensionem la primera partició per ocupar aquest espai

![image-20200304185238026](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304185238026.png)













### Exercici 5

Sortim de GParted Live i tornem a la instalació de Ubuntu Server.

Executem `ls /dev | grep sd` per veure tots els discs i les seves particions

![image-20200304190116755](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304190116755.png) 

També podrem utilitzar la comanda `sudo fdisk -l | grep sd`

![image-20200304190512221](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304190512221.png)

Podem observar que cada disc té un titol que es compon de la direcció del disc i la seva mida en GigaBytes, Bytes i Sectors.

Les líneas que segueixen son les particions. Aquesta taula té 5 columnes:

- La direcció de la partició
- El sector on comença la partició 
- El sector on acaba
- El número de sectors totals que té
- La mida de la partició en GB
- El tipus númeric de partició
- El tipus de la partició















## LVM

### Exercici 1

Per tenir un grup de volums primer necessitem crear un *Physical Volume* per cada partició. Aixó ho farem amb la següent comanda

`sudo pvcreate /dev/sdXY`

On X és la lletra del disc i Y és el número de la partició. Exemple:

![image-20200304192319452](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304192319452.png)

### Exercici 2

Crearem el grup de volums amb la comanda `vgcreate` seguit del nom de el grup que volem crear i totes les particions que en formen part.

![image-20200304192759405](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304192759405.png)

![image-20200304192915817](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304192915817.png)

### Exercici 3

Ara podem dividir aquests grups en *Logical volumes*. Per fer-ho utilitzarem la comanda `lvcreate`. Per exemple, per crear el volum logic lv_facturacion utilitzarem la comanda:

![image-20200304193238847](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304193238847.png)

I per ocupar la resta amb el volum lv_contabilidad:

![image-20200304193426373](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304193426373.png)



























Podem veure els 4 volums lógics amb la comanda `lvdisplay`.

![image-20200304193906991](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304193906991.png)

![image-20200304193925777](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304193925777.png)

### Exercici 4

Li donem format als *Logical Volume*

![image-20200304194209411](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194209411.png)

![image-20200304194227455](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194227455.png)

![image-20200304194248489](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194248489.png)

![image-20200304194309106](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194309106.png)

### Exercici 5

Creem les carpetes a la nostra home

![image-20200304194447305](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194447305.png)

Muntem les particions a la seves corresponents carpetes

![image-20200304194737957](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194737957.png)![image-20200304194844507](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194844507.png)![image-20200304194912546](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194912546.png)![image-20200304194943767](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304194943767.png)

### Exercici 6

Editem el fitxer `/etc/fstab` per que els volums s'automuntin al iniciar el sistema.

![image-20200304200008201](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304200008201.png)

### Exercici 7

Per acabar de comprobar que tot està correcte executarem les següents comandes

Comprobem els *Physycal Volumes*

![image-20200304200339925](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304200339925.png)

Els grups de volums

![image-20200304200443656](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304200443656.png)

















I la divisió d'aquests

![image-20200304200522275](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304200522275.png)









































Per acabar de estar segurs executarem les següents comandes:

![image-20200304200743956](/home/roger/git/dam/sys/PracticaLVM.imgs/image-20200304200743956.png)

Si tot sembla correcte haurem finalitzat la nostra creació de volums LVM.