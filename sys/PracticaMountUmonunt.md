# Practica: Monunt/Umount de Roger Gras

[TOC]

## Exercici 1

#### Pas 1.

- Disc dur connectat al canal primari IDE (master): /dev/hda
  - Partició primària: /dev/hda1
  - Partició estesa: /dev/hda2
    - Partició lògica 1: /dev/hda5
    - Partició lògica 2: /dev/hda6
    - Partició lògica 3: /dev/hda7
    - Partició lògica 4: /dev/hda8

- Disc dur connectat al canal secundari IDE (slave): /dev/hdd
  - Partició primària 1: /dev/hdd1
  - Partició primària 2: /dev/hdd2
  - Partició estesa: /dev/hdd3
    - Partició lògica 1: /dev/hda5
    - Partició lògica 2: /dev/hda6
    - Partició lògica 3: /dev/hda7

- Pendrive: /dev/sda
  
  - Partició primària 1: /dev/sda1
  - Partició primària 2: /dev/sda2

#### Pas 2.

Al fitxer /etc/fstab:

`/dev/sda /mnt/flash auto auto,ro 0 0  `

#### Pas 3.

Executem la següent comanda

`sudo umount /mnt/flash`

#### Pas 4.

Al fitxer /etc/fstab:

`/dev/fd0 /mnt/floppy fat32 auto,noexec 0 0`

#### Pas 5.

Executem:

`sudo umount /mnt/floppy`

#### Pas 6.

`sudo cp /etc/fstab /etc/fstab.bak`

#### Pas 7.

Al fitxer /etc/fstab:

`/dev/sda /mnt/flash auto auto,user,ro 0 0  `

#### Pas 8.

Aquest pas es pràctic així que no el puc dur a terme.

#### Pas 9.

`sudo rm /etc/fstab && sudo mv /etc/fstab.bak /etc/fstab`

## Exercici 2

Introduirem el disc de les *Guest Additions* prement al botó que surt al desplegar el menú Dispositius

![image-20191205200207159](PracticaMountUmonunt.imgs/image-20191205200207159.png)

I el muntem amb la seguent comanda:

![image-20191205200835587](PracticaMountUmonunt.imgs/image-20191205200835587.png)

Al ser un CDROM de sols lectura, ens avisa que no el pot muntar en read-write.

Abans d'instal·lar-ho necessitarem instal·lar les dependències. Ho farem amb la següent comanda:

![image-20191205201100076](PracticaMountUmonunt.imgs/image-20191205201100076.png)

Fet això ens mourem a la carpeta on hem muntat el disc

![image-20191205201243799](PracticaMountUmonunt.imgs/image-20191205201243799.png)

I executarem l'instal·lador

![image-20191205201322031](PracticaMountUmonunt.imgs/image-20191205201322031.png)

I reiniciem:

![image-20191205201646675](PracticaMountUmonunt.imgs/image-20191205201646675.png)

## Exercici 3

Obrim de nou el desplegable *Dispositivos* i accedim a *Preferencias USB*

![image-20191205202503720](PracticaMountUmonunt.imgs/image-20191205202503720.png)

Premem al botó d'afegir i afegim l'usb que volem

![image-20191205202605100](PracticaMountUmonunt.imgs/image-20191205202605100.png)

Per comprovar que s'ha reconegut correctament executarem `lsblk`

![image-20191205202725660](PracticaMountUmonunt.imgs/image-20191205202725660.png)

Creem la carpeta i el muntem

![image-20191205203325698](PracticaMountUmonunt.imgs/image-20191205203325698.png)

a) `sudo mount -ro /dev/sdb1 /media/usb`

b) `sudo umount /media/usb`

## Exercici 4

Per contactar-nos a la nostra maquina virtual de forma remota necessitem redireccionar el port de ssh. Per això seguirem els següents passos:

Anem a la configuració de xarxa de la maquina i ens assegurem de que esta en mode NAT. Si ho està, despleguem el menú "Avanzadas" y obrim la finestra de redirecció de ports.

![image-20191216152934569](PracticaMountUmonunt.imgs/image-20191216152934569.png)

Afegim la IP de la màquina amfitrió i la de la virtual, en ambdós casos el port es el 22(el port de ssh). Guardem la regla i sortim.

Ara simplement accedim a PuTTY a la nostra maquina real i introduïm la nostra pròpia IP.

<img src="PracticaMountUmonunt.imgs/image-20191216154018443.png" alt="image-20191216154018443" style="zoom: 80%;" />

(No oblidem que necessitem tenir un servidor ssh instal·lat al Ubuntu Server, aquest s'instal·la amb la comanda `sudo apt install ssh`)

Ens preguntarà si volem guardar la clau a la caché. Li indiquem que si

![image-20191216154131982](PracticaMountUmonunt.imgs/image-20191216154131982.png)

I iniciem sessió com si fos una terminal normal a la nostra maquina virtual. Això por permetre, entre altres usos, fer un inici desacoblat de la maquina virtual i controlar-la simplement per la connexió ssh.

![image-20191216154259277](PracticaMountUmonunt.imgs/image-20191216154259277.png)

## Exercici 5

#### a)

​    CDROM:

![image-20191216155343167](PracticaMountUmonunt.imgs/image-20191216155343167.png)

​    Disc dur principal:

<img src="PracticaMountUmonunt.imgs/image-20191216155115152.png" alt="image-20191216155115152"  />

#### b)

![image-20191216155730920](PracticaMountUmonunt.imgs/image-20191216155730920.png)

El meu fstab té 2 files i cada fila compta amb 6 columnes.

**A la primera línia:**

- La part de color verd és la identificació única de la partició del disc dur a muntar
- La part blava es el punt de muntatge de la partició (en aquest cas `/`)
- La part rosa es el sistema de fitxers (en aquest cas es `ext4`)
- La part taronja es les opcions que s'utilitzaran en el muntatge (en aquest cas les per defecte)
- Els dos números finals representen en aquest ordre:
  - La freqüència de el backup de dades (en aquest cas 0, que significa mai)
  - El ordre en que el sistema revisarà la partició (en aquest cas 0, que significa mai)

**A la segona línia:**

- La part de color verd és la direcció on es troba la swap (en aquest cas es un arxiu al directori arrel)
- La part blava es el punt de muntatge¡(en aquest cas ningun ja que no es una partició)
- La part rosa es el sistema de fitxers (en aquest cas es `swap`)
- La part taronja es les opcions que s'utilitzaran en el muntatge (en aquest cas el `sw` fa referencia a que es muntada automàticament per el sistema)
- Els dos números finals representen en aquest ordre:
  - La freqüència de el backup de dades (en aquest cas 0, que significa mai)
  - El ordre en que el sistema revisarà (en aquest cas 0, que significa mai)

#### c)

Es tan senzill com muntar la partició que conté la / a una carpeta amb la següent comanda:

![image-20191216161214412](PracticaMountUmonunt.imgs/image-20191216161214412.png)

#### d)

El fitxer /etc/mtab es el fitxer on es guarden totes les particions muntades amb la seva configuració. La sortida de la comanda `mount` printa, a part d'altres, aquest arxiu. Els paràmetres son iguals que al fstab.

![image-20191216161622208](PracticaMountUmonunt.imgs/image-20191216161622208.png)

#### e)

Afegirem la seguent linea al fstab:

![image-20191216161806022](PracticaMountUmonunt.imgs/image-20191216161806022.png)
