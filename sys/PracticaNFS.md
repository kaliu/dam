## Compartició de carpetes NFS

Com podem veure s'està compartint la carpeta */home/compartida* al  meu servidor mitjançant NFS

![Anotación 2020-04-02 175832](PracticaNFS.imgs/Anotación 2020-04-02 175832.png)

I si creem un arxiu al client apareix al servidor

![Anotación 2020-04-02 180048](PracticaNFS.imgs/Anotación 2020-04-02 180048.png)

![Anotación 2020-04-02 180106](PracticaNFS.imgs/Anotación 2020-04-02 180106.png)

## Creació de carpetes compartides

![Anotación 2020-04-02 181023](PracticaNFS.imgs/Anotación 2020-04-02 181023.png)![Anotación 2020-04-02 181112](PracticaNFS.imgs/Anotación 2020-04-02 181112.png)

## Permisos de carpetes en NFS

#### Usuaris del Server

| Nom   | UID  | GID  |
| ----- | ---- | ---- |
| root  | 0    | 0    |
| roger | 1000 | 1000 |
| marc  | 1001 | 1001 |
| marta | 1002 | 1002 |

#### Usuaris del Client

| Nom     | UID  | GID  |
| ------- | ---- | ---- |
| root    | 0    | 0    |
| monica  | 1001 | 1001 |
| ernesto | 1002 | 1002 |

L'usuari propietari de la carpeta *prova* és roger(UID 1000)

![Anotación 2020-04-02 183920](PracticaNFS.imgs/Anotación 2020-04-02 183920.png)

Si accedeixo amb un UID igual al del propietari si que puc entrar, crear i modificar arxius.

Amb l'usuari mònica puc entrar al directori *arrel* però no puc ni entrar ni veure el contingut del directori prova

![image-20200402184144373](PracticaNFS.imgs/image-20200402184144373.png)

### Accés a les dades amb root

De normal no es pot accedir al directori prova si no sóc el usuari roger

![image-20200402185130375](PracticaNFS.imgs/image-20200402185130375.png)

























Ara, amb l'opció *no_root_squash*, puc accedir a la carpeta correctament

![image-20200402185910658](PracticaNFS.imgs/image-20200402185910658.png)

![image-20200402184650432](PracticaNFS.imgs/image-20200402184650432.png)