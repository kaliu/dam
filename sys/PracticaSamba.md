# Samba

[TOC]

## Ubuntu Server com a servidor Samba

Primer de tot, instal·lem el paquet de samba

![image-20200409114736621](PracticaSamba.imgs/image-20200409114736621.png)

Creem la carpeta a compartir i li assignem els permisos pertinents

![image-20200409115048665](PracticaSamba.imgs/image-20200409115048665.png)

Afegim la carpeta a la configuració de Samba de la seguent manera a */etc/samba/smb.conf*

![image-20200409115430459](PracticaSamba.imgs/image-20200409115430459.png)

I a continuació simplement reiniciem el servei

![image-20200409115904586](PracticaSamba.imgs/image-20200409115904586.png)

Ara, a la maquina de Windows 7, em surt correctament la carpeta compartida

![image-20200409120124891](PracticaSamba.imgs/image-20200409120124891.png)

## Windows 7 com a servidor Samba

Compartim la carpeta com ja sabem a Windows 7

![image-20200409121050065](PracticaSamba.imgs/image-20200409121050065.png)

Instal·lem el següent paquet al client Ubuntu

![image-20200409125518323](PracticaSamba.imgs/image-20200409125518323.png)

I muntem la carpeta remota

![image-20200409125929090](PracticaSamba.imgs/image-20200409125929090.png)

![image-20200409130121212](PracticaSamba.imgs/image-20200409130121212.png)

![image-20200409130133414](PracticaSamba.imgs/image-20200409130133414.png)

