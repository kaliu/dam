# Practica solució de problemes

Per Roger Gras.

[TOC]



## Connexió a internet

El navegador Firefox ja ve instal·lat per defecte i si necessitem un client de correu també Ubuntu ve amb Thunderbird. El problema que he tingut amb el sistema es que no tenia connexió a internet a causa de que la interfície no te direcció IP.

![image-20200526122216086](PracticaSolucióProbemes.imgs/image-20200526122216086.png)

La solució es bastant senzilla: editem el arxiu */etc/netplan/01-network-manager-all.yaml* i afegim les següents línies

![image-20200526155432626](PracticaSolucióProbemes.imgs/image-20200526155432626.png)







- La línia de color rosa especifica quin tipus de targeta de xarxa és. En aquest cas, una Ethernet.
- La línia de color verda especifica el nom de la interfície. En aquest cas *enp0s3*.
- La línia de color groga especifica com volem establir o obtenir la IP. En el nostre cas la volem obtenir utilitzant DHCP i així ho posem al arxiu.

Executem la següent comanda per aplicar els canvis:

![image-20200526155915056](PracticaSolucióProbemes.imgs/image-20200526155915056.png)

I ja tenim connexió a Internet

![image-20200526135016966](PracticaSolucióProbemes.imgs/image-20200526135016966.png)



## Instal·lació de suite ofimàtica

Per instal·lar la suite ofimatica LibreOffice necessitem utilitzar el gestor de paquets [Snap](https://snapcraft.io/) ([més informació](https://blogubuntu.com/que-es-ubuntu-snap)) ja que no es troba als repositoris oficials. Aixó es fa d'una forma bastant similar a apt com podem veure

![image-20200526135402328](PracticaSolucióProbemes.imgs/image-20200526135402328.png)

![image-20200526135630132](PracticaSolucióProbemes.imgs/image-20200526135630132.png)

Al finalitzar la instal·lació podem executar el programa com qualsevol altre.

<img src="PracticaSolucióProbemes.imgs/image-20200526135710101.png" alt="image-20200526135710101" style="zoom: 33%;" />



## Impressora PDF

Per últim, el usuari necessita imprimir a PDF els arxius que escriu. Per alguna raó a la meva instal·lació no troba el paquet *cups-pdf* ni *printer-driver-cups-pdf* així que he tingut que [buscar el paquet](https://pkgs.org/download/cups-pdf) i [descarregar directament el .deb](https://ubuntu.pkgs.org/18.04/ubuntu-universe-amd64/printer-driver-cups-pdf_3.0.1-5_amd64.deb.html) a la maquina amb wget

![image-20200526153750402](PracticaSolucióProbemes.imgs/image-20200526153750402.png)

Quant ja hem descarregar el paquet utilitzem dpkg per instal·lar-ho:

![image-20200526153803887](PracticaSolucióProbemes.imgs/image-20200526153803887.png)

Si la instal·lació ha sigut satisfactòria podrem seleccionar PDF al apartat de impressores:

<img src="PracticaSolucióProbemes.imgs/image-20200526153931500.png" alt="image-20200526153931500" style="zoom: 50%;" />