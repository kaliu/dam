# Practica Ubuntu Server

[TOC]

# 2.

## 2.1

La meva direcció IP és 10.0.2.15

![image-20191202160645007](/home/roger/dam/sys/imgs/690a14c22241f7ea1af8dea9cd43dd3c337fecb1.png)

## 2.2

Si, tinc connexió a internet

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 19-27-58.png)

## 2.3

Si, es capaç de resoldre noms. En aquest cas ha resolt que *inslaferreria.cat* és *91.142.215.126*

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 19-29-09.png)

## 2.4

Per canviar la configuració de xarxa necessitem configurar Netplan. Per això utilitzarem nano i accedirem a l'arxiu */etc/netplan/ARXIU-CONFIG* 

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 20-13-26.png)

![image-20191216163624681](PracticaUbuntuServer.imgs/image-20191216163624681.png)

## 2.5

A la configuració de Xarxa de la maquina virtual afegim la targeta de xarxa

![image-20191216163145726](PracticaUbuntuServer.imgs/image-20191216163145726.png)

![image-20191216164200775](PracticaUbuntuServer.imgs/image-20191216164200775.png)

El nom de la tarja de xarxa és `enp0s8`

## 2.6

![image-20191216164649560](PracticaUbuntuServer.imgs/image-20191216164649560.png)

![image-20191216164726070](PracticaUbuntuServer.imgs/image-20191216164726070.png)

## 2.7

![image-20191216164825202](PracticaUbuntuServer.imgs/image-20191216164825202.png)

# 3.

## 3.1

![image-20191216165214673](PracticaUbuntuServer.imgs/image-20191216165214673.png)

## 3.2

![image-20191216165248314](PracticaUbuntuServer.imgs/image-20191216165248314.png)

## 3.3

![image-20191216165323739](PracticaUbuntuServer.imgs/image-20191216165323739.png)

## 3.4

![image-20191216165612226](PracticaUbuntuServer.imgs/image-20191216165612226.png)

## 3.5

![image-20191216165703172](PracticaUbuntuServer.imgs/image-20191216165703172.png)

![image-20191216165740939](PracticaUbuntuServer.imgs/image-20191216165740939.png)

## 3.6

![image-20191216165840060](PracticaUbuntuServer.imgs/image-20191216165840060.png)

## 3.7

![image-20191216165917312](PracticaUbuntuServer.imgs/image-20191216165917312.png)

## 3.8

![image-20191218191832740](PracticaUbuntuServer.imgs/image-20191218191832740.png)

## 3.9

![image-20191216170323310](PracticaUbuntuServer.imgs/image-20191216170323310.png)

## 3.10

![image-20191218191932715](PracticaUbuntuServer.imgs/image-20191218191932715.png)

## 3.11

![image-20191218192020622](PracticaUbuntuServer.imgs/image-20191218192020622.png)

# 4.

## 4.1

![image-20191218193118732](PracticaUbuntuServer.imgs/image-20191218193118732.png)

## 4.2

![image-20191218193212059](PracticaUbuntuServer.imgs/image-20191218193212059.png)

## 4.3

<img src="PracticaUbuntuServer.imgs/image-20191218193242799.png" alt="image-20191218193242799"  />

Els fitxers ocults no es mostren de forma normal. Es diferencien de la resta perquè comencen per un punt(.).

## 4.4

![image-20191218193502153](PracticaUbuntuServer.imgs/image-20191218193502153.png)

L'executable es propietat de l'usuari root. Pot llegir-ho qualsevol i sols escriure el propietari. Tots els usuaris poden executar-ho.

## 4.5

![image-20191218193827780](PracticaUbuntuServer.imgs/image-20191218193827780.png)

L'executable es troba a `/usr/bin/` i el contingut de la carpeta és

![image-20191218193955374](PracticaUbuntuServer.imgs/image-20191218193955374.png)

## 4.6

![image-20191218194158912](PracticaUbuntuServer.imgs/image-20191218194158912.png)

## 4.7

![image-20191218194403098](PracticaUbuntuServer.imgs/image-20191218194403098.png)

# 5.

## 5.1

![image-20191218194658272](PracticaUbuntuServer.imgs/image-20191218194658272.png)

## 5.2

![image-20191218194842144](PracticaUbuntuServer.imgs/image-20191218194842144.png)

## 5.3

<img src="PracticaUbuntuServer.imgs/image-20191218194933916.png" alt="image-20191218194933916"  />

## 5.4

![image-20191218195129929](PracticaUbuntuServer.imgs/image-20191218195129929.png)

## 5.5

![image-20191218195712532](PracticaUbuntuServer.imgs/image-20191218195712532.png)

## 5.6

![image-20191218200106680](PracticaUbuntuServer.imgs/image-20191218200106680.png)

## 5.7

![image-20191218200148493](PracticaUbuntuServer.imgs/image-20191218200148493.png)

## 5.8

<img src="PracticaUbuntuServer.imgs/image-20191218200322159.png" alt="image-20191218200322159"  />

# 6.

## 6.1

![image-20191218201240294](PracticaUbuntuServer.imgs/image-20191218201240294.png)

## 6.2

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 20-34-41.png)

## 6.3

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 20-36-29.png)

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 20-41-20.png)

![](/home/roger/dam/sys/imgs/Captura de pantalla de 2020-01-08 20-47-07.png)

## 6.4

Si, puc obrir el fitxer amb 7zip ja que soporta els dos tipus de fitxer.

# 7.

## 7.1

