# Practica Windows Server 2019

[TOC]

## Canvi de IP

![photo_2020-03-17_16-15-29](PracticaWindowsServer.imgs/photo_2020-03-17_16-15-29.jpg)







## Canvi de nom de l'equip

![photo_2020-03-17_16-16-07](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-07.jpg)

## Creació dels primers usuaris

![photo_2020-03-17_16-16-11](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-11.jpg)



















## Creació dels primers grups i assignació dels cursos al grup Informàtica

![photo_2020-03-17_16-16-15](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-15.jpg)

































## Permisos a la carpeta Comuna

![photo_2020-03-17_16-16-20](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-20.jpg)

































## Creació de plantilles per els usuaris

![photo_2020-03-17_16-16-24](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-24.jpg)

![photo_2020-03-17_16-16-27](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-27.jpg)

![photo_2020-03-17_16-16-30](PracticaWindowsServer.imgs/photo_2020-03-17_16-16-30.jpg)

## Unió del client al servidor Active Directory

![2020-03-17 160121](PracticaWindowsServer.imgs/2020-03-17 160121.png)

## Canvi de directiva de contrasenyes

![2020-03-17 162424](PracticaWindowsServer.imgs/2020-03-17 162424.png)

![2020-03-17 163015](PracticaWindowsServer.imgs/2020-03-17 163015.png)

Funciona correctament.

## Muntatje automàtic de la carpeta Comuna

![2020-03-17 163650](PracticaWindowsServer.imgs/2020-03-17 163650.png)

![2020-03-17 164534](PracticaWindowsServer.imgs/2020-03-17 164534.png)

## Creació i assignació de la GPO per instalar 7zip

![2020-03-17 165630](PracticaWindowsServer.imgs/2020-03-17 165630.png)





























## Resultat instalació automàtica de 7zip a el client

![2020-03-17 165840](PracticaWindowsServer.imgs/2020-03-17 165840.png)































## Assignació de carpeta perfil als usuaris

![2020-03-17 171648](PracticaWindowsServer.imgs/2020-03-17 171648.png)

## Creació i assignació de la directiva per redireccionar carpetes

![2020-03-17 172350](PracticaWindowsServer.imgs/2020-03-17 172350.png)









## Demostració de que els perfils mòbils funcionen

![2020-03-17 180053](PracticaWindowsServer.imgs/2020-03-17 180053.png)

![2020-03-17 180200](PracticaWindowsServer.imgs/2020-03-17 180200.png)

![2020-03-17 182629](PracticaWindowsServer.imgs/2020-03-17 182629.png)