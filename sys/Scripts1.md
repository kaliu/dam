# Exercici 1

```bash
#!/bin/bash
comanda = ls
echo “$comanda”
echo `$comanda`
echo '$comanda'
```

El primer *echo* mostra el valor de la variable com a text.

El segon *echo* executa la comanda i mostra el resultat.

El tercer *echo* mostra el text simplement mostra `$comanda`, el text literal entre cometes

# Exercici 2

```bash
#!/bin/bash
touch tmp
for i in *.txt
do     
    grep "examen" $i >> tmp
done

wc -l < tmp
rm tmp
```

Aquest script compta les vegades que es diu la paraula "examen" en tots els fitxers a la carpeta, els escriu en un fitxer anomenat tmp i després mostra cuantes vegades s'ha repetit al contar les linees que hi ha al fitxer tmp.

La comanda per substituir aquest script és 

`grep -o 'examen' *.txt | wc -l`

# Exercici 3

Per comprobar si dos cadenes son iguals el codi es el seguent:

```bash
#!/bin/bash

echo 'Introdueix la cadena 1'
read cdn1
echo 'Introdueix la cadena 2'
read cdn2

if [ "$cdn1" == "$cdn2" ]; then
 echo 'SI! Ambdues cadenes son iguals'
else
 echo 'No, no son iguals'
fi
```


