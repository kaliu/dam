#!/bin/bash

file='agenda.txt'

if ! [ -e $file ];
then
	touch $file
fi

while true
do

echo '

Bienvenido a la agenda de Contactos!	
Las opciones son las siguientes:
	1. Mostrar todas las entradas de la agenda de forma alfabetica
	2. Mostrar solo los nombres que empiezan por una letra
	3. Añadir un nuevo contacto de la agenda a teléfonos
	4. Eliminar un contacto de la agenda de telefonos
	5. Modificar el teléfono de un determinado nombre
	6. Encontrar un número de teléfono a partir de un nombre
	7. Encontrar un nombre a partir de un número de teléfono
	8. Salir
Introdueix una opció:' 
	read input
	case "$input" in
		"1")
			echo ""
			echo "El contenido de la agenda actualmente es el siguiente:"
			cat $file | sort | more
			;;
		"2")
			echo "Introduce la inicial por la que quieres filtrar"
			read char
			grep -F "$char" $file
			;;
		"3")
			echo "Introduce abajo el nombre del contacto"
			read nom
			echo "Introduce abajo el telefono del contacto"
			read telf
			printf "\n$nom $telf" >> $file
			;;
		"4")
			echo "Introduce abajo el NOMBRE del contacto que quieres eliminar"
			read nomDel
			sed -i "/$nomDel/d" "$file"
			;;
		"5")
			echo "Introduce el NOMBRE del contacto que quieres modificar"
			read nomMod
			echo "Intoduce el nuevo numero de telefono"
			read newTelf
			sed -i "/$nomMod/d" "$file"
			printf "\n$nomMod $newTelf" >> $file
			# sed -i -e "s/$nomMod*/$nomMod 19121213/g" "$file"
			;;
		"6")
			echo "Introduce el NOMBRE del telefono que buscas"
			read nomSearch
			grep -F "$nomSearch" $file
			;;
		"7")
			echo "Introduce el número del contacto que buscas"
			read numSearch
			grep -F "$numSearch" $file
			;;
		"8")
			exit 0;
			;;
	esac
done