#!/bin/bash
copNum=1
conta=1


if [ -d /etc/copiasSeguridad ]; then
	echo 'Carpeta de copias existe. Procediendo con la copia...'
else
	echo 'La carpteta no existe, creando...'
	mkdir /etc/copiasSeguridad
fi


while [ $conta -eq 1 ]; do
	if [ -e /etc/copiasSeguridad/fichero_conf_$copNum ]; then
		let "copNum = $copNum + 1"
	else
		conta=2
	fi

	if [ $copNum -gt 9 ]; then
		copNum=1
		rm /etc/copiasSeguridad/fichero_conf_$copNum
	fi

done


touch /etc/copiasSeguridad/fichero_conf_$copNum