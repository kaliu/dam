#!/bin/bash
#Validar si es un numero
es_valido='^[+]?[1-9]$'
i=0

# Filtros argumento
if ! [ $1 ] ; then
	echo 'Error, no se ha introducido argumento. Sintaxis: ex10.sh +-[1-10]'
	exit 1
fi

if ! [[ $1 =~ $es_valido || $1 == 10 ]] ; then
	echo 'Error, el argument introduit no es un nombre del 1 al 10.'
	exit 2
fi

# Bucle principal
while [ $i -le 10 ] 
do
		echo "$1 x $i= `expr $1 \* $i`"
		i=`expr $i + 1`
done