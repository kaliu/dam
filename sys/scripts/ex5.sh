#!/bin/bash

for i in "$@" 
do
	if [ test -d i == 1 ]; then
		echo "$i es un directori"
	elif [ test -f i == 1 ]; then
		echo "$i es un arxiu"
	else
		echo "$i no existeix"
	fi
done