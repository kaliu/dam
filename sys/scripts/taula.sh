#! /bin/bash

esNumero='^[0-9]+$'

# COMPROVACIÓ D'ERRORS EN ELS ARGUMENTS

## comprova si el número de paràmetres es correcte i que el tercer ha de ser igual o inferior a el segon
if [ $# -gt 3 ]; then
    echo 'Has posat més paràmetres del compte. Només cal posar 3 paràmetres.'
    exit 1
elif [ $# -lt 3 ]; then
    echo 'Es imprescindible que el script tingui 3 paràmetres.'
    exit 1
elif [ $3 -gt $2 ]; then
    echo 'El tercer paràmetre ha de ser igual o inferior al segon paràmetre.'
    exit 1
fi
## comprova si els arguments son numeros
if ! [[ $1 =~ $esNumero ]]; then
    echo 'El primer argument no es un número'
    exit 1
elif ! [[ $2 =~ $esNumero ]]; then
    echo 'El segon argument no es un número'
    exit 1
elif ! [[ $3 =~ $esNumero ]]; then
    echo 'El tercer argument no es un número'
    exit 1
fi
## comprova si els números són mes grans de 0 i més petits que 100
if [ $1 -lt 0 ] || [ $1 -gt 100 ]; then
    echo 'El primer paràmetre no pot ser més petit que 0 o més gran que 100.'
    exit 1
elif [ $2 -lt 0 ] || [ $2 -gt 100 ]; then
    echo 'El segon paràmetre no pot ser més petit que 0 o més gran que 100.'
    exit 1
elif [ $3 -lt 0 ] || [ $3 -gt 100 ]; then
    echo 'El tercer paràmetre no pot ser més petit que 0 o més gran que 100.'
    exit 1
fi

# CALCUL DE LA TAULA DE MULTIPLICAR

i=$2
while [ $i -gt $3 ] || [ $i -eq $3 ]
do
    echo " $1 x $i = `expr $1 \* $i`"
    i=`expr $i - 1`
done






## una alternativa al sistema de comprobació es aquest que retorna literalment el argument que no es un número 
## es per aprofitar el espai i, a més, ja he fet dues vegades el triple if al programa  i fer-ho
## una tercera vegada em sembla repetitiu.
# for arg in $@
# do
#     if ! [[ $arg =~ $esNumero ]]; then
#         echo "Error. El argument $arg no es un número."
#         exit 1
#     fi
# done